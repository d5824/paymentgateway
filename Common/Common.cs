﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Common
    {
        public const string gDisplayDateFormat = "dd/MM/yyyy";
        public const string gDisplayDateTimeFormat = "dd/MM/yyyy HH:mm:ss";
        public const int FieldNameLength = 50;
        public const int DescriptionShortLength = 100;
        public const int DescriptionLongLength = 200;
        public const int DescriptionVeryLongLength = 250;
        public const int RemarkLength = 500;
        public const int LongRemarkLength = 1000;

        #region Session constant
        public const string sesUserID = "sesUserID";
        #endregion


     

    }
}
