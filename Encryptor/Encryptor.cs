﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
namespace Encryptor
{
    public class Encryptor : IDisposable
    {
        static TripleDESCryptoServiceProvider TripleDes = new TripleDESCryptoServiceProvider();
        public static string GetHash(string str)
        {
            using (SHA512Managed hash = new SHA512Managed())
            {
                byte[] data = hash.ComputeHash(Encoding.UTF8.GetBytes(str));
                StringBuilder sb = new StringBuilder();

                for (int i = 0; i <= data.Length - 1; i++)
                {
                    sb.Append(data[i].ToString("x2"));
                }

                return sb.ToString();
            }
        }

        public Encryptor(string key)
        {
            TripleDes.Key = TruncateHash(key, TripleDes.KeySize / 8);
            TripleDes.IV = TruncateHash(key, TripleDes.BlockSize / 8);
        }

        private static byte[] TruncateHash(string key, int lenght)
        {
            SHA1CryptoServiceProvider sha1 = new SHA1CryptoServiceProvider();
            byte[] keyBytes = Encoding.Unicode.GetBytes(key);
            byte[] hash = sha1.ComputeHash(keyBytes);
            Array.Resize(ref hash, lenght );
            return hash;
        }

        public  string Encrypt(string PlainText, string salt = "")
        {
            byte[] plaintextBytes = Encoding.Unicode.GetBytes(salt + PlainText);
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            using (CryptoStream encStream = new CryptoStream(ms, TripleDes.CreateEncryptor(), CryptoStreamMode.Write))
            {
                encStream.Write(plaintextBytes, 0, plaintextBytes.Length);
                encStream.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());
            }

        }

        public  string Decrypt(string EncryptedText, string salt = "")
        {
            if (EncryptedText != "")
            {
                byte[] encryptedBytes = Convert.FromBase64String(EncryptedText);
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                string DecText = "";
                using (CryptoStream encStream = new CryptoStream(ms, TripleDes.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    encStream.Write(encryptedBytes, 0, encryptedBytes.Length);
                    encStream.FlushFinalBlock();
                    DecText = Encoding.Unicode.GetString(ms.ToArray());

                    if (salt == "" || DecText.StartsWith(salt))
                    {
                        DecText = DecText.Substring(salt.Length);
                    }
                    else
                    {
                        DecText = "0";
                    }
                }
                return DecText;
            }
            else
            {
                return "";
            }
        }

        static bool disposedValue;
        protected virtual void Dispose(bool disposing)
        {
            if (!Encryptor.disposedValue)
            {
                if (disposing)
                {
                    TripleDes.Dispose();
                }
            }

            Encryptor.disposedValue = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }

    public class EncryptorFactory{
        static Dictionary<string, Encryptor> EncryptorDict = new Dictionary<string, Encryptor>();
        static object LockObject = new object();

        public static Encryptor GetEncryptor(string key)
        {
            lock (LockObject)
            {
                if (!EncryptorDict.ContainsKey(key))
                {
                    EncryptorDict[key] = new Encryptor(key);
                }
            }
            return EncryptorDict[key];
        }

     }
}

