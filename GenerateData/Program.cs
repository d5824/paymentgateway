﻿using System;
using System.Net;
using System.Text.RegularExpressions;

namespace GenerateData
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            string strURL = @"https://www.fakeaddressgenerator.com/All_countries/address/country/Bangladesh";
            GetHTMLWebClient(strURL);
        }


        static string GetHTMLWebClient(string strURL)
        {
            //return html ul
            try
            {

                using (WebClient wc = new WebClient())
                {
                    //wc.Credentials = new NetworkCredential(userName, password);
                    string htmlCode = wc.DownloadString(strURL);
                    wc.Dispose();
                    string replacement = Regex.Replace(htmlCode, @"\t|\n|\r", "");

                    int pFrom = replacement.IndexOf("<ul>");
                    int pTo = replacement.LastIndexOf("</ul>");
                    string result = replacement.Substring(pFrom, pTo - pFrom);

                    return result;
                }

            }
            catch (Exception ex)
            {
               
                Console.WriteLine(ex.Message);
                return string.Empty;
            }


        }
    }
}
