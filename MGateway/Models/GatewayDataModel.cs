﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace PGateway.Models
{
    public class GatewayDataModel
    {
        public string GatewayID { get; set; }
        public string GatewayKey { get; set; }
        public enum ActiveStatus { get; set; }
       
    }

    public class GatewayMerchantModel
    {
        public string GatewayID { get; set; }
        public string merchantID { get; set; }
        public string AssignStatus { get; set; }

    }

    public enum ActiveStatus
    {
        [Description("Inactive")]
        Inactive = 0, 
        [Description("Active")]
        Active = 1, 
       
    }

    public class MerchantDataModel
    {
        public string merchantID { get; set; }
        public string merchantName { get; set; }
        public enum ActiveStatus { get; set; }

    }
}
