﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace MGateway.Models
{
    public class amaarModel
    {
        public string store_id { get; set; } = "sohoz"; //*REQUIRED
        public string signature_key { get; set; } = "1e75f16ea26bfb05baa5e4426eb2077c"; //*REQUIRED
        public string amount { get; set; } //*REQUIRED
        public string currency { get; set; } = "BDT"; //*REQUIRED
        public string tran_id { get; set; } //*REQUIRED
        public string cus_name { get; set; } //*REQUIRED
        public string cus_email { get; set; } //*REQUIRED
        public string cus_add1 { get; set; } = ""; //*REQUIRED
        public string cus_add2 { get; set; } = "House 1 Road 2"; //*REQUIRED
        public string cus_city { get; set; } = ""; //*REQUIRED
        public string cus_state { get; set; } = ""; 
        public string cus_postcode { get; set; } = "";
        public string cus_country { get; set; } = ""; //*REQUIRED
        public string cus_phone { get; set; }
        public string cus_fax { get; set; } = "";
        public string ship_name { get; set; } = "";
        public string ship_add1 { get; set; } = "";
        public string ship_add2 { get; set; } = "";
        public string ship_city { get; set; } = "";
        public string ship_state { get; set; } = "";
        public string ship_postcode { get; set; } = "";
        public string ship_country { get; set; } = "";
        public string desc { get; set; } = "Test Payment"; //*REQUIRED
        public string amount_processingfee_ratio { get; set; } = "0";
        public string amount_processingfee { get; set; } = "0";
        public string amount_vatratio { get; set; } = "0";
        public string amount_vat { get; set; } = "0";
        public string amount_taxratio { get; set; } = "0";
        public string amount_tax { get; set; } = "0";
        public string success_url { get; set; } //*REQUIRED
        public string fail_url { get; set; } //*REQUIRED
        public string cancel_url { get; set; } //*REQUIRED
        public string opt_a { get; set; } = "";
        public string opt_b { get; set; } = "";
        public string opt_c { get; set; } = "";
        public string opt_d { get; set; } = "";

    }


    public class amaarResModel
    {
        public string pg_service_charge_bdt { get; set; }
        public string pg_service_charge_usd { get; set; }
        public string pg_card_bank_name { get; set; }
        public string pg_card_bank_country { get; set; }
        public string card_number { get; set; }
        public string card_holder { get; set; }
        public string cus_phone { get; set; }
        public string desc { get; set; }
        public string success_url { get; set; }
        public string fail_url { get; set; }
        public string cus_name { get; set; }
        public string cus_email { get; set; }
        public string currency_merchant { get; set; }
        public string convertion_rate { get; set; }
        public string ip_address { get; set; }
        public string other_currency { get; set; }
        public string pay_status { get; set; }
        public string pg_txnid { get; set; }
        public string epw_txnid { get; set; }
        public string mer_txnid { get; set; } //from amaarModel.tran_id  
        public string store_id { get; set; }
        public string merchant_id { get; set; }
        public string currency { get; set; }
        public string store_amount { get; set; }
        public string pay_time { get; set; }
        public string amount { get; set; }
        public string bank_txn { get; set; }
        public string card_type { get; set; }
        public string reason { get; set; }
        public string pg_card_risklevel { get; set; }
        public string pg_error_code_details { get; set; }
        public string opt_a { get; set; }
        public string opt_b { get; set; }
        public string opt_c { get; set; }
        public string opt_d { get; set; }
        public DateTime RecordTime { get; set; } = DateTime.Now;
        //public ResultStatus ResultStatus { get; set; }
}

    //public enum ResultStatus
    //{
    //    [Description("Success")]
    //    Success = 1,
    //    [Description("Failed")]
    //    Failed = 2,
    //    [Description("Cancelled")]
    //    Cancelled = 3,
    //}
}