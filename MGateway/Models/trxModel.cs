﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace MGateway.Models
{
    public class trxModel
    {
        public string TrxID { get; set; }
        public string TrxID_M { get; set; }
        public string TrxID_G { get; set; }
       // public TrxStatus TrxStatus { get; set; }
        public string MerchantID { get; set; }
        public string GatewayID { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime GatewayGeneratedTime { get; set; }
        public DateTime GatewayRespondTime { get; set; }
        public DateTime ManualUpdateTime { get; set; }
        public string ManualUpdateBy { get; set; }

    }

    //public enum TrxStatus
    //{
    //    [Description("New")]
    //    New = 0, //When record is generated from payment gateway.
    //    [Description("Pending")]
    //    Pending = 1, //When record is generated from payment gateway.
    //    [Description("Success")]
    //    Success = 2, //payment gateway respond
    //    [Description("Failed")]
    //    Failed = 3, //payment gateway respond
    //    [Description("Cancelled")]
    //    Cancelled = 4, //payment gateway respond
    //    [Description("Manual")]
    //    Manual = 5, //Manual success
    //    [Description("Expired")]
    //    Expired = 6 //?? scheduler expired ? 
    //}
}