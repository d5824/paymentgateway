﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MGateway.Models;
using System.Web.Mvc;
using System.Web.Mvc.Html;
namespace MGateway.Processor
{
    public class FormLayoutHelper
    {
        protected HtmlHelper MyHtmlHelper;
        protected ViewContext MyViewContext;


        public FormLayoutHelper(HtmlHelper HtmlHelper, ViewContext ViewContext)
        {
            MyHtmlHelper = HtmlHelper;
            MyViewContext = ViewContext;
        }

        public MvcHtmlString GenerateWebContent(string WebContent, UrlHelper urlHelper)
        {
            MyViewContext.Writer.WriteLine(WebContent);
           

            return MvcHtmlString.Empty;
        }

      

    }


}