﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;
using System.Reflection;

namespace LuckyDraw.Repository
{

    
    public class MySQLDB
    {
        static string _Server = System.Configuration.ConfigurationManager.AppSettings.Get("ServerName");
        static string _DbName = System.Configuration.ConfigurationManager.AppSettings.Get("DBName");
        static string _UserName = "RDAdmin";
        static string _PW = "RDAdmin1100";
        static string _ConnStr = "Server={0};database={1};UID={2};password={3};{4}";
        static string _HandleNullDate = "convert zero datetime=True";

        public string ConnectionString { get; set; }

        private static MySQLDB _instance { get; set; }
        public static MySQLDB Instance(bool isDefault)
        {
            if (_instance == null)
            {
                _instance = new MySQLDB();

                if (isDefault == true)
                {
                    //Dim Enc As Encryptor = EncryptorFactory.GetEncryptor("DB")
                    Helper.Encryptor enc = Helper.EncryptorFactory.GetEncryptor("DB");
                    //undim this to get Encrypt server and database 
                    //string ServerName = enc.Encrypt(_Server, "Server");
                    //string DatabaseName = enc.Encrypt(_DbName, "Database");
                    _Server = enc.Decrypt(_Server, "Server");
                    _DbName = enc.Decrypt(_DbName, "Database");
                    _instance.ConnectionString = string.Format(_ConnStr, _Server, _DbName, _UserName, _PW, _HandleNullDate);

                }
            }



            return _instance;
        }

        public static DataTable GetDataTable( string strSQL, params object[] values)
        {

            DataTable tbl = new DataTable();
            MySQLDB dbCon = MySQLDB.Instance(true);
            using (MySqlConnection conn = new MySqlConnection(dbCon.ConnectionString))
            {

                conn.Open();
                using (MySqlCommand myCommand = new MySqlCommand(strSQL, conn))
                {
                    AddParam(values, myCommand);
                    using (MySqlDataAdapter adapter = new MySqlDataAdapter(myCommand))
                    {
                        adapter.Fill(tbl);
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
            return tbl;
        }

        public static List<T> GetValueModel<T>(string strSQL, params object[] values)
        {
            List<T> list = new List<T>();
            
            MySQLDB dbCon = MySQLDB.Instance(true);
           
            using (MySqlConnection conn = new MySqlConnection(dbCon.ConnectionString))
            {
                using (MySqlCommand comm = new MySqlCommand(strSQL))
                {
                    AddParam(values, comm);
                    comm.Connection = conn;
                    conn.Open();
                    using (MySqlDataReader dr = comm.ExecuteReader())
                    {
                        T obj = default(T);
                        while (dr.Read())
                        {
                            obj = Activator.CreateInstance<T>();
                            foreach (PropertyInfo prop in obj.GetType().GetProperties())
                            {

                                if (!object.Equals(dr[prop.Name], DBNull.Value))
                                {
                                    object safeValue = null;
                                    Type t = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                                    if (prop.PropertyType.IsEnum)
                                    {
                                        safeValue = (dr[prop.Name] == null) ? null : Enum.ToObject(prop.PropertyType, dr[prop.Name]);
                                    }
                                    else
                                    {
                                         safeValue = (dr[prop.Name] == null) ? null : Convert.ChangeType(dr[prop.Name], t);
                                    }
                                    
                                    prop.SetValue(obj, safeValue, null);
                                }
                            }
                            list.Add(obj);
                        }

                        conn.Close();
                        conn.Dispose();
                    }


                    return list;
                }
            }
        }

        public static int ExecuteNonQuery(string strSQL, params object[] values)
        {
            MySQLDB dbCon = MySQLDB.Instance(true);
            using (MySqlConnection conn = new MySqlConnection(dbCon.ConnectionString))
            {

                conn.Open();
                using (MySqlCommand myCommand = new MySqlCommand(strSQL, conn))
                {
                    AddParam(values, myCommand);
                    return myCommand.ExecuteNonQuery();

                }
            }
        }

        public static bool HasRows(string strSQL, params object[] values)
        {
            MySQLDB dbCon = MySQLDB.Instance(true);
            using (MySqlConnection conn = new MySqlConnection(dbCon.ConnectionString))
            {

                conn.Open();
                using (MySqlCommand myCommand = new MySqlCommand(strSQL, conn))
                {
                    AddParam(values, myCommand);
                    using (MySqlDataReader rdr = myCommand.ExecuteReader())
                    {
                        return rdr.HasRows;
                    }
                   // return myCommand.ExecuteNonQuery();

                }
            }
        }


        private static void AddParam(object[] values, MySqlCommand com)
        {
            int i = 0;
            for (i = 0; i <= values.Length - 1; i++)
            {
                MySqlParameter param = new MySqlParameter("@p" + i, values[i]);
                if (param.DbType == DbType.DateTime)
                {
                    //param.DbType = DbType.DateTime2;
                }

                com.Parameters.Add(param);

            }
        }

        public static List<T> GetValueModel<T>(MySqlCommand MySqlComm)
        {
            List<T> list = new List<T>();
            MySQLDB dbCon = MySQLDB.Instance(true);
            using (MySqlConnection conn = new MySqlConnection(dbCon.ConnectionString))
            {
                using (MySqlCommand comm = MySqlComm)
                {

                    comm.Connection = conn;
                    conn.Open();
                    using (MySqlDataReader dr = comm.ExecuteReader())
                    {
                        T obj = default(T);
                        while (dr.Read())
                        {
                            obj = Activator.CreateInstance<T>();
                            foreach (PropertyInfo prop in obj.GetType().GetProperties())
                            {

                                if (!object.Equals(dr[prop.Name], DBNull.Value))
                                {
                                    Type t = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                                    object safeValue = (dr[prop.Name] == null) ? null : Convert.ChangeType(dr[prop.Name], t);
                                    prop.SetValue(obj, safeValue, null);
                                }
                            }
                            list.Add(obj);
                        }

                        conn.Close();
                        conn.Dispose();
                    }


                    return list;
                }
            }
        }
    }
}