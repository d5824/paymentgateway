﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using PGateway.Models;
using MySql.Data.EntityFramework;

namespace PGateway.Repository
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class LuckyDrawDB : DbContext
    {
        public LuckyDrawDB() : base(MySQLDB.Instance(true).ConnectionString)
        {
            this.Configuration.ValidateOnSaveEnabled = false;
        }

      public LuckyDrawDB(string Connectionstring)
        {
            this.Database.Connection.ConnectionString = Connectionstring;
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
           
            //modelBuilder.Entity<UserLoginModel>().ToTable("tbl_UserLogin");
            //modelBuilder.Entity<CampaignModel>().ToTable("tbl_Campaign");
            //modelBuilder.Entity<CampaignPrizeModel>().ToTable("tbl_CampaignPrize").
            //    HasRequired(x => x.master).
            //    WithMany(x => x.prizes).
            //    HasForeignKey(x => new { x.UserID, x.CampaignName }).
            //    WillCascadeOnDelete(true);
            //modelBuilder.Entity<CampaignPrizeModel>().Property(x=> x.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            //modelBuilder.Entity<FileLibraryModel>().ToTable("tbl_FileLibrary").Property(x=> x.FileNo).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
           
        }
    } 


}