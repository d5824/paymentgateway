﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Linq.Expressions;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Infrastructure;
using Z.EntityFramework;

namespace PGateway.Repository
{
    public interface IRepository<T> : IDisposable where T : class 
    {
        IQueryable<T> Select(Expression<Func<T, object>> predicate);
        IQueryable<T> Include(Expression<Func<T, object>> IncludeProp);
        IQueryable<T> GetAll();
        IQueryable<T> Where(Expression<Func<T, bool>> predicate);
        IQueryable<T> WhereNoTracking(Expression<Func<T, bool>> predicate);
        bool Contains(Expression<Func<T, bool>> predicate);
        T Find(params object[] keyvalues);
        void Detach(T item);

        int InsertOrUpdate(T item);
        int Insert(T item);
        int Update(T item);
        int Delete(T item);
        IEnumerable<T> DeleteRange(IEnumerable<T> item);

        void BulkInsert(IEnumerable<T> item, int BatchSize = 10000);

    }
    public class Repository<T> : IRepository<T> where T : class
    {
        protected DbContext _db;
        bool _ShareContext = false;
        public Repository()
        {
            _ShareContext = true;
            _db = new LuckyDrawDB();
            _db.Database.CommandTimeout = 600;
        }

        public DbContext Db
        {
            get
            {
                return _db;

            }
          
        }

        protected DbSet<T> DbSet
        {
            get
            {
                return _db.Set<T>();

            }

        }

        public IQueryable<T> Select(Expression<Func<T, object>> predicate) 
        {
            return (IQueryable<T>)DbSet.Select(predicate);
        }

        public IQueryable<T> Include(Expression<Func<T, object>> IncludeProp)
        {
            return (IQueryable<T>)DbSet.Include(IncludeProp);
        }

        public IQueryable<T> GetAll()
        {
            return DbSet.AsQueryable();
        }

        public IQueryable<T> Where(Expression<Func<T, bool>> predicate)
        {
            return DbSet.Where(predicate);
        }

        public IQueryable<T> WhereNoTracking(Expression<Func<T, bool>> predicate)
        {
            return DbSet.AsNoTracking().Where(predicate);
        }

        public bool Contains(Expression<Func<T, bool>> predicate)
        {
            return DbSet.Count(predicate) > 0;
        }

        public T Find(params object[] keyvalues)
        {
            return DbSet.Find(keyvalues);
        }

        public void Detach(T item)
        {
            _db.Entry<T>(item).State = EntityState.Detached;
        }

        public int InsertOrUpdate(T item)
        {
            DbSet.AddOrUpdate(item);
            try
            {
                return _db.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                foreach(System.Data.Entity.Validation.DbEntityValidationResult ValidationError in ex.EntityValidationErrors)
                {
                    foreach(System.Data.Entity.Validation.DbValidationError ErrorItem in ValidationError.ValidationErrors)
                    {
                        Console.Write(ErrorItem.PropertyName + " - " + ErrorItem.ErrorMessage);
                    }
                }
                throw;
            }

        }


        public int Insert(T item)
        {
            DbSet.Add(item);
            try
            {
                return _db.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                foreach (System.Data.Entity.Validation.DbEntityValidationResult ValidationError in ex.EntityValidationErrors)
                {
                    foreach (System.Data.Entity.Validation.DbValidationError ErrorItem in ValidationError.ValidationErrors)
                    {
                        Console.Write(ErrorItem.PropertyName + " - " + ErrorItem.ErrorMessage);
                    }
                }
                throw;
            }
        }
        public int Update(T item)
        {
            _db.Entry<T>(item).State = EntityState.Modified;
            try
            {
                return _db.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                foreach (System.Data.Entity.Validation.DbEntityValidationResult ValidationError in ex.EntityValidationErrors)
                {
                    foreach (System.Data.Entity.Validation.DbValidationError ErrorItem in ValidationError.ValidationErrors)
                    {
                        Console.Write(ErrorItem.PropertyName + " - " + ErrorItem.ErrorMessage);
                    }
                }
                throw;
            }
        }
        public int Delete(T item)
        {
            DbSet.Remove(item);
            return _db.SaveChanges();
        }
        public IEnumerable<T> DeleteRange(IEnumerable<T> item)
        {
            return DbSet.RemoveRange(item);
        }

        public int SaveChanges()
        {
            try
            {
                return _db.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            {
                foreach (System.Data.Entity.Validation.DbEntityValidationResult ValidationError in ex.EntityValidationErrors)
                {
                    foreach (System.Data.Entity.Validation.DbValidationError ErrorItem in ValidationError.ValidationErrors)
                    {
                        Console.Write(ErrorItem.PropertyName + " - " + ErrorItem.ErrorMessage);
                    }
                }
                throw;
            }
        }

        public void BulkInsert(IEnumerable<T> item, int BatchSize = 10000)
        {
            _db.BulkInsert(item, options => { options.BatchTimeout = 600; options.BatchSize = BatchSize; }) ;
        }

        static bool disposedValue;
        protected virtual void Dispose(bool disposing)
        {
            if (!Repository.Repository<T>.disposedValue)
            {
                if (disposing)
                {
                    if(_ShareContext && _db != null)
                    {
                        _db.Dispose();
                    }
                    
                }
            }

            Repository.Repository<T>.disposedValue = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }




    }


}