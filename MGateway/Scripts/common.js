﻿var _func = {
    ajxNew: function () {
        var ajxVal =
        {
            type: 'POST',
            url: '',
            data: '',
            cache: false,
            async: true,
           
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
          
        }

        return ajxVal;
    },
    ajxCall: function (ajxVal, _returnAction) {
        var returnVal;
        $.ajax({
            type: ajxVal.type,
            url: ajxVal.url,
            data: ajxVal.data,
            cache: ajxVal.cache,
            async: ajxVal.async,
            contentType: ajxVal.contentType,
            beforeSend: function () {
                $("#loadingscreen").show();
            },
            //.done() Replaces method success() which was deprecated in jQuery 1.8.
            //success: function (resdata) {
            //   
            //    var returnRes = { 'result': 'success', 'returnVal': resdata };
            //    _returnAction(returnRes);
            //    $('#loadingscreen').hide();
            //},
            //.fail() Replaces method fail() which was deprecated in jQuery 1.8.
            //fail: function (xhr, textStatus, errorThrown) {
            //    var result = { 'xhr': xhr, 'textStatus': textStatus, 'errorThrown': errorThrown };
            //    returnVal = { 'result': 'fail', 'returnVal': result };
            //    console.log(returnVal);
            //    //$('#loadingscreen').hide();
            //}
        }).done(function (resdata) {
            var returnRes = { 'result': 'success', 'returnVal': resdata };
            _returnAction(returnRes);
            $('#loadingscreen').hide();
        }).fail(function (jqXHR, textStatus, errorThrown) {
            var result = { 'xhr': jqXHR, 'textStatus': textStatus, 'errorThrown': errorThrown };
            returnVal = { 'result': 'fail', 'returnVal': result };
            console.log(returnVal);
            $('#loadingscreen').hide();
        });

    },
    errorShow: function (title, message) {
        $('#__errorTitle').text(title);
        $('#__errorMsg').text(message);
        $('#__divError').show();
        $('#__divcontent').hide();
    },
    errorHide: function () {
        $('#__divError').hide();
        $('#__divcontent').show();
    },

};