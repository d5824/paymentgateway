﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Global.Model;
using MGatewayHelpers;
using MGatewayProcessor;
using MGatewayBO.Model;
using System.Data;
namespace MGatewayBO.Controllers
{
    public class CashTransactionController : Controller
    {
      
        public ActionResult Index(string _TrxType)
        {
            ViewBag.Title = "Cash Out";
            ViewBag.ObjectID = 4;
            TransactionType TrxType = TransactionType.CashOut;
            if (_TrxType == "CashIn")
            {
                ViewBag.ObjectID = 3;
                ViewBag.Title = "Cash In";
                TrxType = TransactionType.CashIn;
            }
            
            DateTime currentMonth = DateTime.Today;
            DateTime StartDate = new DateTime(currentMonth.Year, currentMonth.Month, 1);
            DateTime EndDate = new DateTime(currentMonth.Year, currentMonth.Month, DateTime.DaysInMonth(currentMonth.Year, currentMonth.Month));
            string _StartDate = StartDate.ToString(GlobalProperties.gDisplayDateFormat);
            string _EndDate = EndDate.ToString(GlobalProperties.gDisplayDateFormat);

            return ReloadTransactionList(TrxType, _StartDate, _EndDate);
        }

        public ActionResult ReloadTransactionList(TransactionType trxType,string _StartDate, string _EndDate)
        {
            JsGridModel m = PrepareTransactionList(trxType,_StartDate, _EndDate);
            GlobalSetingsViewModel gbl = GlobalSettings.GetSettings();
            List<CashInOutViewModel> dataList = CashTransaction.GetTransactionInfoCache(GlobalProperties.UserID);
            ViewBag.TotalAmount = CashTransaction.GetTransactionTotalAmount(dataList);
            ViewBag.DateRange = _StartDate + " - " + _EndDate;
            ViewBag.RefreshInterval = gbl.WalletMinList;
            PrepareEditFormViewBag();
            List<SelectListItem> selectlist = new List<SelectListItem>();
            List<ShopModel> ShopList = Shop.GetShop(GlobalProperties.MerchantID);
            foreach (ShopModel item in ShopList)
            {
                selectlist.Add(new SelectListItem { Value = item.ShopID, Text = item.ShopName });
            }
            ViewBag.ShopList = selectlist;


            return View(m);
        }

        public ActionResult ReloadTransactionListDate(TransactionType trxType, string _StartDate, string _EndDate, List<string> ShopList)
        {
            TrxInfoGridReloadResult trxInfoGridReloadResult = new TrxInfoGridReloadResult();
            JsGridModel m = PrepareTransactionList(trxType,_StartDate, _EndDate);
            List<CashInOutViewModel> dataList = CashTransaction.GetTransactionInfoCache(GlobalProperties.UserID);
            if (ShopList != null)
            {
                List<CashInOutViewModel> NewdataList = new List<CashInOutViewModel>();
                foreach (CashInOutViewModel item in dataList)
                {
                    if (ShopList.Contains(item.ShopID))
                    {
                        NewdataList.Add(item);
                    }
                }
                m.data = NewdataList;
                CashTransaction.UpdateTransactionInfoCache(GlobalProperties.UserID, NewdataList);
               
            }

            trxInfoGridReloadResult.jsGrid = m;
            trxInfoGridReloadResult.TotalAmount = CashTransaction.GetTransactionTotalAmount(dataList);


            return Json(trxInfoGridReloadResult);
        }

        private JsGridModel PrepareTransactionList(TransactionType trxType,string _StartDate, string _EndDate)
        {
           
            JsGridModel m = CashTransaction.GetCashTransactionGrid(GlobalProperties.UserRole, GlobalProperties.MerchantID,trxType,GlobalProperties.UserID, _StartDate, _EndDate);
            GridControllerHelper.UpdateRouteURL(m, Request.RequestContext);
            return m;
        }

        public ActionResult LoadCashTransaction(CashInOutViewModel vm,int pageIndex, int pageSize, string sortField, string sortOrder)
        {

            CashTransactionGridFilterResult resultData = CashTransaction.PerformFilter(GlobalProperties.UserID,vm, pageIndex, pageSize, sortField, sortOrder);
            

            return Json(resultData);
        }

        public ActionResult AddRecord(TransactionType TrxType)
        {
            CashTransaction.AddRecord(GlobalProperties.MerchantID, TrxType);
            return Json(true);
        }

        public ActionResult ApprovalAction(string ID, string TrxID, CashInOutAction Status)
        {
            CashTransaction.ApprovalAction(GlobalProperties.UserName, GlobalProperties.MerchantID, ID,TrxID, Status);
            return Json(true);
        }

        public ActionResult DeleteRecord(CashInOutViewModel m)
        {
            CashTransaction.DeleteRecord(m, GlobalProperties.UserName,GlobalProperties.MerchantID );
            CashTransaction.RemoveTransactionFromCache(GlobalProperties.UserID,m);
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ExportToExcel(string ExportProvider)
        {
            List<CashInOutViewModel> trxModel = CashTransaction.GetTransactionInfoCache(GlobalProperties.UserID);
           
            byte[] sfilebytes = Function.ExportToExcelCollection<CashInOutViewModel>(trxModel);
            string sfilename = "Transaction_" + DateTime.Today.ToString("dd_mm_yyyy") + ".xlsx";
            string filetype = "application/vnd.ms-excel";

            return File(sfilebytes, filetype, sfilename);
        }


        public ActionResult EditForm()
        {
            CashInOutSQLModel m = new CashInOutSQLModel();
            PrepareEditFormViewBag();
            ViewBag.ObjectID = 9;
            ViewBag.Title = "Add New Transaction";

            List<SelectListItem> selectlist = new List<SelectListItem>();
            List<ShopWalletsViewModel> WalletList = Shop.GetAllShopWallets(GlobalProperties.MerchantID);
            foreach (ShopWalletsViewModel item in WalletList)
            {
                selectlist.Add(new SelectListItem { Value = item.WalletType + "#" + item.WalletNumber, Text = item.WalletNumber });
            }
            ViewBag.WalletList = selectlist;

            return View("_EditForm", m);

        }

        public ActionResult AddManualRecord(CashInOutSQLModel m)
        {
            AddManualRecordContainer res = CashTransaction.AddRecordManual(GlobalProperties.MerchantID, m);

           
            return Json(res.resultmsg);
        }


        private void PrepareEditFormViewBag()
        {
            List<SelectListItem> selectlist = new List<SelectListItem>();
            Dictionary<int, string> TransactionTypeDict = MGatewayProcessor.Helpers.DataReaderHelper.GetEnumDict(typeof(TransactionType));

            
            foreach (KeyValuePair<int, string> item in TransactionTypeDict)
            {
                selectlist.Add(new SelectListItem { Value = item.Key.ToString(), Text = item.Value });
            }
            ViewBag.TransactionTypeList = selectlist;

            selectlist = new List<SelectListItem>();
            Dictionary<int, string> WalletTypeDict = MGatewayProcessor.Helpers.DataReaderHelper.GetEnumDict(typeof(WalletType));

            
            foreach (KeyValuePair<int, string> item in WalletTypeDict)
            {
                selectlist.Add(new SelectListItem { Value = item.Key.ToString(), Text = item.Value });
            }
            ViewBag.WalletTypeList = selectlist;

        }







    }
}