﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Global.Model;
using MGatewayHelpers;
using MGatewayProcessor;
using MGatewayBO.Model;
using System.Data;
namespace MGatewayBO.Controllers
{
    public class CashTransactionLogController : Controller
    {
      
        public ActionResult Index(string _TrxType)
        {
            ViewBag.Title = "Cash In Out Log";
            ViewBag.ObjectID = 11;
          
            DateTime currentMonth = DateTime.Today;
            DateTime StartDate = new DateTime(currentMonth.Year, currentMonth.Month, 1);
            DateTime EndDate = new DateTime(currentMonth.Year, currentMonth.Month, DateTime.DaysInMonth(currentMonth.Year, currentMonth.Month));
            string _StartDate = StartDate.ToString(GlobalProperties.gDisplayDateFormat);
            string _EndDate = EndDate.ToString(GlobalProperties.gDisplayDateFormat);

            return ReloadTransactionList( _StartDate, _EndDate);
        }

        public ActionResult ReloadTransactionList(string _StartDate, string _EndDate)
        {
            JsGridModel m = PrepareTransactionList(_StartDate, _EndDate);
           
            List<CashInOutLogViewModel> dataList = CashTransactionLog.GetTransactionInfoCache(GlobalProperties.UserID);
            ViewBag.TotalAmount = CashTransactionLog.GetTransactionTotalAmount(dataList);
            ViewBag.DateRange = _StartDate + " - " + _EndDate;
           
            PrepareEditFormViewBag();
            List<SelectListItem> selectlist = new List<SelectListItem>();
            List<ShopModel> ShopList = Shop.GetShop(GlobalProperties.MerchantID);
            foreach (ShopModel item in ShopList)
            {
                selectlist.Add(new SelectListItem { Value = item.ShopID, Text = item.ShopName });
            }
            ViewBag.ShopList = selectlist;


            return View(m);
        }

        public ActionResult ReloadTransactionListDate(string _StartDate, string _EndDate, List<string> ShopList)
        {
            TrxInfoGridReloadResult trxInfoGridReloadResult = new TrxInfoGridReloadResult();
            JsGridModel m = PrepareTransactionList(_StartDate, _EndDate);
            List<CashInOutLogViewModel> dataList = CashTransactionLog.GetTransactionInfoCache(GlobalProperties.UserID);
            if (ShopList != null)
            {
                List<CashInOutLogViewModel> NewdataList = new List<CashInOutLogViewModel>();
                foreach (CashInOutLogViewModel item in dataList)
                {
                    if (ShopList.Contains(item.ShopID))
                    {
                        NewdataList.Add(item);
                    }
                }
                m.data = NewdataList;
                CashTransactionLog.UpdateTransactionInfoCache(GlobalProperties.UserID, NewdataList);
               
            }

            trxInfoGridReloadResult.jsGrid = m;
            trxInfoGridReloadResult.TotalAmount = CashTransactionLog.GetTransactionTotalAmount(dataList);


            return Json(trxInfoGridReloadResult);
        }

        private JsGridModel PrepareTransactionList(string _StartDate, string _EndDate)
        {
           
            JsGridModel m = CashTransactionLog.GetCashTransactionGrid(GlobalProperties.UserRole, GlobalProperties.MerchantID,GlobalProperties.UserID, _StartDate, _EndDate);
            GridControllerHelper.UpdateRouteURL(m, Request.RequestContext);
            return m;
        }

        public ActionResult LoadCashTransaction(CashInOutLogViewModel vm,int pageIndex, int pageSize, string sortField, string sortOrder)
        {

            CashTransactionGridLogFilterResult resultData = CashTransactionLog.PerformFilter(GlobalProperties.UserID,vm, pageIndex, pageSize, sortField, sortOrder);
            

            return Json(resultData);
        }


        public ActionResult ExportToExcel(string ExportProvider)
        {
            List<CashInOutLogViewModel> trxModel = CashTransactionLog.GetTransactionInfoCache(GlobalProperties.UserID);

            byte[] sfilebytes = Function.ExportToExcelCollection<CashInOutLogViewModel>(trxModel);
            string sfilename = "TransactionLog_" + DateTime.Today.ToString("dd_mm_yyyy") + ".xlsx";
            string filetype = "application/vnd.ms-excel";

            return File(sfilebytes, filetype, sfilename);
        }


        private void PrepareEditFormViewBag()
        {
            List<SelectListItem> selectlist = new List<SelectListItem>();
            Dictionary<int, string> TransactionTypeDict = MGatewayProcessor.Helpers.DataReaderHelper.GetEnumDict(typeof(TransactionType));

            
            foreach (KeyValuePair<int, string> item in TransactionTypeDict)
            {
                selectlist.Add(new SelectListItem { Value = item.Key.ToString(), Text = item.Value });
            }
            ViewBag.TransactionTypeList = selectlist;

            selectlist = new List<SelectListItem>();
            Dictionary<int, string> WalletTypeDict = MGatewayProcessor.Helpers.DataReaderHelper.GetEnumDict(typeof(WalletType));

            
            foreach (KeyValuePair<int, string> item in WalletTypeDict)
            {
                selectlist.Add(new SelectListItem { Value = item.Key.ToString(), Text = item.Value });
            }
            ViewBag.WalletTypeList = selectlist;

        }








    }
}