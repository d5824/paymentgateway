﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MGatewayBO.Controllers
{
    public class GlobalSettingsController : Controller
    {
      
        public ActionResult Index()
        {
            ViewBag.Title = "Global Settings";
            ViewBag.ObjectID = 8;
            Model.GlobalSetingsViewModel m = MGatewayProcessor.GlobalSettings.GetSettings();
            Dictionary<int, string> WalletType = MGatewayProcessor.Helpers.DataReaderHelper.GetEnumDict(typeof(Model.WalletType));
            Dictionary<string, Model.ShopWalletsSenderModel> ShopwalletSenderDict = MGatewayProcessor.Shop.GetShopWalletSender();
            Dictionary<string, Model.RejectRemarksModel> RejectRemarksDict = MGatewayProcessor.GlobalSettings.GetRejectRemarksDict();
            ViewBag.WalletTypeDict = WalletType;
            ViewBag.WalletSender = ShopwalletSenderDict;
            ViewBag.RejectRemark = RejectRemarksDict;
            return View(m);
        }

        public ActionResult UpdateSettings(Model.GlobalSetingsViewModel vm, List<Model.ShopWalletsSenderModel> WalletSenderList, List<Model.RejectRemarksModel> RejectRemarksList)
        {
            MGatewayProcessor.GlobalSettings.UpdateSettings(vm);
            MGatewayProcessor.GlobalSettings.UpdateRejectRemark(RejectRemarksList);
            MGatewayProcessor.Shop.UpdateShopWalletSender(WalletSenderList);

            return Json(true);
        }

        public ActionResult DeleteRejectRemark(string ID)
        {
          
            MGatewayProcessor.GlobalSettings.DeleteRejectRemark(ID);
            

            return Json(true);
        }



    }
}