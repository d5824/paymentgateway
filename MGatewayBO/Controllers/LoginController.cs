﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MGatewayHelpers;
using MGatewayProcessor;
using MGatewayBO.Model;
namespace MGatewayBO.Controllers
{
    public class LoginController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
           // Login.TestConnection();
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Index(string username, string password)
        {
            //testpush
            Dictionary<string, object> resultDict = new Dictionary<string, object>();
            resultDict.Add("msg", string.Empty);
            LoginResultModel LoginResult = Login.CheckLogin(username, password);
            if (LoginResult.AccountStatus == AccStatus.Active || LoginResult.AccountStatus == AccStatus.ChangePassword)
            {
               
                GlobalProperties.UserID = LoginResult.UserID;
                GlobalProperties.UserName = LoginResult.UserName;
                GlobalProperties.MerchantID = LoginResult.MerchantID;
                GlobalProperties.UserRole = LoginResult.UserRole;
                //if(LoginResult.UserRole == "Merchant")
                //{
                //    resultDict.Add("URL", Url.Action("EditForm", "CashTransaction"));
                //}
                //else
                //{
                    resultDict.Add("URL", Url.Action("Index", "Shop"));
                //}
                
                resultDict["msg"] = string.Empty;
            }
            else
            {
                resultDict["msg"] = LoginResult.AccountStatus.GetEnumDescription();
            }

            return Json(resultDict, JsonRequestBehavior.AllowGet);
          
          
        }

        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Index");
        }



    }
}