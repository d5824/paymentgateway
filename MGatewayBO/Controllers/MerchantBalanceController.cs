﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Global.Model;
using MGatewayHelpers;
using MGatewayProcessor;
using MGatewayBO.Model;
using System.Data;
namespace MGatewayBO.Controllers
{
    public class MerchantBalanceController : Controller
    {
      
        public ActionResult Index(string _TrxType)
        {
            ViewBag.Title = "Merchant Balance";
            ViewBag.ObjectID = 10;
          
            DateTime currentMonth = DateTime.Today;
            DateTime StartDate = new DateTime(currentMonth.Year, currentMonth.Month, 1);
            DateTime EndDate = new DateTime(currentMonth.Year, currentMonth.Month, DateTime.DaysInMonth(currentMonth.Year, currentMonth.Month));
            string _StartDate = StartDate.ToString(GlobalProperties.gDisplayDateFormat);
            string _EndDate = EndDate.ToString(GlobalProperties.gDisplayDateFormat);

            return ReloadTransactionList( _StartDate, _EndDate);
        }

        public ActionResult ReloadTransactionList(string _StartDate, string _EndDate)
        {
            Dictionary<string, MerchantBalanceModel> m = CashTransaction.GetMerchantBalance(GlobalProperties.MerchantID, _StartDate, _EndDate);
            //PrepareEditFormViewBag();
            List<SelectListItem> selectlist = new List<SelectListItem>();
            List<ShopModel> ShopList = Shop.GetShop(GlobalProperties.MerchantID);
            ViewBag.DateRange = _StartDate + " - " + _EndDate;
            foreach (ShopModel item in ShopList)
            {
                selectlist.Add(new SelectListItem { Value = item.ShopID, Text = item.ShopName });
            }
            ViewBag.ShopList = selectlist;

            return View(m);
        }

        public ActionResult ReloadTransactionListDate( string _StartDate, string _EndDate, List<string> ShopList)
        {
            Dictionary<string, MerchantBalanceModel> m = CashTransaction.GetMerchantBalance(GlobalProperties.MerchantID, _StartDate, _EndDate, ShopList);

            return Json(m);
        }

     


        private JsGridModel PrepareTransactionList(TransactionType trxType,string _StartDate, string _EndDate)
        {
           
            JsGridModel m = CashTransaction.GetCashTransactionGrid(GlobalProperties.UserRole, GlobalProperties.MerchantID,trxType,GlobalProperties.UserID, _StartDate, _EndDate);
            GridControllerHelper.UpdateRouteURL(m, Request.RequestContext);
            return m;
        }

        public ActionResult LoadCashTransaction(CashInOutViewModel vm,int pageIndex, int pageSize, string sortField, string sortOrder)
        {

            CashTransactionGridFilterResult resultData = CashTransaction.PerformFilter(GlobalProperties.UserID,vm, pageIndex, pageSize, sortField, sortOrder);
            

            return Json(resultData);
        }

        public ActionResult AddRecord(TransactionType TrxType)
        {
            CashTransaction.AddRecord(GlobalProperties.MerchantID, TrxType);
            return Json(true);
        }

        //public ActionResult ApprovalAction(string TrxID,string ID, CashInOutAction Status)
        //{
        //    CashTransaction.ApprovalAction(GlobalProperties.UserName, TrxID, Status);
        //    return Json(true);
        //}

        public ActionResult DeleteRecord(CashInOutViewModel m)
        {
            CashTransaction.DeleteRecord(m, GlobalProperties.UserName, GlobalProperties.MerchantID);
            CashTransaction.RemoveTransactionFromCache(GlobalProperties.UserID,m);
            return Json(true, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ExportToExcel(string ExportProvider)
        {
            List<CashInOutViewModel> trxModel = CashTransaction.GetTransactionInfoCache(GlobalProperties.UserID);
           
            byte[] sfilebytes = Function.ExportToExcelCollection<CashInOutViewModel>(trxModel);
            string sfilename = "Transaction_" + DateTime.Today.ToString("dd_mm_yyyy") + ".xlsx";
            string filetype = "application/vnd.ms-excel";

            return File(sfilebytes, filetype, sfilename);
        }


        public ActionResult EditForm()
        {
            CashInOutSQLModel m = new CashInOutSQLModel();
            PrepareEditFormViewBag();
            ViewBag.ObjectID = 9;
            ViewBag.Title = "Add New Transaction";
            return View("_EditForm", m);

        }

        public ActionResult AddManualRecord(CashInOutSQLModel m)
        {
            AddManualRecordContainer res = CashTransaction.AddRecordManual(GlobalProperties.MerchantID, m);

           
            return Json(res.resultmsg);
        }


        private void PrepareEditFormViewBag()
        {
            List<SelectListItem> selectlist = new List<SelectListItem>();
            Dictionary<int, string> TransactionTypeDict = MGatewayProcessor.Helpers.DataReaderHelper.GetEnumDict(typeof(TransactionType));

            
            foreach (KeyValuePair<int, string> item in TransactionTypeDict)
            {
                selectlist.Add(new SelectListItem { Value = item.Key.ToString(), Text = item.Value });
            }
            ViewBag.TransactionTypeList = selectlist;

            selectlist = new List<SelectListItem>();
            Dictionary<int, string> WalletTypeDict = MGatewayProcessor.Helpers.DataReaderHelper.GetEnumDict(typeof(WalletType));

            
            foreach (KeyValuePair<int, string> item in WalletTypeDict)
            {
                selectlist.Add(new SelectListItem { Value = item.Key.ToString(), Text = item.Value });
            }
            ViewBag.WalletTypeList = selectlist;

        }







    }
}