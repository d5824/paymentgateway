﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Global.Model;
using MGatewayHelpers;
using MGatewayProcessor;
using MGatewayBO.Model;
namespace MGatewayBO.Controllers
{
    public class MerchantSetupController : Controller
    {
      
        public ActionResult Index()
        {
            ViewBag.ObjectID = 7;
            ViewBag.Title = "Merchant Setup";
            return ReloadMerchantList();
        }

        public ActionResult ReloadMerchantList()
        {
            JsGridModel m = PrepareMerchantList();
            return View(m);
        }

        private JsGridModel PrepareMerchantList()
        {
            JsGridModel m = MerchantSetup.GetMerchantGrid();
            GridControllerHelper.UpdateRouteURL(m, Request.RequestContext);
            return m;
        }

        public ActionResult UpdateMerchant(MerchantDataModel vm)
        {
            MerchantDataModel m = MerchantSetup.UpdateMerchantDetails(vm);
            return Json(m);
        }

        public ActionResult DeleteMerchant(MerchantDataModel vm)
        {
            MerchantSetup.DeleteMerchant(vm);
            return Json(true);
        }


        public ActionResult ViewMerchantDetails(MerchantDataViewModel vm)
        {
           
            return PartialView("_EditForm", vm);
        }




    }
}