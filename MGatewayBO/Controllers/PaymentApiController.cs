﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MGatewayHelpers;
using MGatewayProcessor;
using MGatewayBO.Model;
using Newtonsoft.Json;
namespace MGatewayBO.Controllers
{
  
    public class PaymentApiController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index(string data)
        {
            // string DecodedBase64string = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(data));

            CashInOutApiModel m = CashTransaction.GetCashInOutTransaction(data);
            ViewBag.RedirectURL = m.RedirectURL;
            //string _TokenID = LuckySpinApi.CreateToken(LicenseKey, UserName);
            if(m.Status ==  CashInOutAction.New)
            {
                if (m.TransactionType == TransactionType.CashOut)
                {

                    return View(m);
                }
                else if (m.TransactionType == TransactionType.CashIn)
                {
                    return View("_Withdrawal", m);
                }
           
            }
            return View("_InvalidTransaction", m);

            // Login.TestConnection();

        }
        [AllowAnonymous]
        public ActionResult Simulator()
        {
           
            //List<JsGridSelectItemModel> WalletList = GetDropDownList(typeof(MGatewayBO.Model.WalletType));
            Dictionary<int, string> WalletType = MGatewayProcessor.Helpers.DataReaderHelper.GetEnumDict(typeof(WalletType));
            List<SelectListItem> selectlist = new List<SelectListItem>();

            selectlist.Add(new SelectListItem { Value = "", Text = "" });
            foreach (KeyValuePair<int, string> item in WalletType)
            {
                selectlist.Add(new SelectListItem { Value = item.Key.ToString(), Text = item.Value });
            }
            ViewBag.WalletTypeList = selectlist;

            selectlist = new List<SelectListItem>();
            selectlist.Add(new SelectListItem { Value = "Deposit", Text = "Deposit" });
            selectlist.Add(new SelectListItem { Value = "Withdrawal", Text = "Withdrawal" });

            ViewBag.TransactionType = selectlist;



            List<string> NameList = new List<string>() { "Luella Wooten", "Amos Cartwright", "Phoebe Bate", "Ansh Wiggins", "Shannen Vaughan", "Aden Peck", "Lynn Gilmour", "Kylan Mcarthur", "Fenella Metcalfe", "Hussain Perez", "Tonicha Tait", "Mahi Oliver", "Virgil Berry", "Tymoteusz Webber", "Alyssa Dickson", "Henry Yoder", "Zena Atkins", "Stefano Brett", "Hamaad Rudd", "Alan Sherman" };
            Random rnum = new Random();
            ViewBag.DepositAmount = rnum.Next(500, 1000);
            ViewBag.PaymentMethod = rnum.Next(2);
            ViewBag.Name = NameList[rnum.Next(NameList.Count)];

            return View("_EditForm");
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ConfirmPayment(string ID, string MerchantID, string PlayerTransactionID, string RedirectURL, HttpPostedFileBase receipt)
        {
            List<string> resultmsg = new List<string>();
            if (PlayerTransactionID == null || PlayerTransactionID == string.Empty)
            {
                resultmsg.Add("Please Key in Transaction ID");
            }
            if (receipt == null)
            {
                resultmsg.Add("Please Upload Receipt");
            }

            //update cash in out
            PaymentApiStatus status = PaymentApiStatus.Waiting;
            if (resultmsg.Count == 0)
            {
                int FileNo = FileLibrary.SaveFileToLibrary(MerchantID, receipt.InputStream, receipt.FileName, receipt.ContentType, 0);
                status = CashTransaction.updateCashInOut(MerchantID, ID, PlayerTransactionID, FileNo, CashInOutAction.Pending);
            }
            
            if(status == PaymentApiStatus.Invalid)
            {
                resultmsg.Add("Invalid Transaction, Please contact customer service.");
            }
            return Json(new { msg = string.Join("\n", resultmsg), URL = Url.Action("ThankYou", "PaymentApi", new {msg  = status.GetEnumDescription(), RedirectURL  = RedirectURL }) });


        }


        [AllowAnonymous]
        public ActionResult ThankYou(string msg, string RedirectURL)
        {
            ViewBag.Msg = msg;
            ViewBag.RedirectURL = RedirectURL;
            return View("_ThankYou");
        }

        [AllowAnonymous]
        public ActionResult PaymentRequest(string data, string MerchantID)
        {
            string DecodedBase64string = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(data));
            DepositSandBoxModel m = JsonConvert.DeserializeObject<DepositSandBoxModel>(DecodedBase64string);
        

            CashInOutSQLModel vm = new CashInOutSQLModel();
            if(m.TransactionType == "Deposit")
            {
                APIIntegration.Model.DepositAPIModel DepositApi = PaymentApi.ConvertToDepositApi(m);
                vm  = Api.ConvertDeposit(MerchantID, DepositApi);
            }
            else
            {
                APIIntegration.Model.WithdrawalAPIModel WithdrawalApi = PaymentApi.ConvertToWithdrawalApi(m);
                vm = Api.ConvertWithdrawal(MerchantID, WithdrawalApi);
            }
            AddManualRecordContainer res = new AddManualRecordContainer();
            if(vm.WalletNumber == String.Empty && vm.TransactionType == TransactionType.CashOut)
            {
                res.transactionID = "";
                res.resultmsg = "no available payment agent at the moment.";
                return Json(res);
            }
            else
            {
               
                res = CashTransaction.AddRecordManual(MerchantID, vm, true);
                string UniqueKey = PaymentApi.InsertToApiMapping(MerchantID, res.transactionID, m.MerchantTransactionID,"","");
                res.transactionID = UniqueKey;
            }

            return Json(res);
        }

        [AllowAnonymous]
        public ActionResult ConnectApi(string ApiKey)
        {
            MerchantDataModel merchant = Api.GetMerchantIDByAPIKEY(ApiKey);
            
            return Json(merchant);
        }




    }
}