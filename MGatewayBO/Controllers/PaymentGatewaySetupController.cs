﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Global.Model;
using MGatewayHelpers;
using MGatewayProcessor;
using MGatewayBO.Model;
namespace MGatewayBO.Controllers
{
    public class PaymentGatewaySetupController : Controller
    {
      
        public ActionResult Index()
        {
            ViewBag.Title = "Payment Gateway Setup";
            return ReloadGatewayList();
        }

        public ActionResult ReloadGatewayList()
        {
            JsGridModel m = PrepareGatewayList();
            return View(m);
        }

        private JsGridModel PrepareGatewayList()
        {
            JsGridModel m = PaymentGatewaySetup.GetPaymentGatewayGrid();
            GridControllerHelper.UpdateRouteURL(m, Request.RequestContext);
            return m;
        }

        public ActionResult UpdateGateway(GatewayDataModel vm)
        {
            PaymentGatewaySetup.UpdateGatewayDetails(vm);
            return Json(true);
        }

        public ActionResult DeleteGateway(GatewayDataModel vm)
        {
            PaymentGatewaySetup.DeleteGateway(vm);
            return Json(true);
        }


        public ActionResult ViewGatewayDetails(GatewayDataModel vm)
        {
            return PartialView("_EditForm", vm);
        }



    }
}