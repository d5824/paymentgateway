﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using FirebaseAdmin;
using FirebaseAdmin.Messaging;
using Google.Apis.Auth.OAuth2;
using MGatewayProcessor;

namespace MGatewayBO.Controllers
{
    public class PushNotificationController : Controller
    {
        // GET: PushNotification
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult push(string title, string content, string token)
        {
            var registrationTokens = new List<string>()
            {
               token,
                // ...
                "fVsLaWURRNucbyK7FRG5A2:APA91bEOwPB6Rk6Xg1tdIO60qMn-c9ryKOsu9n29Yr5KZWQ1JwdCk1D5UMMU1haJRdar5OqY37Nu5isBliqk7lQJVzg9o5qhjtYib8BN-1AcPndi8euAuaziiKDniJ_sXUhRm40p9xGB",
            };

            _ = PushNotification.pushMessageAsync(title, content, registrationTokens);
           // PushNotification.SendNotification2(registrationTokens, content, title);

            return Json(true);
        }
    }
}