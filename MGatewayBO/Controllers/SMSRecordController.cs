﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Global.Model;
using MGatewayHelpers;
using MGatewayProcessor;
using MGatewayBO.Model;
namespace MGatewayBO.Controllers
{
    public class SMSRecordController : Controller
    {
      
        public ActionResult Index()
        {
            ViewBag.Title = "SMS";
            ViewBag.ObjectID = 5;
           
            
            DateTime currentMonth = DateTime.Today;
            DateTime StartDate = new DateTime(currentMonth.Year, currentMonth.Month, 1);
            DateTime EndDate = new DateTime(currentMonth.Year, currentMonth.Month, DateTime.DaysInMonth(currentMonth.Year, currentMonth.Month));
            string _StartDate = StartDate.ToString(GlobalProperties.gDisplayDateFormat);
            string _EndDate = EndDate.ToString(GlobalProperties.gDisplayDateFormat);
          
            return ReloadSMSList(_StartDate, _EndDate);
        }

        public ActionResult ReloadSMSList(string _StartDate, string _EndDate)
        {
            JsGridModel m = PrepareSMSList(_StartDate, _EndDate);
            GlobalSetingsViewModel gbl = GlobalSettings.GetSettings();
            ViewBag.TotalAmount = SMSRecord.GetTransactionTotalAmount(SMSRecord.GetTransactionInfoCache(GlobalProperties.UserID));
            ViewBag.DateRange = _StartDate + " - " + _EndDate;
            ViewBag.RefreshInterval = gbl.WalletMinList;

            List<SelectListItem> selectlist = new List<SelectListItem>();
            List<ShopModel> ShopList = Shop.GetShop(GlobalProperties.MerchantID);
            foreach (ShopModel item in ShopList)
            {
                selectlist.Add(new SelectListItem { Value = item.ShopID, Text = item.ShopName });
            }
            ViewBag.ShopList = selectlist;
            return View(m);
        }

        public ActionResult ReloadSMSListDate( string _StartDate, string _EndDate, List<string> ShopList)
        {
            TrxInfoGridReloadResult trxInfoGridReloadResult = new TrxInfoGridReloadResult();
            JsGridModel m = PrepareSMSList(_StartDate, _EndDate);
            List<SMSRecordViewModel> dataList = SMSRecord.GetTransactionInfoCache(GlobalProperties.UserID);
            if (ShopList != null)
            {
                List<SMSRecordViewModel> NewdataList = new List<SMSRecordViewModel>();
                foreach (SMSRecordViewModel item in dataList)
                {
                    if (ShopList.Contains(item.ShopID))
                    {
                        NewdataList.Add(item);
                    }
                }
                m.data = NewdataList;
                SMSRecord.UpdateTransactionInfoCache(GlobalProperties.UserID, NewdataList);

            }

            trxInfoGridReloadResult.jsGrid = m;
            trxInfoGridReloadResult.TotalAmount = SMSRecord.GetTransactionTotalAmount(SMSRecord.GetTransactionInfoCache(GlobalProperties.UserID ) );

            return Json(trxInfoGridReloadResult);
        }

     


        private JsGridModel PrepareSMSList(string _StartDate, string _EndDate)
        {
            JsGridModel m = SMSRecord.GetSMSRecordGrid(GlobalProperties.UserID,GlobalProperties.MerchantID, _StartDate, _EndDate);
            GridControllerHelper.UpdateRouteURL(m, Request.RequestContext);
            return m;
        }

        public ActionResult LoadSMSRecord(SMSRecordViewModel vm,int pageIndex, int pageSize, string sortField, string sortOrder)
        {

            SMSRecordGridFilterResult resultData = SMSRecord.PerformFilter(GlobalProperties.UserID,vm, pageIndex, pageSize, sortField, sortOrder);
            

            return Json(resultData);
        }


        public ActionResult AddRecord()
        {
            SMSRecord.AddRecord(GlobalProperties.MerchantID);
            return Json(true);
        }
       
       



    }
}