﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Global.Model;
using MGatewayHelpers;
using MGatewayProcessor;
using MGatewayBO.Model;
namespace MGatewayBO.Controllers
{
    public class ShopController : Controller
    {
      
        public ActionResult Index()
        {
            ViewBag.ObjectID = 1;
            ViewBag.Title = "Shop";
            return ShopList();
        }

        public ActionResult ShopList()
        {
            JsGridModel m = PrepareShopList();
            return View(m);
        }

        private JsGridModel PrepareShopList()
        {
            JsGridModel m = Shop.GetShopGrid(GlobalProperties.MerchantID);
            if(GlobalProperties.UserRole == "Merchant")
            {
                m.AllowEdit = false;
                m.AllowDelete = false;
                m.HasDetails = false;
            }
            GridControllerHelper.UpdateRouteURL(m, Request.RequestContext);
            return m;
        }

        public ActionResult UpdateShop(ShopModel vm)
        {
          string result = Shop.UpdateShopDetails(GlobalProperties.MerchantID,vm);
            return Json(result);
        }

        public ActionResult AddShop(ShopModel vm)
        {
            string result =  Shop.AddShopDetails(GlobalProperties.MerchantID, vm);

            return Json(result);
        }


        public ActionResult DeleteShop(ShopModel vm)
        {
            Shop.DeleteShop(vm);
            return Json(true);
        }


        public ActionResult ViewShopDetails(ShopViewModel vm)
        {
            //PrepareViewBag();
            return PartialView("_EditForm", vm);
        }

        public ActionResult WalletList(string ShopID, string ShopName)
        {
          
            JsGridModel m = Shop.GetWalletsGrid(ShopID, GlobalProperties.MerchantID);
            GridControllerHelper.UpdateRouteURL(m, Request.RequestContext);
            if (GlobalProperties.UserRole == "Merchant")
            {
                m.AllowAdd = false;
                m.AllowEdit = false;
                m.AllowDelete = false;
            }
            ViewBag.ShopName = ShopName;
            ViewBag.ShopID = ShopID;
            return PartialView("_Wallets",m);
        }

        public ActionResult WalletListMain()
        {
            ViewBag.ObjectID = 2;
            ViewBag.Title = "Wallet";
            JsGridModel m = Shop.GetWalletsGrid(GlobalProperties.MerchantID);
            m.AllowAdd = false;
            m.AllowEdit = false;
            m.AllowDelete = false;
            GridControllerHelper.UpdateRouteURL(m, Request.RequestContext);
         
            return View("_Wallets", m);
        }


        public ActionResult DeleteWallet(ShopWalletsModel vm)
        {
           Shop.DeleteWallet(vm);
            return Json(true);
        }

        public ActionResult AddWallet(ShopWalletsModel vm)
        {
            bool isAdded = Shop.AddWallet(vm);
            return Json(isAdded);
        }

        public ActionResult UpdateWallet(ShopWalletsViewModel vm)
        {
            bool isUpdated = Shop.UpdateWallet(vm);
            return Json(isUpdated);
        }








    }
}