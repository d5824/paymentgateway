﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Global.Model;
using MGatewayHelpers;
using MGatewayProcessor;
using MGatewayBO.Model;
namespace MGatewayBO.Controllers
{
    public class TransactionInfoController : Controller
    {
      
        public ActionResult Index()
        {
            ViewBag.Title = "Transaction Info";
            DateTime currentMonth = DateTime.Today;
            DateTime StartDate = new DateTime(currentMonth.Year, currentMonth.Month, 1);
            DateTime EndDate = new DateTime(currentMonth.Year, currentMonth.Month, DateTime.DaysInMonth(currentMonth.Year, currentMonth.Month));
            string _StartDate = StartDate.ToString(GlobalProperties.gDisplayDateFormat);
            string _EndDate = EndDate.ToString(GlobalProperties.gDisplayDateFormat);
           
            return ReloadTransactionList(_StartDate, _EndDate);
        }

        public ActionResult ReloadTransactionList(string _StartDate, string _EndDate)
        {
            JsGridModel m = PrepareTransactionList(_StartDate, _EndDate);
          
            ViewBag.TotalAmount = TransactionInfo.GetTransactionTotalAmount(TransactionInfo.GetTransactionInfoCache(GlobalProperties.UserID));
            ViewBag.DateRange = _StartDate + " - " + _EndDate;
            return View(m);
        }

        public ActionResult ReloadTransactionListDate(string _StartDate, string _EndDate)
        {
            TrxInfoGridReloadResult trxInfoGridReloadResult = new TrxInfoGridReloadResult();
            JsGridModel m = PrepareTransactionList(_StartDate, _EndDate);
           
            trxInfoGridReloadResult.jsGrid = m;
            trxInfoGridReloadResult.TotalAmount = TransactionInfo.GetTransactionTotalAmount(TransactionInfo.GetTransactionInfoCache(GlobalProperties.UserID) );

            return Json(trxInfoGridReloadResult);
        }

     


        private JsGridModel PrepareTransactionList(string _StartDate, string _EndDate)
        {
            JsGridModel m = TransactionInfo.GetTransactionInfoGrid(GlobalProperties.UserID, _StartDate, _EndDate);
            GridControllerHelper.UpdateRouteURL(m, Request.RequestContext);
            return m;
        }

        public ActionResult LoadTransactionInfo(TrxInfoViewModel vm,int pageIndex, int pageSize, string sortField, string sortOrder)
        {
            
            TrxInfoGridFilterResult resultData = TransactionInfo.PerformFilter(GlobalProperties.UserID,vm, pageIndex, pageSize, sortField, sortOrder);
            

            return Json(resultData);
        }

       
        public ActionResult DeleteMerchant(MerchantDataModel vm)
        {
            MerchantSetup.DeleteMerchant(vm);
            return Json(true);
        }


        public ActionResult ViewMerchantDetails(MerchantDataViewModel vm)
        {
            PrepareViewBag();
            return PartialView("_EditForm", vm);
        }

        public ActionResult AddGatewayMerchant(string MerchantID, string GatewayID) 
        {

            string serviceID = MerchantSetup.AddGatewayMerchant(MerchantID, GatewayID);
            return Json(serviceID);
        }

        public ActionResult DeleteGatewayMerchant(string ServiceID)
        {
            MerchantSetup.DeleteGatewayMerchant(ServiceID);
            return Json("");
        }
        private void PrepareViewBag()
        {
            List<SelectListItem> selectlist = new List<SelectListItem>();
            List<GatewayDataModel> GatewayList = PaymentGatewaySetup.GetAllPaymentGateway();
            selectlist.Add(new SelectListItem { Value = "", Text = "" });
            foreach (GatewayDataModel item in GatewayList)
            {
                selectlist.Add(new SelectListItem { Value = item.GatewayID, Text = item.GatewayName });
            }
            ViewBag.PaymentGatewayList = selectlist;
        }



    }
}