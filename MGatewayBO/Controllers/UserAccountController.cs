﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Global.Model;
using MGatewayHelpers;
using MGatewayProcessor;
using MGatewayBO.Model;
namespace MGatewayBO.Controllers
{
    public class UserAccountController : Controller
    {
      
        public ActionResult Index()
        {
            ViewBag.Title = "User Account";
            ViewBag.ObjectID = 6;
            return ReloadUserList();
        }

        private void PrepareViewBag()
        {
            List<SelectListItem> selectlist = new List<SelectListItem>();
            List<MerchantDataModel> GatewayList = MerchantSetup.GetAllMerchantByUserID(GlobalProperties.UserID);
            selectlist.Add(new SelectListItem { Value = "", Text = "" });
            foreach (MerchantDataModel item in GatewayList)
            {
                selectlist.Add(new SelectListItem { Value = item.merchantID, Text = item.merchantName });
            }
            ViewBag.MerchantList = selectlist;

            selectlist = new List<SelectListItem>();
            selectlist.Add(new SelectListItem { Value = "", Text = "" });
            selectlist.Add(new SelectListItem { Value = "Admin", Text = "Admin" });
            selectlist.Add(new SelectListItem { Value = "MerchantAdmin", Text = "MerchantAdmin" });
            selectlist.Add(new SelectListItem { Value = "Merchant", Text = "Merchant" });
            ViewBag.UserRoleList = selectlist;

        }

        public ActionResult ReloadUserList()
        {
            JsGridModel m = PrepareUserList();
            return View(m);
        }

        private JsGridModel PrepareUserList()
        {
            JsGridModel m = UserAccount.GetUserGrid(GlobalProperties.UserID);
            GridControllerHelper.UpdateRouteURL(m, Request.RequestContext);
            return m;
        }



        public ActionResult DeleteUser(UserLoginViewModel m)
        {
            UserAccount.DeleteUser(m);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateUser(UserLoginViewModel m)
        {

            bool isUpdate = UserAccount.UpdateUsersDetails(m);


            return Json(isUpdate, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewUserDetails(UserLoginViewModel vm)
        {
            PrepareViewBag();
            return PartialView("_EditForm", vm);
        }


    }
}