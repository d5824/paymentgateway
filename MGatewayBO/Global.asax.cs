﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using FirebaseAdmin;
using Google.Apis.Auth.OAuth2;
using MGatewayRepository;
namespace MGatewayBO
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            InitializeDBConnection();
            Initialize();
            string path = AppDomain.CurrentDomain.BaseDirectory + @"Content\pushNotificationKey.json";
            var cc = GoogleCredential.FromFile(path);
             FirebaseApp.Create(new AppOptions()
            {
                Credential = cc,
            });
            



        }

        protected void Initialize()
        {
            string domainPath = System.Configuration.ConfigurationManager.AppSettings.Get("DomainPath").Trim();
            if (domainPath.EndsWith("/"))
            {
                domainPath = domainPath.Remove(domainPath.Length - 1, 1);
            }
            MGatewayHelpers.GlobalProperties.DomainPath = domainPath;
        }

        private void InitializeDBConnection()
        {
            
            MySQLDB._DbName = System.Configuration.ConfigurationManager.AppSettings.Get("DBName");
            MySQLDB._Server = System.Configuration.ConfigurationManager.AppSettings.Get("ServerName");
        }
    }
}
