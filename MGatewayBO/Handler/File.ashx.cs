﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
namespace LuckyDraw.Handler
{
    /// <summary>
    /// Summary description for File
    /// </summary>
    public class File : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //if (context.Request.QueryString["f"] != null)
            //{
            //int FileNo = TicketingProcessor.Helper.DataReaderHelper.GetFormInt(context.Request.QueryString["f"]);
            string APkName = System.Configuration.ConfigurationManager.AppSettings.Get("apkName");
            var fileNameAndPath = AppDomain.CurrentDomain.BaseDirectory + @"apk\" + APkName;
                FileInfo file = new FileInfo(fileNameAndPath);
                file.Refresh();
                if (file.Exists)
                {

                    context.Response.Clear();
                    context.Response.ContentType = "application/vnd.android.package-archive";
                    context.Response.AddHeader("Content-Disposition",
                        "attachment; filename= " + "GenkinPay.apk" + "; size=" + file.Length.ToString());
                    context.Response.TransmitFile(fileNameAndPath);
                    context.Response.Flush();
                    context.Response.End();


                    //if (m != null)
                    //{
                    //    context.Response.AddHeader("content-disposition", "attachment;filename=\"" + m.FileName + "\"");
                    //    context.Response.ContentType = m.FileType;
                    //    context.Response.OutputStream.Write(m.FileContent, 0, m.FileSize);

                    //}
                    //else
                    //{
                    //    context.Response.ContentType = "text/html";
                    //    context.Response.Write("File Not Found");
                    //}
                }
                else
                {
                    context.Response.ContentType = "text/html";
                    context.Response.Write("File Not Found");
                }
            //}

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}