﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.SessionState;
namespace LuckyDraw.Handler
{
    /// <summary>
    /// Summary description for Picture
    /// </summary>
    public class ViewReceipt : IHttpHandler, IReadOnlySessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.QueryString["f"] != null)
            {
                int FileNo = MGatewayProcessor.Helpers.DataReaderHelper.GetFormInt(context.Request.QueryString["f"]);

                if (FileNo != 0)
                {
                    Global.Model.FileLibraryModel m = MGatewayProcessor.FileLibrary.Find(FileNo);
                    if (m != null)
                    {
                        context.Response.ContentType = m.FileType;
                        context.Response.OutputStream.Write(m.FileContent, 0, m.FileSize);
                    }
                }
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}