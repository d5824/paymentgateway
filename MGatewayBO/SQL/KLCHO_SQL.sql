DELIMITER $$
DROP PROCEDURE IF EXISTS `spAddColumn`$$
CREATE DEFINER=`RDAdmin`@`%` PROCEDURE `spAddColumn`(

in _TableName varchar(50),
in _ColumnName varchar (50),
in _ColumnType varchar(50)
)
BEGIN

SET @tablename = _TableName;
SET @columnname = _ColumnName;
SET @columntype = _ColumnType;
SET @preparedStatement = (SELECT IF(
  (
    SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
    WHERE
      (table_name = @tablename)
      AND (column_name = @columnname)
  ) > 0,
  "SELECT true as ColumnExists",
  CONCAT("ALTER TABLE ", @tablename, " ADD ", @columnname, " ", @columntype, ";")
));
PREPARE alterIfNotExists FROM @preparedStatement;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;
END$$
DELIMITER ;


DELIMITER $$
DROP PROCEDURE IF EXISTS `spAlterColumn`$$
CREATE DEFINER=`RDAdmin`@`%` PROCEDURE `spAlterColumn`(

in _TableName varchar(50),
in _ColumnName varchar (50),
in _ColumnType varchar(50)
)
BEGIN

SET @tablename = _TableName;
SET @columnname = _ColumnName;
SET @columntype = _ColumnType;
SET @preparedStatement = (SELECT IF(
  (
    SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
    WHERE
      (table_name = @tablename)
      AND (column_name = @columnname)
  ) > 0,
   CONCAT("ALTER TABLE ", @tablename, " MODIFY ", @columnname, " ", @columntype, ";"),
 "SELECT 1 as ColumnNotExists"
));
PREPARE alterIfNotExists FROM @preparedStatement;
EXECUTE alterIfNotExists;
DEALLOCATE PREPARE alterIfNotExists;
END$$
DELIMITER ;

Create TABLE IF NOT EXISTS tbl_UserLogin(
UserID varchar(50) not null,
UserName varchar(50) not null,
UserPassword varchar(500) not null,
Email varchar(200) not null,
LastLoginDateTime DateTime not null,
FailedLoginAttempt int not null,
AccountStatus int not null,
CreatedDate DateTime not null,
CONSTRAINT PK_tbl_UserLogin PRIMARY KEY (UserID)
);


CREATE TABLE `tbl_Campaign` (
  `UserID` varchar(50) NOT NULL,
  `CampaignName` varchar(200) NOT NULL,
  `CampaignTitle` varchar(500) NOT NULL,
  `BackgroundFileNo` int NOT NULL,
  `CampaignStartDate` datetime DEFAULT NULL,
  `CampaignEndDate` datetime DEFAULT NULL,
  `DrawDate` datetime DEFAULT NULL,
  `CustomMessage` varchar(4000) NOT NULL,
  `URL` varchar(500) NOT NULL,
  `Token` varchar(50) NOT NULL,
  `Status` int NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `LinkToken` varchar(50) NOT NULL,
  `LogoFileNo` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`UserID`,`CampaignName`),
  KEY `CampaignName` (`CampaignName`),
  CONSTRAINT `FK_tbl_Campaign_tbl_UserLogin` FOREIGN KEY (`UserID`) REFERENCES `tbl_UserLogin` (`UserID`) ON DELETE CASCADE
);


Create TABLE IF NOT EXISTS tbl_CampaignPrize(
ID int AUTO_INCREMENT not null,
UserID varchar(50) not null,
CampaignName varchar(200) not null,
Prize varchar(50) not null,
unit int not null,
CONSTRAINT PK_tbl_CampaignPrize PRIMARY KEY (ID),
CONSTRAINT FK_tbl_CampaignPrize_tbl_Campaign foreign key (UserID,CampaignName) references tbl_Campaign(UserID,CampaignName) ON DELETE CASCADE
);


Create TABLE IF NOT EXISTS tbl_CampaignUsers(
UserID varchar(50) not null,
CampaignName  varchar(200) not null,
UserName varchar (200) not null,
EntryCount int not null,
CONSTRAINT PK_tbl_CampaignUsers PRIMARY KEY (UserID,CampaignName,UserName),
CONSTRAINT FK_tbl_CampaignUsers_tbl_Campaign foreign key (UserID,CampaignName) references tbl_Campaign(UserID,CampaignName) ON DELETE CASCADE
);


Create TABLE IF NOT EXISTS tbl_CampaignWinners(
UserID varchar(50) not null,
CampaignName  varchar(200) not null,
Prize varchar (50) not null,
UserName varchar (200) not null,
CONSTRAINT PK_tbl_CampaignWinners PRIMARY KEY (UserID,CampaignName,UserName,Prize),
CONSTRAINT FK_tbl_CampaignWinners_tbl_Campaign foreign key (UserID,CampaignName) references tbl_Campaign(UserID,CampaignName) ON DELETE CASCADE
);


Create TABLE IF NOT EXISTS tbl_FileLibrary(
FileNo int not null AUTO_INCREMENT,
FileName varchar (200) not null,
FileSize int not null,
FileType varchar(50) not null,
UploadBy varchar(50) not null,
UploadDate datetime,
FileContent mediumblob not null,
CONSTRAINT PK_tbl_FileLibrary PRIMARY KEY (FileNo)
);

ALTER TABLE tbl_CampaignPrize ADD COLUMN isDrawed TINYINT(1) default 0;

DELIMITER $$
DROP PROCEDURE IF EXISTS `spUpdateParticipantList`$$
CREATE PROCEDURE `spUpdateParticipantList`(
in _jsonData text,
in _CampaignName varchar(200)
)
BEGIN
DROP TEMPORARY TABLE IF EXISTS tempJSONPList;
CREATE TEMPORARY TABLE tempJSONPList select * from JSON_TABLE(
_jsonData,
"$[*]" COLUMNS(
UserID VARCHAR(50) PATH "$.UserID",
CampaignName VARCHAR(200) PATH "$.CampaignName",
UserName VARCHAR(200) PATH "$.Username",
EntryCount int PATH "$.EntryCount")
) AS tmptbl;

delete from tbl_CampaignWinners where CampaignName = _CampaignName;
delete from tbl_CampaignUsers where CampaignName = _CampaignName;
update tbl_CampaignPrize set isDrawed = 0 where CampaignName = _CampaignName;

insert into tbl_CampaignUsers select t2.UserID,t1.CampaignName,t1.UserName,t1.EntryCount 
from tempJSONPList t1 
inner join tbl_Campaign t2 on t2.CampaignName=t1.CampaignName;

select * from tbl_CampaignUsers where CampaignName = _CampaignName;
END$$
DELIMITER ;
  
DELIMITER $$
DROP PROCEDURE IF EXISTS `spGETParticipantList`$$
CREATE PROCEDURE `spGETParticipantList`(
in _CampaignName varchar(200)
)
BEGIN
select * from tbl_CampaignUsers where CampaignName = _CampaignName;
END$$
DELIMITER ;
  
DELIMITER $$
DROP PROCEDURE IF EXISTS `spWinnerListUpdate`$$
CREATE PROCEDURE `spWinnerListUpdate`(
in _jsonData text,
in _CampaignName varchar(200)
)
BEGIN
DROP TEMPORARY TABLE IF EXISTS tempJSONWList;
CREATE TEMPORARY TABLE tempJSONWList select * from JSON_TABLE(
_jsonData,
"$[*]" COLUMNS(
UserID VARCHAR(50) PATH "$.UserID",
CampaignName VARCHAR(200) PATH "$.CampaignName",
Prize VARCHAR(50) PATH "$.Prize",
UserName VARCHAR(200) PATH "$.Username"
)) AS tmptbl;

insert into tbl_CampaignWinners select t2.UserID,t1.CampaignName,t1.Prize,t1.UserName 
from tempJSONWList t1 
inner join tbl_Campaign t2 on t2.CampaignName=t1.CampaignName;

END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS `spWinnerListGET`$$
CREATE PROCEDURE `spWinnerListGET`(
in _CampaignName varchar(200),
in _isDrawed TINYINT(1)
)
BEGIN
select * from tbl_CampaignWinners t1 
inner join tbl_CampaignPrize t2 on t2.CampaignName = t1.CampaignName and t2.Prize=t1.Prize and t2.isDrawed = _isDrawed
where t1.CampaignName = _CampaignName;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS `spCampaignPrizeDrawed`$$
CREATE PROCEDURE `spCampaignPrizeDrawed`(
in _CampaignName varchar(200),
in _Prize varchar(50)
)
BEGIN
Update tbl_CampaignPrize set isDrawed = 1 where CampaignName = _CampaignName and Prize = _Prize;

END$$
DELIMITER ;





Select * from tbl_FileLibrary
Select * from tbl_CampaignPrize
Select COLUMN_NAME,DATA_TYPE,IS_NULLABLE from Information_Schema.columns
Select * from tbl_UserLogin
insert into tbl_UserLogin values ('3acf59de94504865bfa954deb0136a40','admin','b5218ed697caaaef3e38e797aea2a799037c63c660479a0be37a8b2fa4722d68e0f3a105ca8452f988c1871c760ada7556e4cf371656800020e81ef6b33acf9b','admin_mymarketingarm.com',now(),0,1,now())
Select * from Information_Schema.columns where Table_name = 'tbl_UserLogin'

Select * from tbl_Campaign


