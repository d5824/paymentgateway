
DELIMITER $$
DROP PROCEDURE IF EXISTS `spDeleteParticipantList`$$
CREATE PROCEDURE `spDeleteParticipantList`(
in _CampaignName varchar(200),
in _UserID varchar(50)
)
BEGIN

delete from tbl_CampaignWinners where CampaignName = _CampaignName and UserID = _UserID;
delete from tbl_CampaignUsers where CampaignName = _CampaignName and UserID = _UserID;
update tbl_CampaignPrize set isDrawed = 0 where CampaignName = _CampaignName and UserID = _UserID;

END$$
DELIMITER ;
  