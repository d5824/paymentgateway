DELIMITER $$
DROP PROCEDURE IF EXISTS `spWinnerListUpdate`$$
CREATE PROCEDURE `spWinnerListUpdate`(
in _jsonData text,
in _CampaignName varchar(200),
in _UserID varchar(50)
)
BEGIN
DROP TEMPORARY TABLE IF EXISTS tempJSONWList;
CREATE TEMPORARY TABLE tempJSONWList select * from JSON_Ttbl_CampaignUsersABLE(
_jsonData,
"$[*]" COLUMNS(
UserID VARCHAR(50) PATH "$.UserID",
CampaignName VARCHAR(200) PATH "$.CampaignName",
Prize VARCHAR(50) PATH "$.Prize",
UserName VARCHAR(200) PATH "$.Username"
)) AS tmptbl;

insert into tbl_CampaignWinners select t2.UserID,t1.CampaignName,t1.Prize,t1.UserName 
from tempJSONWList t1 
inner join tbl_Campaign t2 on t2.CampaignName=t1.CampaignName and t2.UserID = t1.UserID;

END$$
DELIMITER ;