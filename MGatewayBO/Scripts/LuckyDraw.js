﻿


var _func = {
    ajxNew: function () {
        var ajxVal =
        {
            type: 'POST',
            url: '',
            data: '',
            cache: false,
            async: true,
            processData : true,
            contentType: 'application/x-www-form-urlencoded; charset=utf-8'
        }

        return ajxVal;
    },
    ajxCall: function (ajxVal, _returnAction) {
        debugger;
        var returnVal;
        $.ajax({
            type: ajxVal.type,
            url: ajxVal.url,
            data: ajxVal.data,
            cache: ajxVal.cache,
            async: ajxVal.async,
            processData: ajxVal.processData,
            contentType: ajxVal.contentType,
            beforeSend: function () {
                $("#loadingscreen").show();
            },
           
        }).done(function (resdata) {
            var returnRes = { 'result': 'success', 'returnVal': resdata };
            _returnAction(returnRes);
            debugger;
            $('#loadingscreen').hide();
        }).fail(function (jqXHR, textStatus, errorThrown) {
            var result = { 'xhr': jqXHR, 'textStatus': textStatus, 'errorThrown': errorThrown };
            returnVal = { 'result': 'fail', 'returnVal': result };
            console.log(returnVal);
            debugger;
            $('#loadingscreen').hide();
        });

    },
    errorShow: function (title, message) {
        $('#__errorTitle').text(title);
        $('#__errorMsg').text(message);
        $('#__divError').show();
        $('#__divcontent').hide();
    },
    errorHide: function () {
        $('#__divError').hide();
        $('#__divcontent').show();
    },

};


$(function () {

    InitDateRangePicker('.txt_daterange');
    InitDateRangeTimePicker('.txt_daterangetime');
    InitDatePicker('.txt_date');
    InitDateTimePicker('.txt_datetime');
    InitNumeric('.txt_num');
    InitMultiSelect('.multiselect');
   // InitNumericContainer('.txt_numContainer');
   

});


var InitMultiSelect = function (selector) {
    $(selector).select2({
        placeholder: '',
        allowClear: true,
        tokenSeparators: [','],
        closeOnSelect: false,
        minimumResultsForSearch: 10

        //dropdownCssClass: '',
        //selectionCssClass: ''
    });

    AssignMultiSelectValue(selector);
    //Initiate validation

}

var AssignMultiSelectValue = function (selector) {
    $.each($(selector), function (i, v) {
        var selectedItem = $(v).data('selecteditem');
        if (selectedItem != undefined && selectedItem != '') {
            $(v).val(selectedItem.split(';'));
            $(v).trigger('change');
        }

        /*names.split(',')*/
    });
}

var InitNumeric = function (selector) {
    
    $(selector).on("blur.NumericPlugin", function () {
        $(this).val(GetNumber($(this).val()));
    })
};

var InitNumericContainer = function (selector) {
    var numInput = $(selector).find('input')
    $.each(numInput, function (i, v) {
        $(v).on("blur.NumericPlugin", function () {
            $(this).val(GetNumber($(this).val()));
        })
    })
   
};

var GetNumber = function (value) {
    var replacedVal = value.toString().replace(/,/g, '');
    var num = parseFloat(replacedVal);

    if (isNaN(num)) {
        return 0;
    } else {
        return num;
    }
};

var InitDateRangePicker = function (selector) {
    $(selector).daterangepicker({
        autoUpdateInput: true,
        applyButtonClasses: 'btnprimary',
        autoApply:true,
        locale: {
            format: "DD/MM/YYYY",
            cancelLabel: 'Clear'
        },

    });

    $(selector).on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    });

    $(selector).on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
};

var InitDateRangeTimePicker = function (selector) {
    $(selector).daterangepicker({
        autoUpdateInput: true,
        applyButtonClasses: 'btn-primary',
        autoApply: true,
        timePicker: true,
        locale: {
            format: "DD/MM/YYYY HH:mm",
            cancelLabel: 'Clear'
        },

    });

    $(selector).on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY HH:mm') + ' - ' + picker.endDate.format('DD/MM/YYYY HH:mm'));
    });

    $(selector).on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
};


var InitDatePicker = function (selector) {

    $(selector).datetimepicker({
      
        format: 'DD/MM/YYYY'
      
    });


};

var InitDateTimePicker = function (selector) {

    $(selector).datetimepicker({
      
        icons:
        {
           
            up: 'fa fa-angle-up',
            down: 'fa fa-angle-down'
        },
        format: 'DD/MM/YYYY HH:mm',

        sideBySide: true
      
    });


};


var showMessage = function (msg, isSuccess, _timeOut) {
    var timeOut = 1500;
   
    if (_timeOut != undefined) {
        timeOut = +_timeOut;
    }
    toastr.options.timeOut = timeOut
    toastr.options.hideDuration = 1000;
    toastr.options.showDuration = 300
    toastr.options.extendedTimeOut = 1000
    toastr.options.positionClass = "toast-top-center";
    if (isSuccess) {
        toastr.success(msg)
    } else {
     
        toastr.error(msg);
    
    }
}

var ShowConfirm = function (msg, OkCallBack, CancelCallback) {
    toastr.options.timeOut = 0
    toastr.options.extendedTimeOut = 0
    toastr.options.positionClass = "toast-top-center";
    toastr.options.hideDuration = 100
    toastr.options.showDuration = 100
    var elem = $('<div>')
    /*toastr["error"]('<div><input class="input - small" value="textbox"/>&nbsp;<a href="http://johnpapa.net" target="_blank">This is a hyperlink</a></div><div><button type="button" id="okBtn" class="btn btn-primary">Close me</button><button type="button" id="surpriseBtn" class="btn" style="margin: 0 8px 0 8px">Surprise me</button></div>');*/
    $(elem).append(msg + '<br />');
    
    var Okelem = $("<button>").attr({
        class: "btn btn-light",
        style: 'width: 100px; margin-right:5px'
    }).append('Yes').click(function (e) {
        if (OkCallBack) {
            OkCallBack();
        }
    });

    var Cancelelem = $("<button>").attr({
        class: "btn btn-light",
        style: 'width: 100px; margin-right:5px'
    }).append('No').click(function (e) {
        if (CancelCallback) {
            CancelCallback();
        }
    });


    $(elem).append(Okelem).append(Cancelelem)

    toastr["info"](elem);
    

}

//$(document).click(function (e) {
//    if ($(e.target.closest('#toast-container')).length == 0) {
//        $('#toast-container').remove();
//    }
    
//})