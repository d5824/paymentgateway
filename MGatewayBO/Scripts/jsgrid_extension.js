﻿//Still got a lot need to improve, master key and master item not really good handling.

(function ($, window) {
    var MasterKey;
    var MasterItem;
    var IniGrid = function (element, data, customItemTemplate, masterKey) {
        var GridLayout = GetDefault(element)
        UpdateGridLayout(GridLayout, data, customItemTemplate);
        MasterItem = masterKey;
        $(element).jsGrid(GridLayout);
        // return new Chart($(element), data);
    }

    function GetDefault(element) {
        var GridLayout = {
            width: "100%",
            height: "auto",
            inserting: false,
            editing: false,
            filtering: true,
            sorting: true,
            paging: true,
            pageIndex: 1,
            pagerContainer: "#externalPager",
            pageSize: 13,
            pageButtonCount: 8,
            pagerFormat: "  Page {pageIndex} of {pageCount} ( {itemCount} items ) {first} {prev} {pages} {next} {last}  ",
            pageLoading: false,
            confirmDeleting: false,
            onItemDeleting: function (args) {
                if (!args.item.deleteConfirmed) { // custom property for confirmation
            args.cancel = true; // cancel deleting
               ShowConfirm('Are you sure you want to delete this record?', function () {
                   args.item.deleteConfirmed = true;
                   $("#loadingscreen").show();
               $(element).jsGrid('deleteItem', args.item); //call deleting once more in callback
            });
                 }
            },
           // deleteConfirm: 'Are you sure you want to delete this record?',
            data: [],
            //rowClass: function (item, itemIndex) {
            //    return "testrowcolor"
            //},
            fields: [],
            controller: {

            },
            onItemInserted: function (args) {

                // Check for duplicates in database.
                var myEvent = args.item;
                args.cancel = true;
                //var isDuplicate = checkNewEvent(myEvent).done(function (r) {
                //    if (r.Id) {
                //        // Event with same date was found.
                //        alert("There is already an event scheduled for this date.");
                //        args.cancel = true;
                //    }
                //}).fail(function (x) {
                //    alert("request to server failed"); // Tell the user something bad happened
                //    args.cancel = true;
                //})
            },

            onItemInserting: function (args) {
                //        var myEvent = args.item;
                //args.cancel = true;
            },

            onDataLoading: function (args) {
                $("#loadingscreen").show();
            },    // before controller.loadData
            onDataLoaded: function (args) {
                $('#loadingscreen').hide();
            },     // on done of controller.loadData
            
        }

        return GridLayout
    }

    function UpdateGridLayout(GridLayout, Options, customItemTemplate) {
        //jsGrid.ControlField.prototype.editButtonClass = "jsgrid-search-button";
        //jsGrid.ControlField.prototype.deleteButtonClass = "jsgrid-search-button";
        var itemTemplate = function (value, item) {
            var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
            //var $myButton = $("<button>test</button>");
            var $customEditButton = $("<button>").attr({ class: "customGridEditbutton jsgrid-button jsgrid-search-button" })
                .click(function (e) {
                    MasterItem = item;
                    var callback = function (data) {
                        //to handle if populate grid or normal form
                        $('#' + Options.ViewCallBack.childGridID).html(data);
                        $('#' + Options.ViewCallBack.Parentelem).hide();
                        $('#' + Options.ViewCallBack.childelem).show();

                    }
                    AjaxCallFunction(Options.ViewURL.URL, item, e, callback);
                    e.stopPropagation();

                });

            return $result.add($customEditButton);


        }

        var ControlFields = { type: "control", editButton: false, deleteButton: false, width: 80, clearFilterButton: false, modeSwitchButton: false };
        MasterKey = Options.KeyFieldName;
        if (Options.HasDetails) {
            ControlFields.itemTemplate = itemTemplate
            MasterKey = Options.KeyFieldName;
        }

        if (Options.AllowAdd) {
            GridLayout.inserting = true;
        }
        if (Options.AllowEdit) {
            GridLayout.editing = true;
            ControlFields.editButton = true;
        }
        if (Options.AllowDelete) {
            //GridLayout.inserting = true;
            ControlFields.deleteButton = true
        }
        if (Options.AllowDelete || Options.AllowEdit || Options.HasDetails) {
            GridLayout.fields.push(ControlFields);
        }
        
        GridLayout.data = Options.data;
        // GridLayout.fields.push(Options.fields);
        $.each(Options.fields, function (i, v) {
            if (customItemTemplate != undefined) {
                if (customItemTemplate[v.name] != undefined) {
                    v.itemTemplate = customItemTemplate[v.name];
                }
            }
            //if (v.isLink) {
            //    v.itemTemplate = function (value) {
            //        var elem = $('<div>')
            //        var hyperLink = $("<a>").attr("href", value).text(value);
            //        var buttonelem = $("<button>").attr({ class: "customGridEditbutton jsgrid-button jsgrid-search-button" }).click(function (e) {

            //            var $temp = $("<input>");
            //            $("body").append($temp);
            //            $temp.val($(this).closest('td').text()).select();
            //            document.execCommand("copy");
            //            $temp.remove();

            //        })
            //        elem.append(hyperLink)
            //       // elem.append(buttonelem);
            //        return elem;
            //    }
            //}
            GridLayout.fields.push(v);
        });


        if (Options.InsertURL.URL != null && Options.InsertURL.URL != '') {
            var InsertItem = function (item) {
                var d = $.Deferred();
                getMasterKeyValue(item);
                var isSuccess = { result: false };

                AjaxCallFunction(Options.InsertURL.URL, item, isSuccess);
                if (isSuccess.result == false) {
                    alert('duplicate value');
                    d.reject();
                    return d.promise();
                }

            }
            GridLayout.controller.insertItem = InsertItem;
        }

        if (Options.UpdateURL.URL != null && Options.UpdateURL.URL != '') {
            var UpdateItem = function (item) {
                var d = $.Deferred();
                var isSuccess = { result: false };
                getMasterKeyValue(item);
                AjaxCallFunction(Options.UpdateURL.URL, item, isSuccess);
            }
            GridLayout.controller.updateItem = UpdateItem;
        }

        if (Options.DeleteURL.URL != null && Options.DeleteURL.URL != '') {
          
            var DeleteItem = function (item) {
                $("#loadingscreen").show();
                var d = $.Deferred();
                var isSuccess = { result: false };
                getMasterKeyValue(item);
                AjaxCallFunction(Options.DeleteURL.URL, item, isSuccess);
            }
            GridLayout.controller.deleteItem = DeleteItem;
        }


        function getMasterKeyValue(item) {
            var mKey = MasterKey.split(',')
            for (var key in MasterItem) {
                if (mKey.includes(key)) {
                    item[key] = MasterItem[key]
                }
            }
        }

        var LoadData = function (filter) {
            return $.grep(this.data, function (item) {
                var result = true;
                for (var key in filter) {
                    var value = filter[key];
                    if (value != undefined && value.length > 0) {
                        if (item[key].toString().toLowerCase().indexOf(value.toLowerCase()) == -1) {
                            result = false;
                            return false;
                        }

                    }
                }

                return result;
            });
        }

        var serverSideLoadData = function (filter) {
           
            var startIndex = (filter.pageIndex - 1) * filter.pageSize;

            var ajxNew = new _func.ajxNew();
            ajxNew.url = Options.LoadDataURL.URL;
            ajxNew.data = filter;
            ajxNew.async = true; //duno why i put to false by default
            var d = $.Deferred();
            _func.ajxCall(ajxNew, function (returndata) {
                var data = returndata.returnVal;

                da = {
                    data: data.data,
                    itemsCount: data.itemsCount
                };

                if (typeof CustomFilterCallback != 'undefined') {
                    if (CustomFilterCallback != undefined) {
                        CustomFilterCallback(data);
                    }
                }

                d.resolve(da);

            })
            return d.promise();


            
        }

        if (Options.ServerSideLoading) {
            GridLayout.pageLoading = true;
            GridLayout.autoload = true;
            GridLayout.controller.loadData = serverSideLoadData;
        } else {
            GridLayout.controller.loadData = LoadData
        }
        GridLayout.controller.data = GridLayout.data;
       
    }

    function AjaxCallFunction(URL, data, isSuccess, callback) {


        var ajxNew = new _func.ajxNew();
        ajxNew.url = URL;
        ajxNew.data = data;
        ajxNew.async = false; //duno why i put to false by default
        _func.ajxCall(ajxNew, function (returndata) {
            var data = returndata.returnVal;
            isSuccess.result = data;
            if (callback !== undefined) {
                callback(data);
            }
            if (typeof CustomPostCallback != 'undefined') {
                if (CustomPostCallback != undefined) {
                    CustomPostCallback();
                }
            }

        })
    }
    $.fn.IniJsGrid = function (data, customItemTemplate, MasterKey) {
        if (this.length === 1) {
            IniGrid(this, data, customItemTemplate, MasterKey)
        }
    }


})(jQuery,window)