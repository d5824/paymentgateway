﻿var Validation = function (formSelector) {
    $(formSelector).validate({
        submitHandler: function (form) {
            return true;
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");

            // Add `has-feedback` class to the parent div.form-group
            // in order to add icons to inputs
            element.parents(".col-sm-5").addClass("has-feedback");

            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }

            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if (!element.next("span")[0]) {
                $("<span class='glyphicon glyphicon-remove form-control-feedback'></span>").insertAfter(element);
            }
        },
        success: function (label, element) {
            // Add the span element, if doesn't exists, and apply the icon classes to it.
            if (!$(element).next("span")[0]) {
                $("<span class='glyphicon glyphicon-ok form-control-feedback'></span>").insertAfter($(element));
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid").removeClass("is-valid");
            //$(element).parents(".input-wrapper").addClass("has-error").removeClass("has-success");
            //$(element).next("span").addClass("glyphicon-remove").removeClass("glyphicon-ok");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).addClass("is-valid").removeClass("is-invalid");
            //$(element).parents(".input-wrapper").addClass("has-success").removeClass("has-error");
            //$(element).next("span").addClass("glyphicon-ok").removeClass("glyphicon-remove");
        }
    });
}


var Validate = function (formSelector) {

    validateinput = $(formSelector).find('[data-validate]');
    validate = true;
    $.each(validateinput, function (i, v) {
        var elem = $(v);

        if (elem.val() == '') {
            elem.removeClass('is-valid').addClass('is-invalid');
            addValidateEvent(elem);
            validate = false;
        } else {
            if (elem.attr('id') == 'Email') {
                var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                if (emailReg.test(elem.val()) == false) {
                    addValidateEvent(elem);
                    elem.removeClass('is-valid').addClass('is-invalid');
                    validate = false;
                } else {
                    elem.removeClass('is-invalid').addClass('is-valid');
                }
            } else {
                elem.removeClass('is-invalid').addClass('is-valid');
            }


        }

    })

    return validate;
}

function addValidateEvent(elem) {
    $(elem).blur(function () {
        addValidationClass(elem);
      
    });


    $(elem).on('apply.daterangepicker', function (ev, picker) {
        addValidationClass(elem);
     

    });


    $(elem).change(function () {
        addValidationClass(elem);
    
    });
}

function addValidationClass(elem) {
    if ($(elem).val() == '') {
        elem.removeClass('is-valid').addClass('is-invalid');
    } else {

        if (elem.attr('id') == 'Email') {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            if (emailReg.test(elem.val()) == false) {
                elem.removeClass('is-valid').addClass('is-invalid');

            } else {
                elem.removeClass('is-invalid').addClass('is-valid');
            }
        } else {
            elem.removeClass('is-invalid').addClass('is-valid');
        }


        //elem.removeClass('is-invalid').addClass('is-valid');
    }

}


