﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using Newtonsoft.Json;
using System.Dynamic;
namespace APIIntegration.Model
{
    public class APIModel
    {

        [StringLength(100)]
        public string PlayerUserName { get; set; }

        [StringLength(100)]
        public string AccountNumber { get; set; }

        [StringLength(100)]
        public MGatewayBO.Model.WalletType PaymentGateway { get; set; }

        [StringLength(100)]
        public string Amount { get; set; }

        [StringLength(100)]
        public string NotifyURL { get; set; }

        [StringLength(100)]
        public string RedirectURL { get; set; }

        public string Hash { get; set; }
    }

    public class DepositAPIModel : APIModel
    {
        [StringLength(100)]
        public string merchantDepositId { get; set; }
    }

    public class WithdrawalAPIModel : APIModel
    {
        [StringLength(100)]
        public string merchantWithdrawalId { get; set; }
    }


    public class ApiResultModel
    {

        public string status { get; set; }

        public string message { get; set; }

        public dynamic data { get; set; }

        public ApiResultModel()
        {
            data = new ExpandoObject();
        }
    }

    public class MD5ResultModel
    {
        public string md5 { get; set; }

        public string Hash { get; set; }
    }




}
