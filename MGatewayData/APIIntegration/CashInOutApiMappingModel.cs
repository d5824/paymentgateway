﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using Newtonsoft.Json;
namespace APIIntegration.Model
{
    public class CashInOutApiMappingModel
    {
        [Key]
        [StringLength(50)]
        public string UniqueKey { get; set; }

        [StringLength(50)]

        public string TrxID { get; set; }

        [StringLength(50)]
        public string MerchantID { get; set; }

        public string MerchantTrxID { get; set; }

        public string NotifyURL { get; set; }

        public string RedirectURL { get; set; }

        public DateTime CreatedDateTime { get; set; }


    }





}
