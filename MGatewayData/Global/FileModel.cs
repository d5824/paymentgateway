﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Global.Model
{
    public class FileLibraryModel
    {
        [Key]
        public int FileNo { get; set; }

        [StringLength(200)]
        public string FileName { get; set; }
        public int FileSize { get; set; }
        [StringLength(50)]
        public string FileType { get; set; }
        [StringLength(50)]
        public string UploadBy { get; set; }
        public DateTime UploadDate { get; set; }
        public byte[] FileContent { get; set; }

        public FileLibraryModel()
        {
            DateTime n = DateTime.Now;
            UploadDate = n.AddMilliseconds(-n.Millisecond);
        }

    }

}