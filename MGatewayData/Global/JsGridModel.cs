﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Global.Model
{
    public class JsGridColumnType
    {
        public const string JsGridTextBox = "text";
        public const string JsGridComboBox = "select";
        public const string JsGridCheckBox = "checkbox";
        public const string JsGridTextArea = "textarea";
        public const string JsGridNumericTextBox = "number";
    }
   
    public class JsGridModel
    {

        public bool AllowAdd { get; set; }
        public bool AllowEdit { get; set; }
        public bool AllowDelete { get; set; }

        public bool HasDetails { get; set; }

        public bool ServerSideLoading { get; set; }
        public string KeyFieldName { get; set; }
        public object data { get; set; }

        public List<JsGridFieldsModel> fields { get; set; }

        public ActionURL InsertURL { get; set; }
        public ActionURL UpdateURL { get; set; }

        public ActionURL DeleteURL { get; set; }

        public ActionURL ViewURL { get; set; }

        public ActionURL LoadDataURL { get; set; }

        public CallbackElements ViewCallBack { get; set; }

        public JsGridModel()
        {
            InsertURL = new ActionURL();
            UpdateURL = new ActionURL();
            DeleteURL = new ActionURL();
            ViewURL = new ActionURL();
            LoadDataURL = new ActionURL();
            fields = new List<JsGridFieldsModel>();
            ViewCallBack = new CallbackElements();
        }

    }


    public class JsGridFieldsModel
    {
        public string name { get; set; }

        public string title { get; set; }
        public string type { get; set; }
        public int width { get; set; }

        public string validate { get; set; }
        public bool editing { get; set; }

        public string css { get; set; }
        public List<JsGridSelectItemModel> items { get; set; }

        public string valueField { get; set; }

        public string textField { get; set; }

        public bool visible { get; set; }
        //for type control only
        public bool editButton { get; set; }

    }

    public class JsGridSelectItemModel
    {
        public string Name;
        public string Id;
    }

    public class ActionURL
    {
        public ControllerURL urlAction { get; set; }
        public string URL { get; set; }

        public ActionURL()
        {
            urlAction = new ControllerURL();
        }
    }
    public class ControllerURL
    {
        public string action { get; set; }
        public string controller { get; set; }
        public string area { get; set; }
    }

    public class CallbackElements
    {
        public string Parentelem { get; set; }
        public string childelem { get; set; }

        public string childGridID { get; set; }
    }


    public class GBL_GridLayoutModel
    {
     
        public string FieldName { get; set; }

        public string FieldTitle { get; set; }

        public bool Visible { get; set; }

        public bool Editable { get; set; }
        public int seq { get; set; }
       
        public string FieldType { get; set; }

        public string css { get; set; }
        public int FieldWidth { get; set; }

        public bool FieldRequired { get; set; }

        public List<JsGridSelectItemModel> items { get; set; }


    }


}