﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
namespace MGatewayBO.Model
{
    public class AamarPayRespondModel
    {

        [Key]
        [StringLength(100)]
        public string mer_txnid { get; set; } //from amaarModel.tran_id  
        [StringLength(100)]
        public string pg_service_charge_bdt { get; set; }
        [StringLength(100)]
        public string pg_service_charge_usd { get; set; }
        [StringLength(100)]
        public string pg_card_bank_name { get; set; }
        [StringLength(100)]
        public string pg_card_bank_country { get; set; }
        [StringLength(100)]
        public string card_number { get; set; }
        [StringLength(100)]
        public string card_holder { get; set; }
        [StringLength(100)]
        public string cus_phone { get; set; }
        [StringLength(100)]
        public string desc { get; set; }
        [StringLength(100)]
        public string success_url { get; set; }
        [StringLength(100)]
        public string fail_url { get; set; }
        [StringLength(100)]
        public string cus_name { get; set; }
        [StringLength(100)]
        public string cus_email { get; set; }
        [StringLength(100)]
        public string currency_merchant { get; set; }
        [StringLength(100)]
        public string convertion_rate { get; set; }
        [StringLength(100)]
        public string ip_address { get; set; }
        [StringLength(100)]
        public string other_currency { get; set; }
        [StringLength(100)]
        public string pay_status { get; set; }
        [StringLength(100)]
        public string pg_txnid { get; set; }
        [StringLength(100)]
        public string epw_txnid { get; set; }
       

        [StringLength(100)]
        public string store_id { get; set; }
        [StringLength(100)]
        public string merchant_id { get; set; }
        [StringLength(100)]
        public string currency { get; set; }
        [StringLength(100)]
        public string store_amount { get; set; }
        [StringLength(100)]
        public string pay_time { get; set; }
        [StringLength(100)]
        public string amount { get; set; }
        [StringLength(100)]
        public string bank_txn { get; set; }
        [StringLength(100)]
        public string card_type { get; set; }
        [StringLength(100)]
        public string reason { get; set; }
        [StringLength(100)]
        public string pg_card_risklevel { get; set; }
        [StringLength(100)]
        public string pg_error_code_details { get; set; }
        [StringLength(100)]
        public string opt_a { get; set; }
        [StringLength(100)]
        public string opt_b { get; set; }
        [StringLength(100)]
        public string opt_c { get; set; }
        [StringLength(100)]
        public string opt_d { get; set; }

        public DateTime RecordTime { get; set; }

        public int ResultStatus { get; set; }





    }

    public enum ResultStatus
    {
        [Description("Success")]
        Success = 1,
        [Description("Failed")]
        Failed = 2,
        [Description("Cancelled")]
        Cancelled = 3,
    }
}
