﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Dynamic;
namespace MGatewayBO.Model
{
    public class GeneralAPIModel
    {
        public string ShopName { get; set; }

        public string MerchantID { get; set; }
        public string Version { get; set; }
        public string Telno { get; set; }
    }
    public class LoginAPIModel : GeneralAPIModel
    {
        public string Password { get; set; }
        public string Token { get; set; }
    }

    public class WithdrawalRequestAPIModel : GeneralAPIModel
    {
        public string WithdrawalID { get; set; }
    }

    public class ManualDepositAPIModel : GeneralAPIModel
    {
        public string DepositID { get; set; }

        public string Status { get; set; }

        public string RejectRemark { get; set; }
    }

    public class ReceiveMessageAPIModel : GeneralAPIModel
    {
        public string RawMessage { get; set; }


    }

    public class MessageContainer
    {
        public string content { get; set; }

        public string Sender { get; set; }

        public string MsgDateTime { get; set; }

        public string ServiceCenter { get; set; }
    }

    public class HistoryAPIModel : GeneralAPIModel
    {
        public string TransactionType { get; set; }

        public string DateRange { get; set; }
    }

    public class PingAPIModel : GeneralAPIModel
    {
        public string appStatus { get; set; }

       
    }


    public class ApiResultModel
    {

        public string status { get; set; }

        public string message { get; set; }

        public string errorMessage { get; set; }

        public dynamic data { get; set; }

        public ApiResultModel()
        {
            data =  new ExpandoObject();
        }
    }

    public class ApiWithdrawalDataModel
    {
        public string WithdrawalID { get; set; }
        public string WithdrawalAmount { get; set; }
        public string WalletType { get; set; }
        public string WalletNumber { get; set; }
    }




}





