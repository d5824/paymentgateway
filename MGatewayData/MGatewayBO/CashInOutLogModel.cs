﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
namespace MGatewayBO.Model
{
    public class CashInOutLogModel
    {
        [Key]
        public string ID { get; set; }
        public TransactionType TransactionType { get; set; }
      
        [StringLength(100)]
        public string TrxID { get; set; }

        [StringLength(50)]
        public string MerchantID { get; set; }

        [StringLength(50)]
        public string ShopID { get; set; }

        public WalletType WalletType { get; set; }
        [StringLength(200)]
        public string WalletNumber { get; set; }

        [StringLength(200)]
        public string WalletAccountClass { get; set; }
        [StringLength(100)]
        public string MemberID { get; set; }
        [StringLength(200)]
        public string MemberPhoneNo { get; set; }

        public double Amount { get; set; }
        [StringLength(50)]
        public string Operator { get; set; }

        public CashInOutAction Status { get; set; }
        [StringLength(500)]
        public string Remark { get; set; }

        public DateTime CreatedTime { get; set; }

        public DateTime ApprovedTime { get; set; }

        public CashInOutAction Action { get; set; }

        public string LastModified { get; set; }

        public DateTime LastModifiedDateTime { get; set; }

    }


    public class CashInOutLogSQLModel
    {

        public string ID { get; set; }
        public TransactionType TransactionType { get; set; }

        public string TrxID { get; set; }

        public string MerchantID { get; set; }

        public string ShopID { get; set; }

        public WalletType WalletType { get; set; }

        public string WalletNumber { get; set; }

        public string WalletAccountClass { get; set; }

        public string MemberID { get; set; }

        public string MemberPhoneNo { get; set; }

        public double Amount { get; set; }

        public string Operator { get; set; }

        public CashInOutAction Status { get; set; }

        public string Remark { get; set; }

        public DateTime CreatedTime { get; set; }

        public DateTime ApprovedTime { get; set; }

        public CashInOutAction Action { get; set; }

        public string ShopName { get; set; }

        public string LastModified { get; set; }

        public DateTime LastModifiedDateTime { get; set; }




    }


    public class CashInOutLogViewModel
    {
        [Key]
        public string ID { get; set; }
        public string TransactionType { get; set; }
        public string TrxID { get; set; }
        public string ShopID { get; set; }
        public string WalletType { get; set; }

        public string WalletNumber { get; set; }

        public string WalletAccountClass { get; set; }

        public string MemberID { get; set; }

        public string MemberPhoneNo { get; set; }

        public string Amount { get; set; }

        public string Operator { get; set; }

        public string Status { get; set; }

        public string Remark { get; set; }

        public string CreatedTime { get; set; }

        public string ApprovedTime { get; set; }

        public string Action { get; set; }

        public string ShopName { get; set; }

        public string MerchantID { get; set; }

        public string LastModified { get; set; }

        public string LastModifiedDateTime { get; set; }


    }

    public class CashTransactionGridLogFilterResult
    {
        public List<CashInOutLogViewModel> data { get; set; }

        public int itemsCount { get; set; }

        public string FilterAmount { get; set; }
    }






}
