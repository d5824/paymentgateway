﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
namespace MGatewayBO.Model
{
    public class CashInOutModel
    {
        [Key,Column(Order = 0)]
        public string ID { get; set; }
        public TransactionType TransactionType { get; set; }
      
        [StringLength(100)]
        public string TrxID { get; set; }

        [Key, Column(Order = 1)]
        [StringLength(50)]
        public string MerchantID { get; set; }

        [StringLength(50)]
        public string ShopID { get; set; }

        public WalletType WalletType { get; set; }
        [StringLength(200)]
        public string WalletNumber { get; set; }

        [StringLength(200)]
        public string WalletAccountClass { get; set; }
        [StringLength(100)]
        public string MemberID { get; set; }
        [StringLength(200)]
        public string MemberPhoneNo { get; set; }

        public double Amount { get; set; }
        [StringLength(50)]
        public string Operator { get; set; }

        public CashInOutAction Status { get; set; }
        [StringLength(500)]
        public string Remark { get; set; }

        public DateTime CreatedTime { get; set; }

        public DateTime ApprovedTime { get; set; }

        public CashInOutAction Action { get; set; }

        public int FileNo { get; set; }

    }


    public class CashInOutSQLModel : GeneralAPIModel
    {

        public string ID { get; set; }
        public TransactionType TransactionType { get; set; }

        public string TrxID { get; set; }

        public string ShopID { get; set; }

        public WalletType WalletType { get; set; }

        public string WalletNumber { get; set; }

        public string WalletAccountClass { get; set; }

        public string MemberID { get; set; }

        public string MemberPhoneNo { get; set; }

        public double Amount { get; set; }

        public string Operator { get; set; }

        public CashInOutAction Status { get; set; }

        public string Remark { get; set; }

        public DateTime CreatedTime { get; set; }

        public DateTime ApprovedTime { get; set; }

        public CashInOutAction Action { get; set; }

        public int FileNo {get;set;}

        public string FileName { get; set; }


    }


    public class CashInOutViewModel
    {
        [Key]
        public string ID { get; set; }
        public string TransactionType { get; set; }
        public string TrxID { get; set; }
        public string ShopID { get; set; }
        public string WalletType { get; set; }
      
        public string WalletNumber { get; set; }

        public string WalletAccountClass { get; set; }
        
        public string MemberID { get; set; }
       
        public string MemberPhoneNo { get; set; }

        public string Amount { get; set; }
       
        public string Operator { get; set; }

        public string Status { get; set; }
        
        public string Remark { get; set; }

        public string CreatedTime { get; set; }

        public string ApprovedTime { get; set; }

        public string Action { get; set; }

        public string ShopName { get; set; }

        public string MerchantID { get; set; }

        public int FileNo { get; set; }

        public string FileName { get; set; }


    }


    public class CashTransactionGridFilterResult
    {
        public List<CashInOutViewModel> data { get; set; }

        public int itemsCount { get; set; }

        public string FilterAmount { get; set; }
    }



    public enum CashInOutAction
    {
        [Description("New")]
        New = 0,
        [Description("Pending")]
        Pending = 1,
        [Description("Approve")]
        Approved = 2,
        [Description("Reject")]
        Rejected = 3,
        

    }


    public enum TransactionType
    {
        [Description("CashIn")]
        CashIn = 1,
        [Description("CashOut")]
        CashOut = 2,


    }


    public class AddManualRecordContainer
    {
        public string resultmsg { get; set; }

        public string transactionID { get; set; }
    }


    public class CashInOutApiModel
    {
        [Key, Column(Order = 0)]
        public string ID { get; set; }
        public TransactionType TransactionType { get; set; }

        [StringLength(100)]
        public string TrxID { get; set; }

        [Key, Column(Order = 1)]
        [StringLength(50)]
        public string MerchantID { get; set; }

        [StringLength(50)]
        public string ShopID { get; set; }

        public WalletType WalletType { get; set; }
        [StringLength(200)]
        public string WalletNumber { get; set; }

        [StringLength(200)]
        public string WalletAccountClass { get; set; }
        [StringLength(100)]
        public string MemberID { get; set; }
        [StringLength(200)]
        public string MemberPhoneNo { get; set; }

        public double Amount { get; set; }
      
        public string MerchantTrxID { get; set; }

        public string NotifyURL { get; set; }

        public string RedirectURL { get; set; }

        public CashInOutAction Status { get; set; }


    }

    public enum PaymentApiStatus
    {
        [Description("Your Deposit has been processed and credited to your account.")]
        AutoMatched = 1,
        [Description("Your Deposit has been successfully submit. Please check your wallet after 5-10 minutes.")]
        Waiting = 2,
        [Description("Invalid Transaction")]
        Invalid = 3,
    }


}


