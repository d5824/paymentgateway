﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
namespace MGatewayBO.Model
{
    public class DepositSandBoxModel
    {

       
            public string PlayerUserName { get; set; }
            public string Amount { get; set; }

            public WalletType PaymentMethod { get; set; }

            public string TransactionType { get; set; }

            public string SenderNumber { get; set; }

            public string ReceiverNumber { get; set; }

            public string MerchantTransactionID { get; set; }


       

    }

}
