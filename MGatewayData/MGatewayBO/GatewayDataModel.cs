﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace MGatewayBO.Model
{
    public class GatewayDataModel
    {
        [Key]
        [StringLength(50)]
        public string GatewayID { get; set; }
        [StringLength(50)]
        public string GatewayKey { get; set; }
        [StringLength(50)]
        public string GatewayName { get; set; }
        [StringLength(100)]
        public string GatewayEmail { get; set; }


    }





}
