﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MGatewayBO.Model
{
    public class GatewayMerchantModel
    {
        [Key]
        [StringLength(50)]
        public string ServiceID { get; set; }

       
        [StringLength(50)]
        public string GatewayID { get; set; }

        
        [StringLength(50)]
        public string merchantID { get; set; }

        public int AssignStatus { get; set; }

    }

    public class GatewayMerchantViewModel 
    {
        public string ServiceID { get; set; }
        public string GatewayID { get; set; }

        public string GatewayName { get; set; }
        public string merchantID { get; set; }
        public int AssignStatus { get; set; }
        public string GatewayKey { get; set; }
        public string GatewayEmail { get; set; }
        public int merchantStatus { get; set; }
        public string success_url { get; set; }
        public string fail_url { get; set; }
        public string cancel_url { get; set; }

    }
}
