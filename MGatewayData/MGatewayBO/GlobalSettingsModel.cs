﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace MGatewayBO.Model
{
    public class GlobalSetingsModel
    {
        [Key, Column(Order = 1)]
        [StringLength(50)]
        public string SysType { get; set; }

        [Key, Column(Order = 2)]
        [StringLength(100)]
        public string Syskey { get; set; }

        [StringLength(4000)]
        public string SysValue { get; set; }
    }


    public class GlobalSetingsViewModel
    {
     
        public bool DepositAutoApprove { get; set; }

        public int DepositMatchFreq { get; set; }

    
        public bool WithdrawalAutoApprove { get; set; }
        public int WithdrawalMatchFreq { get; set; }

        public int WalletMinList { get; set; }
        public int WalletMinListApproved { get; set; }
        public bool ValidateSMSSender { get; set; }


        public string AppVersion { get; set; }

        public int PingInterval { get; set; }

        public string Recipient { get; set; }

        public int IdleInterval { get; set; }

        public bool SendNotification {get; set; }


    }


}



