﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
namespace MGatewayBO.Model
{
    public class LogDataModel
    {
        [Key]
        public int LogID { get; set; }
        [StringLength(50)]
        public string LogCategory { get; set; }
        [StringLength(50)]
        public string LogType { get; set; }
        [StringLength(5000)]
        public string LogContent { get; set; }
        [StringLength(50)]
        public string LogBy { get; set; }

        public DateTime LogDateTime { get; set; }




    }

    public enum ErrorCode
    {
        [Description("Hash Error")]
        e100 = 100,
        [Description("Invalid Service")]
        e101 = 101,
        [Description("Invalid TransactionID")]
        e102 = 102,
        [Description("Invalid UserID")]
        e103 = 103,
        [Description("Invalid Amount")]
        e104 = 104,
        [Description("Invalid Currency")]
        e105 = 105,
        [Description("Service Not Available")]
        e200 = 200,
        [Description("Merchant Not Available")]
        e201 = 201,

    }

}
