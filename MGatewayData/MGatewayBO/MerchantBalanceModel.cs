﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MGatewayBO.Model
{
  

    public class MerchantBalanceModel
    {
        public string TotalCashIn { get; set; }
        public string TotalCashOut { get; set; }

        public string AvailableBalance { get; set; }
       
    }

    public class MerchantBalanceComputeModel
    {
        public double TotalCashIn { get; set; }
        public double TotalCashOut { get; set; }
        public double YesterdayCashOut { get; set; }
        public double YesterdayCashIn { get; set; }
        public double TodayCashOut { get; set; }
        public double TodayCashIn { get; set; }
        public double AvailableBalance { get; set; }
     

    }
}
