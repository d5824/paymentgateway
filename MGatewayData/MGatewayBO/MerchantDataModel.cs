﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
namespace MGatewayBO.Model
{
    public class MerchantDataModel
    {
        [Key]
        [StringLength(50)]
        public string merchantID { get; set; }
        [StringLength(50)]
        public string merchantName { get; set; }

        public MerchantStatus merchantStatus { get; set; }

        public string APIKEY { get; set; }


    }

    public class MerchantDataViewModel
    {
       
        public string merchantID { get; set; }
       
        public string merchantName { get; set; }

        public string merchantStatus { get; set; }

        public string APIKEY { get; set; }



    }


    public enum MerchantStatus
    {
        [Description("Inactive")]
        Inactive = 0,
        [Description("Active")]
        Active = 1
      
    }



  
}
