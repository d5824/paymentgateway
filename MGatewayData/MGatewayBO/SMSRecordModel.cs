﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
namespace MGatewayBO.Model
{
    public class SMSRecordModel
    {
        [Key]
        [StringLength(50)]
        public string ID { get; set; }

        [StringLength(50)]
        public string MerchantID { get; set; }

        [StringLength(100)]
        public string TrxID { get; set; }
        [StringLength(50)]
        public string ShopID { get; set; }

        public WalletType WalletType { get; set; }

        public TransactionType SMSType { get; set; }

        public double Amount { get; set; }
        [StringLength(100)]
        public string AccountNumber { get; set; }

        public DateTime SMSDeliveryTime { get; set; }

        public DateTime CreatedTime { get; set; }
        [StringLength(500)]
        public string RawMessage { get; set; }

        public string Sender { get; set; }
        public double Balance { get; set; }

        public DateTime? SmsDateTime { get; set; }

    }

    public class SMSRecordDetailsModel
    {
        [Key]
        [StringLength(50)]
        public string MessageID { get; set; }

        [StringLength(2000)]
        public string RawMessage { get; set; }

    }

    public class SMSRecordViewModel
    {

        public string ID { get; set; }

        public string MerchantID { get; set; }

        public string Matched { get; set; }
        public string TrxID { get; set; }

        public string ShopID { get; set; }
        public string ShopName { get; set; }

        public string WalletType { get; set; }

        public string SMSType { get; set; }

        public string Amount { get; set; }

        public string AccountNumber { get; set; }

        public string SMSDeliveryTime { get; set; }

        public string CreatedTime { get; set; }

        public string RawMessage { get; set; }

        public string Sender { get; set; }

        public string Balance { get; set; }



    }

    public class SMSRecordSQLModel
    {

        public string ID { get; set; }

        public string Matched { get; set; }
        public string MerchantID { get; set; }


        public string TrxID { get; set; }

        public string ShopID { get; set; }

        public string ShopName { get; set; }

        public WalletType WalletType { get; set; }

        public TransactionType SMSType { get; set; }

        public double Amount { get; set; }

        public string AccountNumber { get; set; }

        public DateTime SMSDeliveryTime { get; set; }

        public DateTime CreatedTime { get; set; }

        public string RawMessage { get; set; }


        public string Sender { get; set; }

        public double Balance { get; set; }


    }

    public class SMSRecordGridFilterResult
    {
        public List<SMSRecordViewModel> data { get; set; }

        public int itemsCount { get; set; }

        public string FilterAmount { get; set; }
    }


    public class UnKnowSMSModel
    {
        [Key]
        public int ID { get; set; }
        [StringLength(50)]
        public string MerchantID { get; set; }

        [StringLength(50)]
        public string ShopID { get; set; }

        public DateTime SMSDeliveryTime { get; set; }

        public DateTime CreatedTime { get; set; }
        [StringLength(500)]
        public string RawMessage { get; set; }

        public string Sender { get; set; }

        public string MessageID { get; set; } = string.Empty;

    }

    public class SMSRecordRematchVM
    {

        public string ID { get; set; }

        public string MerchantID { get; set; }

        public string TrxID { get; set; }

        public string ShopID { get; set; }

        public WalletType WalletType { get; set; }

        public TransactionType SMSType { get; set; }

        public double Amount { get; set; }

        public string AccountNumber { get; set; }

        public DateTime SMSDeliveryTime { get; set; }

        public DateTime CreatedTime { get; set; }

        public string RawMessage { get; set; }

        public string Sender { get; set; }
        public double Balance { get; set; }

        public DateTime? SmsDateTime { get; set; }

        public string ShopName { get; set; }
    }


    public class SMSMatchResult {
        public string ID { get; set; }

        public DateTime? SmsDateTime { get; set; }

        public bool isMatch { get; set; }

        public string TrxID { get; set; }
    }

}
