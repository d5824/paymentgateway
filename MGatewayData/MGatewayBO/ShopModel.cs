﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
namespace MGatewayBO.Model
{
    public class ShopModel
    {
        [Key]
        [StringLength(50)]
        public string ShopID { get; set; }

        [StringLength(50)]
        public string MerchantID { get; set; }

        [StringLength(50)]
        public string ShopName { get; set; }
        [StringLength(200)]
        public string Password { get; set; }
        [StringLength(50)]
        public string Contact { get; set; }
        [StringLength(200)]
        public string WhatappsLink { get; set; }

        public ShopStatus ShopStatus { get; set; }

        public DateTime LastLoginDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastActiveTime { get; set; }

        public ActiveStatus ActiveStatus { get; set; }

        [StringLength(200)]
        public string Token { get; set; }

        public virtual HashSet<ShopWalletsModel> wallets { get; set; }
 

    }

    public class ShopViewModel
    {
        public string ShopID { get; set; }
       
        public string MerchantID { get; set; }
        public string ShopName { get; set; }
        public string Password { get; set; }
        public string Contact { get; set; }
   
        public string WhatappsLink { get; set; }

        public int ShopStatus { get; set; }

        public string LastLoginDate { get; set; }

        public string CreatedDate { get; set; }

        public string LastActiveTime { get; set; }

        public ActiveStatus ActiveStatus { get; set; }
        public int WalletsCount { get; set; }

    }


    public enum ShopStatus
    {
        [Description("Inactive")]
        Inactive = 0,
        [Description("Active")]
        Active = 1,
        [Description("Locked")]
        Locked = 2

    }

    public enum ActiveStatus
    {
        [Description("NeverActive")] //Last Seen
        NeverActive = 0,
        [Description("Online")] //Online status
        Online = 1,
        [Description("LastSeen")] //Start means start the apps.
        LastSeen = 2

    }




}
