﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
namespace MGatewayBO.Model
{
    public class ShopWalletsModel
    {
        [Key, Column(Order = 0)]
        [StringLength(50)]
        public string ShopID { get; set; }

        [Key, Column(Order =1)]
        public WalletType WalletType { get; set; }
        [StringLength(200)]
        public string WalletNumber { get; set; }

        public double CashOutLimit { get; set; }

        public double CashInLimit { get; set; }

        public string AccountClass { get; set; }

        public ShopPriority Priority { get; set; }

        public bool Enabled { get; set; }

        public DateTime CreatedTime { get; set; }

        public virtual ShopModel master { get; set; }

    }

    public class ShopWalletsSQLModel
    {
       
        public string ShopID { get; set; }

        public string ShopName { get; set; }
        public WalletType WalletType { get; set; }
       
        public string WalletNumber { get; set; }

        public double CashOutLimit { get; set; }

        public double CashInLimit { get; set; }

        public string AccountClass { get; set; }

        public ShopPriority Priority { get; set; }

        public bool Enabled { get; set; }

        public DateTime CreatedTime { get; set; }

        public string Sender { get; set; }
        public virtual ShopModel master { get; set; }

    }


    public class ShopWalletsViewModel
    {
     
        public string ShopID { get; set; }

        public string ShopName { get; set; }
        public string WalletType { get; set; }

        public string WalletTypeDesc { get; set; }
    
        public WalletType WalletTypeOri { get; set; }

        public string WalletNumber { get; set; }

        public string CashOutLimit { get; set; }

        public string CashOutTransaction { get; set; }
        public string CashInLimit { get; set; }

        public string CashInTransaction { get; set; }
        public string AccountClass { get; set; }

        public string Priority { get; set; }

        public ShopPriority PriorityOri { get; set; }

        public bool Enabled { get; set; }

        public string Sender { get; set; }


    }

    public class ShopWalletTransactionModel
    {
        public string ShopID { get; set; }

        public string MerchantID { get; set; }

        public TransactionType TransactionType { get; set; }

        public WalletType WalletType { get; set; }

        public DateTime ApprovedTime { get; set; }

        public double Amount { get; set; }
    }

    public enum WalletType
    {
        [Description("BKash")]
        BKash = 0,
        [Description("Rocket")]
        Rocket = 1,
        [Description("Nagad")]
        Nagad = 2,
        [Description("Tap")]
        Tap = 3,
        [Description("Upay")]
        Upay = 4,
        [Description("Sure Cash")]
        SureCash = 5


    }

    public enum ShopPriority
    {
        [Description("Very High")]
        VeryHigh = 1,
        [Description("High")]
        High = 2,
        [Description("Normal")]
        Normal = 3,
        [Description("Low")]
        Low = 4,
        [Description("Very Low")]
        VeryLow = 3

    }




}
