﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
namespace MGatewayBO.Model
{
    public class TrxInfoModel
    {
        //[Key, Column(Order = 0)]
        [Key]
        [StringLength(100)]
        public string TrxID { get; set; }
        [StringLength(50)]
        public string ServiceID { get; set; }
        // [Key, Column(Order = 1)]
        [StringLength(50)]
        public string MerchantID { get; set; }

      //  [Key, Column(Order = 2)]
        [StringLength(50)]
        public string GatewayID { get; set; }
        [StringLength(100)]
        public string TrxID_M { get; set; }
        [StringLength(100)]
        public string TrxID_G { get; set; }

        public TrxStatus TrxStatus { get; set; }

        [StringLength(10)]
        public string Currency { get; set; }

        public double Amount { get; set; }

        public DateTime CreatedTime { get; set; }

        public DateTime GatewayGeneratedTime { get; set; }

        public DateTime GatewayRespondTime { get; set; }

        public DateTime ManualUpdateTime { get; set; }
        [StringLength(50)]
        public string UpdateBy { get; set; }



    }

    public class TrxInfoViewModel
    {
      
        public string TrxID { get; set; }
    
        public string MerchantID { get; set; }

        public string GatewayID { get; set; }

        public string TrxID_M { get; set; }

        public string TrxID_G { get; set; }

        public string TrxStatus { get; set; }

        public string CreatedTime { get; set; }

        public string GatewayGeneratedTime { get; set; }

        public string GatewayRespondTime { get; set; }

        public string ManualUpdateTime { get; set; }
    
        public string UpdateBy { get; set; }

        public string Currency { get; set; }
        public string Amount { get; set; }

        public string ServiceID { get; set; }



    }

    public class TrxInfoGridFilterResult
    {
        public List<TrxInfoViewModel> data { get; set; }

        public int itemsCount { get; set; }

        public string FilterAmount { get; set; }
    }

    public class TrxInfoGridReloadResult
    {
        public Global.Model.JsGridModel jsGrid { get; set; }

        public string TotalAmount { get; set; }
    }

    public enum TrxStatus
    {
        [Description("New")]
        New = 0, //When record is generated from payment gateway.
        [Description("Pending")]
        Pending = 1, //When record is generated from payment gateway.
        [Description("Verified")]
        Success = 2, //payment gateway respond
        [Description("Verification Failed")]
        Failed = 3, //payment gateway respond
        [Description("Verification Cancelled")]
        Cancelled = 4, //payment gateway respond
        [Description("Manual")]
        Manual = 5, //Manual success
        [Description("Expired")]
        Expired = 6, //?? scheduler expired ? 
        [Description("Completed")]
        UpdateSuccess = 7, //Post to merchant success
        [Description("Completed With Error")]
        UpdateFailed = 8 //Post to merchant but no response
    }


    public class RequestPayment
    {
        [Key]
        public string TrxID { get; set; }
        [StringLength(50)]
        public string serviceID { get; set; }
        [StringLength(50)]
        public string TrxID_M { get; set; }
        [StringLength(50)]
        public string userID { get; set; }
        [StringLength(10)]
        public string Currency { get; set; }
        public double Amount { get; set; }
        [StringLength(100)]
        public string sign { get; set; }
        [StringLength(100)]
        public string NotifyUrl { get; set; }
    }

    public class RequestPaymentVM
    {
        public string serviceID { get; set; }
        public string transactionID { get; set; }
        public string userID { get; set; }
        public string Currency { get; set; }
        public string Amount { get; set; }
        public string sign { get; set; }
        public string NotifyUrl { get; set; }
    }
}
