﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace MGatewayBO.Model
{
    public class UserDataModel
    {
        [Key]
        [StringLength(100)]
        public string Phone { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(100)]
        public string Add1 { get; set; }
        [StringLength(100)]
        public string Add2 { get; set; }
        [StringLength(100)]
        public string City { get; set; }
        [StringLength(100)]
        public string Country { get; set; }
        [StringLength(100)]
        public string Email { get; set; }
        [StringLength(100)]
        public string Uid { get; set; } 

        public UserDataModel()
        {
            Email = string.Empty;
            Uid = string.Empty;
        }
    }
}
