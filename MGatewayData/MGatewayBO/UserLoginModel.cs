﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
namespace MGatewayBO.Model
{
    public class UserLoginModel
    {
        [Key]
        [StringLength(50)]
        public string UserID { get; set; }

       
        [StringLength(50)]
        public string MerchantID { get; set; }

        [StringLength(100)]
        public string UserName { get; set; }
        [StringLength(100)]
        public string Email { get; set; }
        [StringLength(200)]
        public string UserPassword { get; set; }

        public DateTime LastLoginDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public int FailedLoginAttempt { get; set; }

        public bool IsSuspended { get; set; }

        public AccStatus Status { get; set; }
        [StringLength(50)]
        public string UserRole { get; set; }


    }

    public class UserLoginViewModel
    {
       
        public string UserID { get; set; }

        public string MerchantID { get; set; }
        public string UserName { get; set; }
  
        public string Email { get; set; }
    
        public string UserPassword { get; set; }

        public int FailedLoginAttempt { get; set; }

        public string LastLoginDate { get; set; }

        public string CreatedDate { get; set; }
        public bool IsSuspended { get; set; }

        public int Status { get; set; }
        [StringLength(50)]

        public string strStatus { get; set; }
        public string UserRole { get; set; }


    }


    public enum AccStatus
    {

        [Description("Access Denied. Please try again.")]
        AccessDenied = 0,

        [Description("Active")]
        Active = 1,

        [Description("Required to change Password.")]
        ChangePassword = 2,

        [Description("Account Suspended.")]
        AccSuspended = 3,

    }


    public class LoginResultModel
    {
        public AccStatus AccountStatus { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }

        public string MerchantID { get; set; }

        public string UserRole { get; set; }
    }





}
