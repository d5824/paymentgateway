﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MGatewayHelpers
{
    public class ControllerHelper
    {
       
        public static string GenerateURL(string Action, string Controller, object RouteValues, RequestContext Request )
        {
            UrlHelper urlHelper = new UrlHelper(Request);
            string URL = urlHelper.Action(Action,Controller, RouteValues);
            return URL;
        }


    }
}