﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Dynamic;
using MGatewayHelpers;
using MGatewayProcessor.Helpers;
namespace ReportPortal.Helper
{
    public class FormHelper
    {
        protected HtmlHelper MyHtmlHelper;
        protected ViewContext MyViewContext;


        public FormHelper(HtmlHelper HtmlHelper, ViewContext ViewContext)
        {
            MyHtmlHelper = HtmlHelper;
            MyViewContext = ViewContext;
        }

        public MvcHtmlString AddTextBox(Object DataSource, string FieldName, string LabelText, bool Enabled, bool required)
        {
            TextBoxInputSettings settings = new TextBoxInputSettings();
            settings.Enabled = Enabled;
            settings.FieldName = FieldName;
            settings.LabelText = LabelText;
            settings.ElementClass = "";
            settings.HtmlAttribute["autocomplete"] = "off";
            settings.Required = required;
            settings.DataSource = DataSource;
            AddInputBox(settings);
            return MvcHtmlString.Empty;
        }

        public MvcHtmlString AddNumericBox(Object DataSource, string FieldName, string LabelText, bool Enabled, bool required)
        {
            TextBoxInputSettings settings = new TextBoxInputSettings();
            settings.Enabled = Enabled;
            settings.FieldName = FieldName;
            settings.LabelText = LabelText;
            settings.ElementClass = "txt_num";
            settings.HtmlAttribute["autocomplete"] = "off";
            settings.Required = required;
            AddInputBox(settings);
            return MvcHtmlString.Empty;
        }


        public MvcHtmlString AddPaswordTextBox(Object DataSource, string FieldName, string LabelText, bool Enabled, bool required)
        {
            TextBoxInputSettings settings = new TextBoxInputSettings();
            settings.Enabled = Enabled;
            settings.FieldName = FieldName;
            settings.LabelText = LabelText;
            settings.ElementClass = "";
            settings.HtmlAttribute["autocomplete"] = "off";
            settings.HtmlAttribute["type"] = "password";
            settings.Required = required;
            AddInputBox(settings);
            return MvcHtmlString.Empty;
        }



        public MvcHtmlString AddToggleBox(Object DataSource, string FieldName, string LabelText, bool Enabled)
        {
            TextBoxInputSettings settings = new TextBoxInputSettings();
            settings.DataSource = DataSource;
            settings.Enabled = Enabled;
            settings.FieldName = FieldName;
            settings.LabelText = LabelText;
            settings.ElementClass = "";
            settings.HtmlAttribute["autocomplete"] = "off";
            AddToggleBox(settings);
            return MvcHtmlString.Empty;
        }


        public MvcHtmlString AddDateTextBox(Object DataSource, string FieldName, string LabelText, bool Enabled, bool required)
        {
            TextBoxInputSettings settings = new TextBoxInputSettings();
            settings.Enabled = Enabled;
            settings.FieldName = FieldName;
            settings.LabelText = LabelText;
            settings.TextboxControls = "txt_date";
            settings.ElementClass = "datetimepicker-input";
            settings.Required = required;
            settings.HtmlAttribute["autocomplete"] = "off";
            settings.HtmlAttribute["data-toggle"] = "datetimepicker";
            settings.HtmlAttribute["data-target"] = "#" + FieldName ;
      
            AddInputBox(settings);
            return MvcHtmlString.Empty;
        }

        public MvcHtmlString AddDateTimeTextBox(Object DataSource, string FieldName, string LabelText, bool Enabled, bool required)
        {
            TextBoxInputSettings settings = new TextBoxInputSettings();
            settings.Enabled = Enabled;
            settings.FieldName = FieldName;
            settings.LabelText = LabelText;
            settings.TextboxControls = "txt_datetime";
            settings.ElementClass = "datetimepicker-input";
            settings.Required = required;
            settings.HtmlAttribute["autocomplete"] = "off";
            settings.HtmlAttribute["data-toggle"] = "datetimepicker";
            settings.HtmlAttribute["data-target"] = "#" + FieldName;

            AddInputBox(settings);
            return MvcHtmlString.Empty;
        }



        public MvcHtmlString AddDateRangeTextBox(Object DataSource, string FieldName, string LabelText, bool Enabled,bool Required)
        {
            TextBoxInputSettings settings = new TextBoxInputSettings();
            settings.Enabled = Enabled;
            settings.FieldName = FieldName;
            settings.LabelText = LabelText;
            settings.TextboxControls = "txt_daterange";
            settings.Required = Required;
            settings.HtmlAttribute["autocomplete"] = "off";
            settings.DataSource = DataSource;
            AddInputBox(settings);
            return MvcHtmlString.Empty;
        }
        public MvcHtmlString AddDateRangeTimeTextBox(Object DataSource, string FieldName, string LabelText, bool Enabled, bool Required)
        {
            TextBoxInputSettings settings = new TextBoxInputSettings();
            settings.Enabled = Enabled;
            settings.FieldName = FieldName;
            settings.LabelText = LabelText;
            settings.TextboxControls = "txt_daterangetime";
            settings.Required = Required;
            settings.HtmlAttribute["autocomplete"] = "off";

            AddInputBox(settings);
            return MvcHtmlString.Empty;
        }

        public MvcHtmlString AddMultiSelectDropDown(Object DataSource, string FieldName, string LabelText,List<SelectListItem> SelectList, bool Enabled, bool Required)
        {
            ComboBoxInputSettings settings = new ComboBoxInputSettings();
            settings.DataSource = DataSource;
            settings.FieldName = FieldName;
            settings.LabelText = LabelText;
            settings.SelectList = SelectList;
            settings.Required = Required;
            settings.Enabled = Enabled;
            AddMultiSelect(settings);
            return MvcHtmlString.Empty;
        }
        public MvcHtmlString AddComboBox(Object DataSource, string FieldName, string LabelText, List<SelectListItem> SelectList, bool Enabled, bool Required, bool Modal = false)
        {
            ComboBoxInputSettings settings = new ComboBoxInputSettings();
            settings.DataSource = DataSource;
            settings.FieldName = FieldName;
            settings.LabelText = LabelText;
            settings.SelectList = SelectList;
            settings.Required = Required;
            settings.Enabled = Enabled;
            settings.IsModal = Modal;
            AddCombo(settings);
            return MvcHtmlString.Empty;
        }


        private void AddInputBox(TextBoxInputSettings settings)
        {
            MyViewContext.Writer.WriteLine("<div class=\"div-wrapper full-wrap\">");
            MyViewContext.Writer.WriteLine("<label for=\"" + settings.FieldName + "\">" + settings.LabelText + "</label>");
            MyViewContext.Writer.WriteLine("<div class=\"input-wrapper\">");
            //MyViewContext.Writer.WriteLine("<input type=\"text\" id=\"" + settings.FieldName + "\" class=\"form-control input-sm " + settings.TextboxControls + "\"");

            if (!settings.HtmlAttribute.ContainsKey("id"))
            {
                settings.HtmlAttribute.Add("id", settings.FieldName);
            }

            string requiredCss = "";
            if (settings.Required)
            {
                requiredCss = "required";
                settings.HtmlAttribute.Add("data-validate", "");
            }
            if (!settings.HtmlAttribute.ContainsKey("class"))
            {
                settings.HtmlAttribute.Add("class", "form-control input-sm " + settings.TextboxControls + " " + settings.ElementClass + " " + requiredCss);
            }

            if (!settings.HtmlAttribute.ContainsKey("placeholder"))
            {
                settings.HtmlAttribute.Add("placeholder", settings.PlaceHolder);
            }

            if (!settings.Enabled)
            {
                settings.HtmlAttribute.Add("disabled", "disabled");
            }
      

            MyViewContext.Writer.Write(MyHtmlHelper.TextBox(settings.FieldName, settings.DataSource, settings.HtmlAttribute).ToHtmlString());

            MyViewContext.Writer.WriteLine("</div>");
            MyViewContext.Writer.WriteLine("</div>");

        }

        public MvcHtmlString AddFileInput(Object DataSource, string FileName, string FieldName, string LabelText, bool enabled = true, bool required = false)
        {
            MyViewContext.Writer.WriteLine("<div class=\"div-wrapper full-wrap\">");
            MyViewContext.Writer.WriteLine("<label for=\"" + FieldName + "\">" + LabelText + "</label>");
            MyViewContext.Writer.WriteLine("<div class=\"input-wrapper\">");
           
            if(DataReaderHelper.GetFormInt(DataSource.ToString()) != 0)
            {
                string FileURL = GlobalProperties.DomainPath + "/Handler/Picture.ashx?f=" + DataReaderHelper.GetFormInt(DataSource.ToString());
                MyViewContext.Writer.WriteLine("<a href=\"" + FileURL + "\" target=\"_blank\" id=\"" + FieldName + "Label" + "\" name=\"" + FieldName + "Label" + "\">" + FileName + " </a>");
                MyViewContext.Writer.WriteLine("<input  id=\"" + FieldName +  "\"" + "type=\"file\" class=\"custom-file-input\" name=\"" + FieldName + "\" />");
            }
            else
            {
                MyViewContext.Writer.WriteLine("<input id=\"" + FieldName + "\" name=\"" + FieldName + "\" type=\"File\" class=\"form-control input-sm\" ");
                if (!enabled)
                {
                    MyViewContext.Writer.WriteLine("disabled=\"disabled\" ");
                }
                if (required)
                {
                    MyViewContext.Writer.WriteLine("data-val=\"true\" data-val-required=\"data-val-required\"");
                }
                MyViewContext.Writer.WriteLine("/>");
            }
          

          
            MyViewContext.Writer.WriteLine("</div>");
            MyViewContext.Writer.WriteLine("</div>");
            return MvcHtmlString.Empty;

        }

        private void AddMultiSelect(ComboBoxInputSettings settings)
        {
            MyViewContext.Writer.WriteLine("<div class=\"div-wrapper full-wrap\">");
            MyViewContext.Writer.WriteLine("<label for=\"" + settings.FieldName + "\">" + settings.LabelText + "</label>");
            MyViewContext.Writer.WriteLine("<div class=\"input-wrapper\">");

            settings.HtmlAttribute.Add("class", "form-control input-sm  multiselect ");
            settings.HtmlAttribute.Add("id", settings.FieldName);
            settings.HtmlAttribute.Add("multiple", "");
            settings.HtmlAttribute.Add("style", "width: 100%");
            settings.HtmlAttribute["autocomplete"] = "off";
            MyViewContext.Writer.Write(MyHtmlHelper.DropDownList(settings.FieldName,
                                                          settings.SelectList,
                                                          settings.HtmlAttribute
                                                          ).ToHtmlString());


            MyViewContext.Writer.WriteLine("</div>");
            MyViewContext.Writer.WriteLine("</div>");
        }

        private void AddCombo(ComboBoxInputSettings settings)
        {
            MyViewContext.Writer.WriteLine("<div class=\"div-wrapper full-wrap\">");
            MyViewContext.Writer.WriteLine("<label for=\"" + settings.FieldName + "\">" + settings.LabelText + "</label>");
            MyViewContext.Writer.WriteLine("<div class=\"input-wrapper\">");

         
            settings.HtmlAttribute.Add("class", "form-select input-sm  custom-select combobox" + (settings.IsModal ? "modal" : ""));
            settings.HtmlAttribute.Add("id", settings.FieldName);
           
            settings.HtmlAttribute.Add("style", "width: 100%");
            settings.HtmlAttribute["autocomplete"] = "off";
            MyViewContext.Writer.Write(MyHtmlHelper.DropDownList(settings.FieldName,
                                                          settings.SelectList,
                                                          settings.HtmlAttribute
                                                          ).ToHtmlString());


            MyViewContext.Writer.WriteLine("</div>");
            MyViewContext.Writer.WriteLine("</div>");
        }

        private void AddToggleBox(TextBoxInputSettings settings)
        {
            bool ischeck = DataReaderHelper.GetFormBoolean(settings.DataSource);
            MyViewContext.Writer.WriteLine("<div class=\"div-wrapper full-wrap\">");
            MyViewContext.Writer.WriteLine("<label for=\"" + settings.FieldName + "\">" + settings.LabelText + "</label>");
            MyViewContext.Writer.WriteLine("<div class=\"input-wrapper\">");
            MyViewContext.Writer.WriteLine("<label class=\"switch\">");
            MyViewContext.Writer.WriteLine("<input id = \"" + settings.FieldName + "\" type=\"checkbox\" " + (ischeck ? "checked":"") + ">");
            MyViewContext.Writer.WriteLine("<span class=\"slider\"></span>");
            MyViewContext.Writer.WriteLine("</label>");
            MyViewContext.Writer.WriteLine("</div>");
            MyViewContext.Writer.WriteLine("</div>");
          
        }

    }

}

public class TextBoxInputSettings : InputSettings
{
    public string PlaceHolder { get; set; }
    public bool Enabled { get; set; }
    public string TextboxControls { get; set; }

    public TextBoxInputSettings():base()
    {
        
    }

}
  

   

  



public class InputSettings
{
    public object DataSource { get; set; }
    public string FieldName { get; set; }
    public string LabelText { get; set; }
    public bool Required { get; set; }
    public Dictionary<string, object> HtmlAttribute { get; set; }
    public bool Visible { get; set; }
    public object DefaultValue { get; set; }
    public string ElementClass { get; set; }

    public InputSettings()
    {
        HtmlAttribute = new Dictionary<string, object>();
        Visible = true;
    }
}

public class ComboBoxInputSettings :InputSettings {

    public List<SelectListItem> SelectList { get; set; }

    public Type ValueType { get; set; }
    public bool Enabled { get; set; }

    public bool IsModal { get; set; }

    public ComboBoxInputSettings()
    {
        SelectList = new List<SelectListItem>();
    }
}


