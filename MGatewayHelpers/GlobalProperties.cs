﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MGatewayHelpers
{
    public class GlobalProperties
    {
        static Version CurrentVersion = new Version(1, 2, 0, 0);
        public static string DomainPath;
        public const string gDisplayDateFormat = "dd/MM/yyyy";
        public const string gDisplayDateTimeFormat = "dd/MM/yyyy HH:mm";
        
        public static string UserID
        {
            get
            {
                return (string)HttpContext.Current.Session["UserID"];

            }
            set
            {
                HttpContext.Current.Session["UserID"] = value;
            }
        }

        public static string UserName
        {
            get
            {
                return (string)HttpContext.Current.Session["UserName"];

            }
            set
            {
                HttpContext.Current.Session["UserName"] = value;
            }
        }

        public static string MerchantID
        {
            get
            {
                return (string)HttpContext.Current.Session["MerchantID"];

            }
            set
            {
                HttpContext.Current.Session["MerchantID"] = value;
            }
        }

        public static string UserRole
        {
            get
            {
                return (string)HttpContext.Current.Session["UserRole"];

            }
            set
            {
                HttpContext.Current.Session["UserRole"] = value;
            }
        }


        public static Version Version
        {
            get
            {
                return CurrentVersion;
            }
        }


    }


}