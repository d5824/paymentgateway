﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Global.Model;
namespace MGatewayHelpers
{
    public class GridControllerHelper
    {
        public static void UpdateRouteURL(JsGridModel m, RequestContext Request)
        {
            m.InsertURL.URL = GetURL(m.InsertURL.urlAction, Request);
            m.UpdateURL.URL = GetURL(m.UpdateURL.urlAction, Request);
            m.DeleteURL.URL = GetURL(m.DeleteURL.urlAction, Request);
            m.ViewURL.URL = GetURL(m.ViewURL.urlAction, Request);
            m.LoadDataURL.URL = GetURL(m.LoadDataURL.urlAction, Request);
        }

        private static string GetURL(ControllerURL m, RequestContext Request)
        {
            string url = string.Empty;
            if (m.action != null && m.action != "")
            {
                url = ControllerHelper.GenerateURL(m.action, m.controller, new { Area = m.area }, Request);
            }
            return url;
        }


    }
}