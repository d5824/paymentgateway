﻿
using System.Web;
using System.Web.Mvc;

namespace MGatewayHelpers
{ 
public class MGatewayAuthorizationAttribute : AuthorizeAttribute
{
    protected override bool AuthorizeCore(HttpContextBase httpContext)
    {
        return GlobalProperties.UserID != null && GlobalProperties.UserID != string.Empty;
    }
}
}