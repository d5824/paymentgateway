﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MGatewayBO.Model;
using Global.Model;
using MGatewayRepository;
using APIIntegration.Model;
using System.Security.Cryptography;
using Newtonsoft.Json;
using Ubiety.Dns.Core;
using MySqlX.XDevAPI.Common;

namespace MGatewayProcessor
{
    public class Api
    {

        public static string GetGeneralApiResult(string ApiKey, APIIntegration.Model.ApiResultModel result)
        {
            MerchantDataModel Merchant = GetMerchantIDByAPIKEY(ApiKey);
            SetApiResult(result, "200", "OK");
            if (Merchant.merchantID == null)
            {
                SetApiResult(result, "500", "Invalid API Key.");
            }


            return Merchant.merchantID;



        }

        public static MerchantDataModel GetMerchantIDByAPIKEY(string ApiKey)
        {
            using (MGatewayDB db = new MGatewayDB())
            {
                string SQL = "Select * from MerchantData where APIKEY = @p0 and merchantStatus = 1";

                MerchantDataModel Merchant = db.Database.SqlQuery<MerchantDataModel>(SQL, ApiKey).FirstOrDefault();

                return (Merchant == null ? new MerchantDataModel() : Merchant) ;
            }
        }

        public static CashInOutSQLModel ConvertDeposit(string MerchantID, DepositAPIModel vm)
        {
            CashInOutSQLModel m = new CashInOutSQLModel();
            m.TransactionType = TransactionType.CashOut;
            m.WalletType = vm.PaymentGateway;
            m.Amount = Helpers.DataReaderHelper.GetFormDouble(vm.Amount);
            m.WalletNumber = AssignShopWalletNumber(MerchantID, m.WalletType);//randomize to get agent wallet number 
            m.MemberPhoneNo = vm.AccountNumber;
            m.TrxID = "";
            return m;
        }

        private static string AssignShopWalletNumber(string MerchantID, WalletType walletype)
        {
            List<ShopWalletsViewModel> Wallets = Shop.GetAllShopWallets(MerchantID).Where(x => x.WalletTypeDesc == walletype.ToString()).ToList();
            List<ShopWalletsViewModel> GeneratedWallets = new List<ShopWalletsViewModel>();
            ShopWalletsViewModel SelectedWallet = new ShopWalletsViewModel();
            if(Wallets.Count > 0)
            {
                //by wallet account class
                //priority 
                //active time
                //
                double Probability = 1 / Wallets.Count;

                Probability = Probability * 100;
                foreach (ShopWalletsViewModel item in Wallets)
                {
                    for (int i = 0; i < Probability; i++)
                    {
                        GeneratedWallets.Add(item);
                    }
                }
                Random rnd = new Random();
                int randomNumber = rnd.Next(0, GeneratedWallets.Count);
                SelectedWallet = GeneratedWallets[randomNumber];

                return SelectedWallet.WalletNumber;
            }

            return string.Empty;
          

        }

        public static CashInOutSQLModel ConvertWithdrawal(string MerchantID, WithdrawalAPIModel vm)
        {
            CashInOutSQLModel m = new CashInOutSQLModel();
            m.TransactionType = TransactionType.CashIn;
            m.WalletType = vm.PaymentGateway;
            m.Amount = Helpers.DataReaderHelper.GetFormDouble(vm.Amount);
            m.WalletNumber = "";//Assigned after the agent get the withdrawal request 
            m.MemberPhoneNo = vm.AccountNumber;
            m.TrxID = "";
            return m;
        }

        public static void SetApiResult(APIIntegration.Model.ApiResultModel result, string Status, string Message)
        {
            result.status = Status;
            result.message = Message;
         
        }

        public static bool CheckMD5Encryption(string Action, string result, string ApiKey)
        {

            MD5ResultModel md5result = ConvertApiDataToString(Action,result, ApiKey);

            using (var md5Hash = MD5.Create())
            {
                // Byte array representation of source string
                var sourceBytes = System.Text.Encoding.UTF8.GetBytes(md5result.md5);

                // Generate hash value(Byte Array) for input data
                var hashBytes = md5Hash.ComputeHash(sourceBytes);

                // Convert hash byte array to string
                var hash = BitConverter.ToString(hashBytes).Replace("-", string.Empty);
                if (hash.ToLower() == md5result.Hash.ToString().ToLower())
                {
                    return true;
                }
                // Output the MD5 hash

            }

          

            return false;
        }

        private static MD5ResultModel ConvertApiDataToString(string Action, string result, string ApiKey)
        {
            // string md5 = string.Empty;
            MD5ResultModel md5result = new MD5ResultModel();
            if (Action == "withdrawal")
            {
                WithdrawalAPIModel res = JsonConvert.DeserializeObject<WithdrawalAPIModel>(result);
                md5result.md5 = "PlayerUserName=" + res.PlayerUserName + "&PaymentGateway=" + res.PaymentGateway 
                    + "&AccountNumber=" + res.AccountNumber + "&Amount=" + res.Amount
                    + "&merchantWithdrawalId=" + res.merchantWithdrawalId + "&ApiKey=" + ApiKey;
                md5result.Hash = Helpers.DataReaderHelper.GetFormString(res.Hash);
            }
            else
            {
                DepositAPIModel res = JsonConvert.DeserializeObject<DepositAPIModel>(result);
                md5result.md5 = "PlayerUserName=" + res.PlayerUserName + "&PaymentGateway=" + res.PaymentGateway
                    + "&AccountNumber=" + res.AccountNumber + "&Amount=" + res.Amount
                    + "&merchantDepositId=" + res.merchantDepositId + "&ApiKey=" + ApiKey;
                md5result.Hash = Helpers.DataReaderHelper.GetFormString(res.Hash);
            }

          
            return md5result;
        }


    }

}
