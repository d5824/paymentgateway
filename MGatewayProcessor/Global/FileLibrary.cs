﻿using MGatewayRepository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Global.Model;
namespace MGatewayProcessor
{
    public class FileLibrary
    {
        public static int SaveFileToLibrary(string UserID, System.IO.Stream FileStream, string FileName, string FileType, int FileNo)
        {
            byte[] FileBytes;
            using (System.IO.MemoryStream ms = new System.IO.MemoryStream())
            {
                FileStream.Position = 0;
                FileStream.CopyTo(ms);
                FileBytes = ms.ToArray();
            }
            return SaveFileToLibrary(UserID, FileBytes, FileName, FileType, FileNo);


        }

        public static int SaveFileToLibrary(string UserID, byte[] FileBytes, string FileName, string FileType, int FileNo)
        {
            int FileSize = FileBytes.Length;

            using (Repository<FileLibraryModel> db = new Repository<FileLibraryModel>())
            {

                FileLibraryModel m = new FileLibraryModel();
                if (FileNo == 0)
                {
                    m = new FileLibraryModel() { FileName = FileName, FileSize = FileSize, FileType = FileType, UploadBy = UserID, FileContent = FileBytes };
                    db.Insert(m);
                    return m.FileNo;
                }
                else
                {
                    m = db.Find(FileNo);
                    if (m != null)
                    {
                        m.FileName = FileName;
                        m.FileType = FileType;
                        m.FileSize = FileSize;
                        m.UploadBy = UserID;
                        m.FileContent = FileBytes;
                        m.UploadDate = DateTime.Now;
                        db.Update(m);
                        return m.FileNo;
                    }
                }
                return 0;

            }

        }

        public static void SaveFileToLocalLibrary(int FileNo, string FileName, Stream FileStream)
        {
            int defaultHeight = 120;
            int dynamicWidth = 0;
            System.Drawing.Image image = System.Drawing.Image.FromStream(FileStream);
            dynamicWidth = image.Width / (image.Height / defaultHeight);
            System.Drawing.Image thumb = image.GetThumbnailImage(dynamicWidth, defaultHeight, () => false, IntPtr.Zero);
            DirectoryInfo FileLibraryFolder = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + "FileLibrary");
            if (!Directory.Exists(FileLibraryFolder.FullName))
            {
                FileLibraryFolder = Directory.CreateDirectory(FileLibraryFolder.FullName);
            }
            string TempFilePath = Path.Combine(FileLibraryFolder.FullName, FileName);
            string FileExtension = Path.GetExtension(TempFilePath);
            thumb.Save(Path.ChangeExtension(Path.Combine(FileLibraryFolder.FullName, FileNo.ToString()), FileExtension));
        }

        public static Dictionary<int, string> GetFileNameDict()
        {
            Dictionary<int, string> fileNoDict = new Dictionary<int, string>();
            using (Repository<FileLibraryModel> db = new Repository<FileLibraryModel>())
            {
                List<FileLibraryModel> mList = db.GetAll().ToList();
                foreach (FileLibraryModel item in mList)
                {
                    fileNoDict[item.FileNo] = item.FileName;
                }
            }
            return fileNoDict;
        }



        public static FileLibraryModel Find(int FileNo)
        {
            using (Repository<FileLibraryModel> db = new Repository<FileLibraryModel>())
            {
                return db.Find(FileNo);
            }
        }

        public static Dictionary<int, FileLibraryModel> GetFileNoDict(IEnumerable<int> FileNo)
        {
            using (Repository<FileLibraryModel> db = new Repository<FileLibraryModel>())
            {
                return db.WhereNoTracking(x => FileNo.Contains(x.FileNo)).ToDictionary(x => x.FileNo, y => y);
            }
        }

        internal static int SaveFileToLibrary(object userID, Stream inputStream, string fileName, string contentType, int v)
        {
            throw new NotImplementedException();
        }

        public static string ConvertToBase64(Stream FileStream, string FileType)
        {
            byte[] Bytes;
            using (MemoryStream ms = new MemoryStream())
            {
                FileStream.Position = 0;
                FileStream.CopyTo(ms);
                Bytes = ms.ToArray();
            }
            string base64String = Convert.ToBase64String(Bytes, 0, Bytes.Length);
            var ImageUrl = "data:" + FileType + ";base64," + base64String;
            return ImageUrl;
        }

        public static string ConvertToBase64(byte[] Bytes, string FileType)
        {
            string base64String = Convert.ToBase64String(Bytes, 0, Bytes.Length);
            var ImageUrl = "data:" + FileType + ";base64," + base64String;
            return ImageUrl;
        }

        public static string CheckFileNo(int FileNo, string FileName)
        {
            string ImgPath = "";
            if (FileNo == 0)
            {
                ImgPath = "/Img/no-photo-available.png";
            }
            else
            {
                string FileExtension = Path.GetExtension(FileName);
                ImgPath = "/FileLibrary/" + FileNo + FileExtension;


                string path = AppDomain.CurrentDomain.BaseDirectory + "FileLibrary";
                bool fileExists = false;
                DirectoryInfo FileLibraryFolder = new DirectoryInfo(path);
                foreach (FileInfo file in FileLibraryFolder.GetFiles())
                {
                    if (file.Name == FileNo + FileExtension)
                    {
                        fileExists = true;
                        break;
                    }

                }

                if (!fileExists)
                {
                    SaveFileToLocalLibraryFromDB(FileNo);
                }
            }
            return ImgPath;
        }

        public static void SaveFileToLocalLibraryFromDB(int FileNo)
        {

            using (Repository<FileLibraryModel> db = new Repository<FileLibraryModel>())
            {

                FileLibraryModel m = new FileLibraryModel();

                m = db.Find(FileNo);
                if (m != null)
                {
                    SaveFileToLocalLibrary(FileNo, m);

                }
            }

        }

        public static void SaveFileToLocalLibrary(int FileNo, FileLibraryModel File)
        {
            if (File != null)
            {
                DirectoryInfo FileLibraryFolder = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory + "FileLibrary");
                if (!Directory.Exists(FileLibraryFolder.FullName))
                {
                    FileLibraryFolder = Directory.CreateDirectory(FileLibraryFolder.FullName);
                }

                byte[] FileBytes = File.FileContent;
                string FileExtension = Path.GetExtension(File.FileName);
                string TempKey = FileNo + FileExtension;
                using (FileStream fs = new FileStream(Path.Combine(FileLibraryFolder.FullName, Path.GetFileName(TempKey)), FileMode.Create, FileAccess.Write))
                {
                    using (BinaryWriter bw = new BinaryWriter(fs))
                    {
                        bw.Write(FileBytes, 0, FileBytes.Length);
                    }
                }
            }



        }
    }



}

