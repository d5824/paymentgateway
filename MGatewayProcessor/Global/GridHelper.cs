﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Global.Model;
using System.Web.Routing;
using MGatewayRepository;
using System.Data;
using Common;
using MGatewayProcessor.Helpers;
namespace MGatewayProcessor
{
    public class GridHelper
    {

        public static void AddFields(string tableName, JsGridModel m, HashSet<string> hideColumns = null)
        {
            hideColumns = (hideColumns == null ? new HashSet<string>() : hideColumns);
            List<GBL_GridLayoutModel> GridLayoutList = GetGridLayout(tableName);
            if (GridLayoutList.Count > 0)
            {
                foreach (GBL_GridLayoutModel item in GridLayoutList)
                {
                    JsGridFieldsModel field = new JsGridFieldsModel();
                    field.name = item.FieldName;
                    field.title = item.FieldTitle;
                    field.type = item.FieldType;
                    field.editing = item.Editable;
                   // field.visible = item.Visible;
                    field.visible = (hideColumns.Contains(field.name) ? false : true);
                    field.width = item.FieldWidth;
                    field.validate = (item.Editable ? "" : "required");
                    field.css = item.css;

                    if (field.type == JsGridColumnType.JsGridComboBox)
                    {
                        field.valueField = "Id";
                        field.textField = "Name";
                        field.items = item.items;
                    }


                    m.fields.Add(field);
                }
            }
            else
            {
                DynamicBuildFromDatabase(tableName, m, hideColumns);
            }


        }

        private static void DynamicBuildFromDatabase(string tableName, JsGridModel m, HashSet<string> hideColumns = null)
        {
            hideColumns = (hideColumns == null ? new HashSet<string>() : hideColumns);
            string sql = "Select distinct COLUMN_NAME,DATA_TYPE,IS_NULLABLE from Information_Schema.columns where table_name = @p0 order by ORDINAL_POSITION";
            DataTable tbl = MySQLDB.GetDataTable(sql, tableName);
            if (tableName == "TrxInfo")
            {
                DataTable tblclone = tbl.Copy();
                for (int i = 0; i < tbl.Rows.Count; i++)
                {

                    DataRow newRow = tblclone.NewRow();

                    var row = tbl.Rows[i];
                    if (row["COLUMN_NAME"].ToString() == "Currency")
                    {
                        newRow.ItemArray = row.ItemArray;
                        tblclone.Rows.Remove(tblclone.Rows[i]);
                        tblclone.Rows.InsertAt(newRow, 1);

                    }

                    if (row["COLUMN_NAME"].ToString() == "Amount")
                    {
                        newRow.ItemArray = row.ItemArray;
                        tblclone.Rows.Remove(tblclone.Rows[i]);
                        tblclone.Rows.InsertAt(newRow, 2);

                    }

                }
                DataRow ActionRow = tblclone.NewRow();
                ActionRow["COLUMN_NAME"] = "Action";
                tblclone.Rows.InsertAt(ActionRow, 3);
                tblclone.AcceptChanges();

                tbl = tblclone.Copy();

            }

            if (tableName == "Shop")
            {
                DataTable tblclone = tbl.Copy();

                DataRow ActionRow = tblclone.NewRow();
                ActionRow["COLUMN_NAME"] = "Wallets";
                tblclone.Rows.InsertAt(ActionRow, 99);
                tblclone.AcceptChanges();

                tbl = tblclone.Copy();

            }

            foreach (DataRow row in tbl.Rows)
            {
                JsGridFieldsModel field = new JsGridFieldsModel();
                bool nullable = DataReaderHelper.GetFormBoolean(row["IS_NULLABLE"]);
                field.name = row["COLUMN_NAME"].ToString();
                field.title = System.Text.RegularExpressions.Regex.Replace(row["COLUMN_NAME"].ToString(), @"((?<=\p{Ll})\p{Lu})|((?!\A)\p{Lu}(?>\p{Ll}))", " $0"); //row["COLUMN_NAME"].ToString();


                field.type = "text";
                field.editing = nullable;
                if (field.name == "AccountClass")
                {



                    field.type = "select";
                    List<JsGridSelectItemModel> combo = new List<JsGridSelectItemModel>();
                    JsGridSelectItemModel item = new JsGridSelectItemModel();
                    item.Id = "";
                    item.Name = "";
                    combo.Add(item);
                    item = new JsGridSelectItemModel();
                    item.Id = "1";
                    item.Name = "Gold";
                    combo.Add(item);
                    item = new JsGridSelectItemModel();
                    item.Id = "2";
                    item.Name = "Silver";
                    combo.Add(item);

                    field.items = combo;
                    field.valueField = "Id";
                    field.textField = "Name";
                    field.editing = true;
                }

                field.visible = (hideColumns.Contains(field.name) ? false : true);

                field.width = 140;

                field.validate = (nullable ? "" : "required");

                m.fields.Add(field);
            }
        }

        private static List<GBL_GridLayoutModel> GetGridLayout(string tableName)
        {
            List<GBL_GridLayoutModel> m = new List<GBL_GridLayoutModel>();
            switch (tableName)
            {
                case "ShopWallets":
                    m = GetShopWalletsLayout();
                    break;
                case "CashInOutLog":
                case "CashInOut":
                    m = GetCashInOutLayout(tableName);
                    break;
             
                case "SMSRecord":
                    m = GetSMSRecordLayout();
                    break;
                default:
                    // code block
                    break;
            }

            return m;
        }

        private static List<GBL_GridLayoutModel> GetShopWalletsLayout()
        {
            List<GBL_GridLayoutModel> m = new List<GBL_GridLayoutModel>();
            List<JsGridSelectItemModel> WalletList = GetDropDownList(typeof(MGatewayBO.Model.WalletType));
            m.Add(new GBL_GridLayoutModel { FieldName = "MerchantID", FieldTitle = "MerchantID", Visible = false, Editable = false, seq = 1, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 140, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "ShopID", FieldTitle = "ShopID", Visible = false, Editable = false, seq = 1, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 140, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "ShopName", FieldTitle = "Shop Name", Visible = true, Editable = false, seq = 1, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 140, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "WalletType", FieldTitle = "Wallet Type", Visible = true, Editable = false, seq = 1, FieldType = JsGridColumnType.JsGridComboBox, FieldWidth = 140, FieldRequired = true, items = WalletList });
            m.Add(new GBL_GridLayoutModel { FieldName = "WalletNumber", FieldTitle = "Wallet Number", Visible = true, Editable = false, seq = 2, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 140, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "CashOutLimit", FieldTitle = "Cash Out Limit", Visible = true, Editable = true, seq = 3, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 140, FieldRequired = true  });
            m.Add(new GBL_GridLayoutModel { FieldName = "CashInLimit", FieldTitle = "Cash In Limit", Visible = true, Editable = true, seq = 4, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 140, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "AccountClass", FieldTitle = "Account Class", Visible = true, Editable = true, seq = 5, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 140, FieldRequired = true });
            List<JsGridSelectItemModel> PriorityList = GetDropDownList(typeof(MGatewayBO.Model.ShopPriority));
            m.Add(new GBL_GridLayoutModel { FieldName = "Priority", FieldTitle = "Priority", Visible = true, Editable = true, seq = 6, FieldType = JsGridColumnType.JsGridComboBox, FieldWidth = 140, FieldRequired = true, items = PriorityList });
            m.Add(new GBL_GridLayoutModel { FieldName = "Enabled", FieldTitle = "Enabled", Visible = true, Editable = true, seq = 7, FieldType = JsGridColumnType.JsGridCheckBox, FieldWidth = 140, FieldRequired = true });

            return m;
        }

        private static List<GBL_GridLayoutModel> GetCashInOutLayout(string tableName)
        {
            List<GBL_GridLayoutModel> m = new List<GBL_GridLayoutModel>();

            m.Add(new GBL_GridLayoutModel { FieldName = "ID", FieldTitle = "ID", Visible = true, Editable = false, seq = 1, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 140, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "TrxID", FieldTitle = "Trx ID", Visible = true, Editable = false, seq = 2, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 140, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "Action", FieldTitle = "Action", Visible = true, Editable = true, seq = 3, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 200, FieldRequired = true });
            if(tableName == "CashInOutLog")
            {
                m.Add(new GBL_GridLayoutModel { FieldName = "LastModified", FieldTitle = "Delete By", Visible = true, Editable = true, seq = 2, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 200, FieldRequired = true });
                m.Add(new GBL_GridLayoutModel { FieldName = "LastModifiedDateTime", FieldTitle = "Delete Date Time", Visible = true, Editable = true, seq = 2, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 200, FieldRequired = true });
            }
            
            m.Add(new GBL_GridLayoutModel { FieldName = "ShopName", FieldTitle = "Shop Name", Visible = true, Editable = false, seq = 3, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 100, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "WalletType", FieldTitle = "Wallet Type", Visible = true, Editable = false, seq = 2, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 100, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "WalletNumber", FieldTitle = "Wallet Number", Visible = true, Editable = true, seq = 3, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 140, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "WalletAccountClass", FieldTitle = "Wallet Class", Visible = true, Editable = true, seq = 4, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 50, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "MemberID", FieldTitle = "Member ID", Visible = true, Editable = true, seq = 5, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 140, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "MemberPhoneNo", FieldTitle = "Member PhoneNo", Visible = true, Editable = true, seq = 6, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 140, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "Amount", FieldTitle = "Amount", Visible = true, Editable = true, seq = 7, FieldType = JsGridColumnType.JsGridNumericTextBox, FieldWidth = 100, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "Operator", FieldTitle = "Operator", Visible = true, Editable = true, seq = 8, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 100, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "Status", FieldTitle = "Status", Visible = true, Editable = true, seq = 9, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 100, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "Remark", FieldTitle = "Remark", Visible = true, Editable = true, seq = 10, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 140, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "FileName", FieldTitle = "Receipt", Visible = true, Editable = true, seq = 11, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 140, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "CreatedTime", FieldTitle = "CreatedTime", Visible = true, Editable = true, seq = 12, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 140, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "ApprovedTime", FieldTitle = "ApprovedTime", Visible = true, Editable = true, seq = 13, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 140, FieldRequired = true });
            

            return m;
        }

        private static List<GBL_GridLayoutModel> GetSMSRecordLayout()
        {
            List<GBL_GridLayoutModel> m = new List<GBL_GridLayoutModel>();

            m.Add(new GBL_GridLayoutModel { FieldName = "ID", FieldTitle = "ID", Visible = true, Editable = false, seq = 1, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 140, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "MerchantID", FieldTitle = "MerchantID", Visible = true, Editable = false, seq = 2, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 140, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "Matched", FieldTitle = "Matched", Visible = true, Editable = false, seq = 2, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 80, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "TrxID", FieldTitle = "TrxID", Visible = true, Editable = false, seq = 2, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 80, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "ShopName", FieldTitle = "Shop Name", Visible = true, Editable = false, seq = 3, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 50, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "ShopID", FieldTitle = "ShopID", Visible = true, Editable = false, seq = 3, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 140, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "WalletType", FieldTitle = "Wallet Type", Visible = true, Editable = false, seq = 2, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 50, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "SMSType", FieldTitle = "SMS Type", Visible = true, Editable = true, seq = 3, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 50, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "Amount", FieldTitle = "Amount", Visible = true, Editable = true, seq = 7, FieldType = JsGridColumnType.JsGridNumericTextBox, FieldWidth = 50, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "AccountNumber", FieldTitle = "Account Number", Visible = true, Editable = true, seq = 8, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 100, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "Sender", FieldTitle = "Sender", Visible = true, Editable = true, seq = 9, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 50, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "Balance", FieldTitle = "Balance", Visible = true, Editable = true, seq = 10, FieldType = JsGridColumnType.JsGridNumericTextBox, FieldWidth = 100, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "SMSDeliveryTime", FieldTitle = "SMS Delivery Time", Visible = true, Editable = true, seq = 11, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 140, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "CreatedTime", FieldTitle = "Created Time", Visible = true, Editable = true, seq = 12, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 140, FieldRequired = true });
            m.Add(new GBL_GridLayoutModel { FieldName = "RawMessage", FieldTitle = "Raw Message", Visible = true, Editable = true, seq = 13, FieldType = JsGridColumnType.JsGridTextBox, FieldWidth = 140, FieldRequired = true });

            return m;
        }


        private static List<JsGridSelectItemModel> GetDropDownList(Type EnumType)
        {
            Dictionary<int, string> EnumDict = DataReaderHelper.GetEnumDict(EnumType);
            List<JsGridSelectItemModel> m = new List<JsGridSelectItemModel>();
            m.Add(new JsGridSelectItemModel { Id = "", Name = "" });
            foreach (KeyValuePair<int, string> item in EnumDict)
            {
                JsGridSelectItemModel vm = new JsGridSelectItemModel();
                vm.Id = item.Key.ToString();
                vm.Name = item.Value;
                m.Add(vm);
            }

            return m;
        }





    }
}