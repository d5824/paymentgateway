﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MGatewayProcessor.Helpers
{
    public class DataReaderHelper
    {
        public static DateTime RowToDate(object o)
        {
            if(o is DateTime)
            {
                return (DateTime)o;
            }
               
            else
            {
                return DateTime.MinValue;
            }
               
           
        }

        public static int RowToInt(object o)
        {
            if (o is int)
            {
                return (int)o;
            }

            else
            {
                return 0;
            }
        }


        public static decimal RowToDecimal(object o)
        {
            if (o is decimal)
            {
                return (decimal)o;
            }

            else
            {
                return 0;
            }
        }


        public static string GetFormString(object str)
        {
            if(str == null)
            {
                return string.Empty;
            }
            else
            {
                return str.ToString().Trim();
            }
        }
        public static DateTime GetFormDate(string strDate)
        {
            if (strDate == string.Empty)
            {
                return DateTime.MinValue;
            }
            else
            {
                DateTime dt = DateTime.MinValue;
                if (DateTime.TryParseExact(strDate, Common.Common.gDisplayDateFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dt))
                { 
                    return dt;
                }
                else
                { 
                    return DateTime.MinValue;
                }
                   
            }
               
                    
           
        }

        public static DateTime GetFormDateTime(string strDate)
        {
            if (strDate == string.Empty)
            {
                return DateTime.MinValue;
            }
            else
            {
                DateTime dt = DateTime.MinValue;
                if (DateTime.TryParseExact(strDate, Common.Common.gDisplayDateTimeFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dt))
                {
                    return dt;
                }
                else
                {
                    return DateTime.MinValue;
                }

            }



        }

        public static string GetCompareCurrentDate(DateTime DateStamp)
        {

            double totalSeconds = DateTime.Now.Subtract(DateStamp).TotalSeconds;
            TimeSpan t = TimeSpan.FromSeconds(totalSeconds);

            string answer= "";
            if(t.Days > 0)
            {
                answer = t.Days.ToString() + " day ";
            }

            if (t.Hours > 0)
            {
                answer += t.Hours.ToString() + " hr ";
            }

            if (t.Minutes > 0 && t.Days <= 0)
            {
                answer += t.Minutes.ToString() + " min ";
            }

            if (t.Seconds > 0 && t.Hours <= 0)
            {
                answer += t.Seconds.ToString() + " sec ";
            }



            return answer;
        }


        public static bool GetFormBoolean(object str)
        {
            if (str == null)
            {
                return false;
            }
            else
            {
                
                if (str.ToString().ToLower() == "yes" || str.ToString().ToLower() == "1" || str.ToString().ToLower() == "true" || str.ToString().ToLower() == "active")
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }

        public static DateRange GetFormDateRange(string strDate)
        {
            if (strDate == string.Empty || strDate == null)
            {
                return new DateRange();
            }
            else
            {
                string[] DateList = strDate.Split(new string[] { " - " }, StringSplitOptions.RemoveEmptyEntries);
                DateTime StartDate = GetFormDate(DateList[0]);
                DateTime EndDate = GetFormDate(DateList[1]).AddHours(23).AddMinutes(59);
                return new DateRange(StartDate, EndDate);
            }
        }

        public static DateRange GetFormDateRangeTime(string strDate)
        {
            if (strDate == string.Empty || strDate == null)
            {
                return new DateRange();
            }
            else
            {
                string[] DateList = strDate.Split(new string[] { " - " }, StringSplitOptions.RemoveEmptyEntries);
                DateTime StartDate = GetFormDateTime(DateList[0]);
                DateTime EndDate = GetFormDateTime(DateList[1]);
                return new DateRange(StartDate, EndDate);
            }
        }


        public static int GetFormInt(string str)
        {
            int Result;
            if (str == null)
                return 0;
            else if (int.TryParse(str, out Result))
                return Result;
            else
                return 0;
        }

        public static double GetFormDouble(string str)
        {
            double Result;
            if (str == null)
                return 0;
            else if (double.TryParse(str, out Result))
                return Result;
            else
                return 0;
        }




        public static decimal NumberToDec(int number)
        {
            decimal result;
            if(decimal.TryParse(number.ToString(),out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }


        public static decimal NumberToDec(double number)
        {
            decimal result;
            if (decimal.TryParse(number.ToString(), out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }

        public static HashSet<string> GetEnumHash(Type EnumType)
        {
            HashSet<string> EnumHash = new HashSet<string>();
            foreach(var Value in Enum.GetValues(EnumType))
            {
                string EnumName = Enum.GetName(EnumType, Value);
                EnumHash.Add(EnumName);
            }

            return EnumHash;
        }

        public static Dictionary<int, string> GetEnumDict(Type EnumType)
        {
            Dictionary<int, string> EnumDict = new Dictionary<int, string>();
            foreach (var Value in Enum.GetValues(EnumType))
            {
                int EnumNumber = Value.GetHashCode();
                var fds = EnumType.GetField(Value.ToString()).GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), true);
                string EnumName = Enum.GetName(EnumType, Value);
                foreach (System.ComponentModel.DescriptionAttribute fd in fds)
                {
                    EnumDict[EnumNumber] = fd.Description;
                }
               

            }

            return EnumDict;
        }

    }

   
}