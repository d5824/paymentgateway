﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MGatewayProcessor.Helpers
{
    public class DateRange : IEquatable<DateRange>
    {
        private DateTime _StartDate;
        private DateTime _EndDate;

        public DateRange()
        {
            _StartDate = DateTime.MinValue;
            _EndDate = DateTime.MaxValue;
        }

        public DateRange(DateTime StartDate,DateTime EndDate)
        {
            _StartDate = StartDate;
            _EndDate = EndDate;
        }
        public DateTime StartDate
        {
            get
            {
                return _StartDate;
            }
            set
            {
                _StartDate = value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return _EndDate;
            }
            set
            {
                _EndDate = value;
            }
        }



        public bool Equals(DateRange other)
        {
            return (other.StartDate == this.StartDate && other.EndDate == this.EndDate);
        }

        public override bool Equals(object obj)
        {
            if (obj is null)
                return base.Equals(obj);
            if (!(obj is DateRange))
            {
                throw new InvalidCastException(obj.GetType().ToString() + " cannot be cast to DateRange.");
            }
            else
            {
                return Equals((DateRange)obj);
            }
        }

        public override int GetHashCode()
        {
            object obj = _StartDate;
            return obj.GetHashCode();
        }

       

    }

    public class DateTimeSpan
    {
        private TimeSpan _StartDate;
        private TimeSpan _EndDate;

        public DateTimeSpan()
        {
            _StartDate = DateTime.Now.TimeOfDay;
            _EndDate = DateTime.Now.TimeOfDay;
        }

        public DateTimeSpan(TimeSpan StartDate, TimeSpan EndDate)
        {
            _StartDate = StartDate;
            _EndDate = EndDate;
        }


        public TimeSpan StartDate
        {
            get
            {
                return _StartDate;
            }
            set
            {
                _StartDate = value;
            }
        }

        public TimeSpan EndDate
        {
            get
            {
                return _EndDate;
            }
            set
            {
                _EndDate = value;
            }
        }


    }
    public class DateHelper
    {
        public static DateRange GetLastWeekDateRange(DateTime StartDate)
        {

            int days = StartDate.DayOfWeek - DayOfWeek.Monday;
            DateTime pastDate = StartDate.AddDays(-(days + 7));
            DateTime futureDate = pastDate.AddDays(6).AddHours(23).AddMinutes(59);

            return new DateRange(pastDate, futureDate);
        }


        public static DateRange GetThisWeekDateRange(DateTime StartDate)
        {

            int days = StartDate.DayOfWeek - DayOfWeek.Monday;
            DateTime pastDate = StartDate.AddDays(-(days));
            DateTime futureDate = pastDate.AddDays(6).AddHours(23).AddMinutes(59).AddSeconds(59);

            return new DateRange(pastDate, futureDate);
        }


        public static DateRange GetThisMonthDateRange(DateTime StartDate)
        {

            return GetMonthRange(StartDate);
        }


        public static DateRange GetLastMonthDateRange(DateTime StartDate)
        {

            StartDate = StartDate.AddMonths(-1);
            return GetMonthRange(StartDate);
        }

        private static DateRange GetMonthRange(DateTime StartDate)
        {
            DateTime pastDate = DateSerial(StartDate.Year, StartDate.Month, 1);
            DateTime futureDate = DateSerial(pastDate.Year, pastDate.Month, DateTime.DaysInMonth(pastDate.Year, pastDate.Month)).AddHours(23).AddMinutes(59).AddSeconds(59);

            return new DateRange(pastDate, futureDate);
        }


        public static DateRange GetThisYearDateRange(DateTime StartDate)
        {
            return GetYearRange(StartDate);
        }

        public static DateRange GetLastYearDateRange(DateTime StartDate)
        {
            StartDate = StartDate.AddYears(-1);
            return GetYearRange(StartDate);
        }

        private static DateRange GetYearRange(DateTime StartDate)
        {
            DateTime pastDate = DateSerial(StartDate.Year, 1, 1);
            DateTime futureDate = DateSerial(pastDate.Year, 12, 31).AddHours(23).AddMinutes(59).AddSeconds(59);

            return new DateRange(pastDate, futureDate);
        }


        public static DateTime DateSerial(int year, int month, int day)
        {
            if (year < 0)
            {
                year = DateTime.Now.Year + year;
            }
            else if (year < 100)
            {
                year = 1930 + year;
            }
            DateTime dt = new DateTime(year, 1, 1);
            dt = dt.AddMonths(month - 1);
            dt = dt.AddDays(day - 1);

            return dt;
        }

        public static List<string> GetWeekList()
        {
            List<string> WeekList = new List<string>();
            for (int i = 1; i <= 7; i++)
            {
                string WeekName = string.Empty;
                switch (i)
                {
                    case 1:
                        WeekName = "Monday";
                        break;
                    case 2:
                        WeekName = "Tuesday";

                        break;
                    case 3:
                        WeekName = "Wednesday";
                        break;
                    case 4:
                        WeekName = "Thursday";
                        break;
                    case 5:
                        WeekName = "Friday";

                        break;
                    case 6:
                        WeekName = "Saturday";

                        break;
                    case 7:
                        WeekName = "Sunday";

                        break;
                    default:

                        break;
                }

                WeekList.Add(WeekName);
            }

            return WeekList;


        }

        public static List<DateRange> GetDateRangeByDay(DateRange DateRange)
        {
            List<DateRange> DateRangeList = new List<DateRange>();
            DateTime StartDate = DateRange.StartDate;
            DateTime EndDate = DateRange.EndDate;

            do
            {
                DateRangeList.Add(new DateRange(StartDate, StartDate.AddHours(23).AddMinutes(59).AddSeconds(59)));
                StartDate = StartDate.AddDays(1);

            } while (StartDate <= EndDate);

            return DateRangeList;

        }

        public static Dictionary<string, DateRange> ConvertDateRangeToDict(List<DateRange> DateRangeList)
        {
            Dictionary<string, DateRange> dateDict = new Dictionary<string, DateRange>();
            foreach (DateRange dr in DateRangeList)
            {
                dateDict[dr.StartDate.ToString(Common.Common.gDisplayDateFormat)] = dr;
            }

            return dateDict;
        }

        public static string GetDateDictKey(DateTime dateTime, Dictionary<string, DateRange> dateRangeDict)
        {
            string dictkey = dateRangeDict.Where(x => dateTime >= x.Value.StartDate && dateTime <= x.Value.EndDate).Select(x => x.Key).FirstOrDefault();
            return dictkey;
        }


        public static DateRange CombineRange(DateRange DateRange1, DateRange DateRange2)
        {
            DateRange ResultRange = new DateRange();
            if (DateRange1.StartDate == DateTime.MinValue)
            {
                return DateRange2;
            }

            if (DateRange2.StartDate == DateTime.MinValue)
            {
                return DateRange1;
            }

            if (DateRange1.StartDate < DateRange2.StartDate)
            {
                ResultRange.StartDate = DateRange1.StartDate;
            } else
            {
                ResultRange.StartDate = DateRange2.StartDate;
            }

            if (DateRange1.EndDate > DateRange2.EndDate)
            {
                ResultRange.EndDate = DateRange1.EndDate;
            }
            else
            {
                ResultRange.EndDate = DateRange2.EndDate;
            }

            return ResultRange;



        }


        public static Dictionary<string, DateTimeSpan> BuildTimeByHoursDict()
        {
            Dictionary<string, DateTimeSpan> TimeDict = new Dictionary<string, DateTimeSpan>();
            DateTime CurrentDate = DateTime.Now.Date;
            for (int i = 1; i <= 12; i++)
            {
                DateTimeSpan date = new DateTimeSpan();
                string DictKey = string.Empty;
                date.StartDate = CurrentDate.TimeOfDay;
                DictKey = CurrentDate.ToShortTimeString();
                CurrentDate = CurrentDate.AddHours(2);
                date.EndDate = CurrentDate.AddMinutes(-1).AddSeconds(59).TimeOfDay;
                TimeDict[DictKey] = date;

            }
            return TimeDict;
        }

        public static Dictionary<string, Dictionary<string, int>> BuildTimeTableDict()
        {
            //Monday , list of time with count
            Dictionary<string, Dictionary<string, int>> TimeTableDict = new Dictionary<string, Dictionary<string, int>>();
            List<string> WeekList = DateHelper.GetWeekList();
            Dictionary<string, DateTimeSpan> TimeDict = BuildTimeByHoursDict();
            foreach (string week in WeekList)
            {
                Dictionary<string, int> TimeList = new Dictionary<string, int>();
                foreach (KeyValuePair<string, DateTimeSpan> item in TimeDict)
                {
                    TimeList.Add(item.Key, 0);
                }
                TimeTableDict[week] = TimeList;
            }
            return TimeTableDict;

        }

        public static string GetTimeDictKey(TimeSpan dateTime, Dictionary<string, DateTimeSpan> dateTimeRangeDict)
        {
            string dictkey = dateTimeRangeDict.Where(x => dateTime >= x.Value.StartDate && dateTime <= x.Value.EndDate).Select(x => x.Key).FirstOrDefault();
            return dictkey;
        }


        public static Dictionary<string, int> GetWeekNameCountDict(DateRange currentRange)
        {
            DateTime StartDate = currentRange.StartDate;
            Dictionary<string, int> WeekNameCountDict = new Dictionary<string, int>();
            do
            {
                if (WeekNameCountDict.ContainsKey(StartDate.DayOfWeek.ToString()))
                {
                    WeekNameCountDict[StartDate.DayOfWeek.ToString()] += 1;
                }
                else
                {
                    WeekNameCountDict[StartDate.DayOfWeek.ToString()] = 1;
                }
                StartDate = StartDate.AddDays(1);

            } while (StartDate <= currentRange.EndDate);

            return WeekNameCountDict;
        }


        public static TimeSpan GetAverageTime(List<TimeSpan> TimeList){
            TimeSpan avgTime = new TimeSpan();

            double totalMs = TimeList.Average(x => x.TotalMilliseconds);

            avgTime = TimeSpan.FromMilliseconds(totalMs);

            return avgTime;
         }

        public static double GetAverageTimeMs(List<TimeSpan> TimeList)
        {
            //double avgTime;

            double totalMs = (TimeList.Count > 0 ? TimeList.Average(x => x.TotalMilliseconds) : 0);

            

            return totalMs;
        }



    }

  
}