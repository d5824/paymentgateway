﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
public static class Extension
{
    public static List<T> Join<T>(this List<T> first, List<T> second)
    {
        if (first == null)
        {
            return second;
        }
        if (second == null)
        {
            return first;
        }

        return first.Concat(second).ToList();
    }

    public static HashSet<T> Join<T>(this HashSet<T> first, HashSet<T> second)
    {
        if (first == null)
        {
            return second;
        }
        if (second == null)
        {
            return first;
        }

        return new HashSet<T>(first.Concat(second).ToList());
    }


}

public static class EnumExtensionMethods
{
    public static string GetEnumDescription(this Enum enumValue)
    {
        var fieldInfo = enumValue.GetType().GetField(enumValue.ToString());

        var descriptionAttributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);

        return descriptionAttributes.Length > 0 ? descriptionAttributes[0].Description : enumValue.ToString();
    }
}

[Obsolete("To Do Task")]
public class ToDo : Attribute
{
    public string msg { get; set; }

    public ToDo(string _msg)
    {
        msg = _msg;

        

    }
  
}

[Obsolete("Function to be improve")]
public class ToImprove : Attribute
{
    public string msg { get; set; }

    public ToImprove(string _msg)
    {
        msg = _msg;
    }
}
