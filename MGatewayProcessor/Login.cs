﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Encryptor;
using MGatewayRepository;
using MGatewayBO.Model;

namespace MGatewayProcessor
{
    public class Login
    {
      public static void TestConnection()
        {
           
            using (Repository<UserLoginModel> db = new Repository<UserLoginModel>())
            {
                UserLoginModel m = db.Find("");
               // db.Delete(m);
            }


            
        }


        public static LoginResultModel CheckLogin(string username, string password)
        {
            string msg = string.Empty;
            LoginResultModel loginResult = new LoginResultModel();


            UserLoginModel m = MySQLDB.GetValueModel<UserLoginModel>("Select * from UserLogin where UserName = @p0", username).FirstOrDefault();
            AccStatus AccountStatus = AccStatus.Active;
            
            if (TryLogin(m, password))
            {
                loginResult.UserID = m.UserID;
                loginResult.UserName = m.UserName;
                loginResult.MerchantID = m.MerchantID;
                loginResult.UserRole = m.UserRole;
                AccountStatus = m.Status;
                if (m.Status == AccStatus.AccSuspended)
                {
                    AccountStatus = AccStatus.AccSuspended;
                }
                else if (m.Status == AccStatus.ChangePassword)
                {

                    AccountStatus = AccStatus.ChangePassword;
                }
            }
            else
            {
                AccountStatus = AccStatus.AccessDenied;
            }
            loginResult.AccountStatus = AccountStatus;
            return loginResult;

        }

        private static bool TryLogin(UserLoginModel m, string password)
        {
            DateTime CurrentDate = DateTime.Now.Date;
            if (m != null)
            {
                string EncryptedPassword = Encryptor.Encryptor.GetHash(m.UserName.ToLower() + "LDKEY" + password + "ldkey" + m.UserName.ToUpper());
                if (m.UserPassword == EncryptedPassword )
                {
                    SetGlobalProperties(m);
                    UpdateLogin(m);
                    return true;
                }
            }

            return false;
        }

        private static void UpdateLogin(UserLoginModel m)
        {
            using (Repository<UserLoginModel> db = new Repository<UserLoginModel>())
            {

                UserLoginModel Login = db.Find(m.UserID);
                if (Login != null)
                {
                    Login.LastLoginDate = DateTime.Now;
                    db.Update(Login);
                }

            }
        }
        private static void SetGlobalProperties(UserLoginModel m)
        {
          
        }

    }
}