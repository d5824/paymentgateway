﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MGatewayBO.Model;
using Global.Model;
using MGatewayRepository;
using System.Text;
using System.Collections.Concurrent;

namespace MGatewayProcessor
{
    public class CashTransaction
    {
        private static string LockAutoNumber = "LockAutoNumber";
        public static string AutoMachedMsg = "Transaction successfully added. Transaction matched.";
        private static ConcurrentDictionary<string, List<CashInOutViewModel>> _CacheTransInfo = new ConcurrentDictionary<string, List<CashInOutViewModel>>();
        public static JsGridModel GetCashTransactionGrid(string UserRole, string MerchantID, TransactionType trxType, string UserID, string _StartDate, string _EndDate)
        {
            DateTime StartDate = Helpers.DataReaderHelper.GetFormDate(_StartDate);
            DateTime EndDate = Helpers.DataReaderHelper.GetFormDate(_EndDate);
            EndDate = EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);
            JsGridModel m = new JsGridModel();
            using (MGatewayDB db = new MGatewayDB())
            {
                string SQL = "Select t1.*,IFNULL(t2.ShopName,'') ShopName,IFNULL(t3.FileName,'') FileName from CashInOut t1 " + Environment.NewLine +
                    "left join Shop t2 on t1.ShopID = t2.ShopID" + Environment.NewLine +
                    "left join FileLibrary t3 on t3.FileNo = t1.FileNo" + Environment.NewLine +
                    "where ApprovedTime between @p0 and @p1 and TransactionType = @p2 and t1.MerchantID = @p3 order by t1.ID desc";
                //if(UserID == "7c06b2db2f9e4dccad6d165444d555f0")
                //{
                //    SQL = "Select * from TrxInfo where CreatedTime >=  @p1 and CreatedTime <= @p2";
                //}
                List<CashInOutViewModel> TransactionList = ConvertToVm(db.Database.SqlQuery<CashInOutSQLModel>(SQL, StartDate, EndDate, trxType, MerchantID).ToList());

                _CacheTransInfo[UserID] = TransactionList;
                m.data = TransactionList;
                m.AllowAdd = false;
                m.AllowDelete = true;
                m.AllowEdit = false;
                m.HasDetails = false;
                m.ServerSideLoading = true;
                m.KeyFieldName = "ID";
                HashSet<string> hideColumns = null;
                if (UserRole == "Merchant")
                {
                    hideColumns = new HashSet<string>() { "Action" };
                }
                GridHelper.AddFields("CashInOut", m, hideColumns);


                m.DeleteURL.urlAction = new ControllerURL { action = "DeleteRecord", area = "", controller = "CashTransaction" };
                m.ViewURL.urlAction = new ControllerURL { action = "", area = "", controller = "CashTransaction" };
                m.LoadDataURL.urlAction = new ControllerURL { action = "LoadCashTransaction", area = "", controller = "CashTransaction" };
                m.ViewCallBack.Parentelem = "parentElement";
                m.ViewCallBack.childelem = "chilElement";
                m.ViewCallBack.childGridID = "TransactionDetails";


                //m.ViewCallBack.Parentelem = "parentElement";
                //m.ViewCallBack.childelem = "chilElement";
                //m.ViewCallBack.childGridID = "UserDetails";

                return m;
            }



        }

        public static List<CashInOutViewModel> ConvertToVm(List<CashInOutSQLModel> m)
        {

            List<CashInOutViewModel> vm = new List<CashInOutViewModel>();
            foreach (CashInOutSQLModel item in m)
            {
                CashInOutViewModel vmitem = new CashInOutViewModel();
                vmitem.ID = item.ID;
                vmitem.TrxID = Helpers.DataReaderHelper.GetFormString(item.TrxID).Replace("\t", "").Replace("\r", "");
                vmitem.TransactionType = item.TransactionType.ToString();
                vmitem.ShopID = item.ShopID;
                vmitem.MerchantID = item.MerchantID;
                vmitem.ShopName = item.ShopName;
                vmitem.WalletType = item.WalletType.ToString();
                vmitem.WalletNumber = item.WalletNumber.Replace("\t","").Replace("\r","");
                vmitem.WalletAccountClass = item.WalletAccountClass;
                vmitem.MemberID = item.MemberID;
                vmitem.MemberPhoneNo = Helpers.DataReaderHelper.GetFormString(item.MemberPhoneNo).Replace("\t", "").Replace("\r", "");
                vmitem.Amount = String.Format("{0:n0}", item.Amount);
                vmitem.Operator = item.Operator;
                vmitem.Status = item.Status.ToString();
                vmitem.Remark = item.Remark;
                // vmitem.CreatedTime = Helpers.DataReaderHelper.GetCompareCurrentDate(item.CreatedTime) + " ago";//item.CreatedTime.ToString(Common.Common.gDisplayDateTimeFormat);
                vmitem.CreatedTime = item.CreatedTime.ToString(Common.Common.gDisplayDateTimeFormat) + " (" + Helpers.DataReaderHelper.GetCompareCurrentDate(item.CreatedTime) + " ago" + ")";


                //vmitem.ApprovedTime = Helpers.DataReaderHelper.GetCompareCurrentDate(item.ApprovedTime) + " ago";
                vmitem.ApprovedTime = item.ApprovedTime.ToString(Common.Common.gDisplayDateTimeFormat) + " (" + Helpers.DataReaderHelper.GetCompareCurrentDate(item.ApprovedTime) + " ago" + ")";

                vmitem.Action = item.Action.ToString();
                vmitem.FileName = item.FileName;
                vmitem.FileNo = item.FileNo;
                vm.Add(vmitem);


            }
            return vm;
        }


        public static List<CashInOutViewModel> GetTransactionInfoCache(string UserID)
        {
            if (_CacheTransInfo.ContainsKey(UserID))
            {
                return _CacheTransInfo[UserID];
            }
            else
            {
                return new List<CashInOutViewModel>();
            }
        }

        public static void UpdateTransactionInfoCache(string UserID, List<CashInOutViewModel> vm)
        {
            if (_CacheTransInfo.ContainsKey(UserID))
            {
                _CacheTransInfo[UserID] = vm;
            }

        }


        public static void RemoveTransactionFromCache(string UserID, CashInOutViewModel vm)
        {
            if (_CacheTransInfo.ContainsKey(UserID))
            {
                CashInOutViewModel removeItem = new CashInOutViewModel();
                foreach (CashInOutViewModel item in _CacheTransInfo[UserID])
                {
                    if (item.ID == vm.ID)
                    {
                        removeItem = item;
                    }

                }

                _CacheTransInfo[UserID].Remove(removeItem);


            }

        }



        public static CashTransactionGridFilterResult PerformFilter(string UserID, CashInOutViewModel vm, int pageIndex, int pageSize, string sortField, string sortOrder)
        {
            CashTransactionGridFilterResult result = new CashTransactionGridFilterResult();
            List<CashInOutViewModel> data = GetTransactionInfoCache(UserID);
            int FilterpageIndex = pageIndex - 1;
            if (pageIndex > 1)
            {
                FilterpageIndex = FilterpageIndex * pageSize;
            }
            data = FilterColumns(vm, data);
            data = PerformSorting(vm, data, sortField, sortOrder);
            result.FilterAmount = GetTransactionTotalAmount(data);
            List<CashInOutViewModel> resultData = data.Skip(FilterpageIndex).Take(pageSize).ToList();
            result.data = resultData;
            result.itemsCount = data.Count;

            return result;


        }

        private static List<CashInOutViewModel> FilterColumns(CashInOutViewModel vm, List<CashInOutViewModel> data)
        {
            List<CashInOutViewModel> resultdata = data;

            if (vm.ID != null)
            {
                data = data.Where(x => x.ID.ToString().ToLower().Contains(vm.ID.ToString().ToLower())).ToList();
            }

            if (vm.TrxID != null)
            {
                data = data.Where(x => x.TrxID.ToString().ToLower().Contains(vm.TrxID.ToString().ToLower())).ToList();
            }


            if (vm.ShopID != null)
            {
                data = data.Where(x => x.ShopID.ToString().ToLower().Contains(vm.ShopID.ToString().ToLower())).ToList();
            }

            if (vm.WalletType != null)
            {
                data = data.Where(x => x.WalletType.ToString().ToLower().Contains(vm.WalletType.ToString().ToLower())).ToList();
            }


            if (vm.WalletNumber != null)
            {
                data = data.Where(x => x.WalletNumber.ToString().ToLower().Contains(vm.WalletNumber.ToString().ToLower())).ToList();
            }

            if (vm.WalletAccountClass != null)
            {
                data = data.Where(x => x.WalletAccountClass.ToString().ToLower().Contains(vm.WalletAccountClass.ToString().ToLower())).ToList();
            }

            if (vm.MemberID != null)
            {
                data = data.Where(x => x.MemberID.ToString().ToLower().Contains(vm.MemberID.ToString().ToLower())).ToList();
            }

            if (vm.MemberPhoneNo != null)
            {
                data = data.Where(x => x.MemberPhoneNo.ToString().ToLower().Contains(vm.MemberPhoneNo.ToString().ToLower())).ToList();
            }

            if (vm.Amount != null)
            {
                data = data.Where(x => x.Amount.ToString().ToLower().Contains(vm.Amount.ToString().ToLower())).ToList();
            }

            if (vm.Operator != null)
            {
                data = data.Where(x => x.Operator.ToString().ToLower().Contains(vm.Operator.ToString().ToLower())).ToList();
            }


            if (vm.Status != null)
            {
                data = data.Where(x => x.Status.ToString().ToLower().Contains(vm.Status.ToString().ToLower())).ToList();
            }

            if (vm.Remark != null)
            {
                data = data.Where(x => x.Remark.ToString().ToLower().Contains(vm.Remark.ToString().ToLower())).ToList();
            }


            if (vm.CreatedTime != null)
            {
                data = data.Where(x => x.CreatedTime.ToString().ToLower().Contains(vm.CreatedTime.ToString().ToLower())).ToList();
            }






            return data;

        }

        private static List<CashInOutViewModel> PerformSorting(CashInOutViewModel vm, List<CashInOutViewModel> data, string sortField, string sortOrder)
        {
            List<CashInOutViewModel> resultdata = data;
            if (sortField == null)
            {
                return data;
            }

            if (sortField == "ID")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.ID).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.ID).ToList();
                }

            }


            if (sortField == "TrxID")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.TrxID).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.TrxID).ToList();
                }

            }

            if (sortField == "ShopName")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.ShopID).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.ShopID).ToList();
                }

            }


            if (sortField == "WalletType")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.WalletType).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.WalletType).ToList();
                }

            }

            if (sortField == "WalletNumber")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.WalletNumber).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.WalletNumber).ToList();
                }

            }

            if (sortField == "WalletAccountClass")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.WalletAccountClass).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.WalletAccountClass).ToList();
                }

            }

            if (sortField == "MemberID")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.MemberID).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.MemberID).ToList();
                }

            }

            if (sortField == "MemberPhoneNo")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.MemberPhoneNo).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.MemberPhoneNo).ToList();
                }

            }


            if (sortField == "Amount")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.Amount).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.Amount).ToList();
                }

            }


            if (sortField == "Operator")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.Operator).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.Operator).ToList();
                }

            }



            if (sortField == "Status")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.Status).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.Status).ToList();
                }

            }


            if (sortField == "Remark")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.Remark).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.Remark).ToList();
                }

            }

            if (sortField == "CreatedTime")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.CreatedTime).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.CreatedTime).ToList();
                }
            }



            return data;
        }

        public static string GetTransactionTotalAmount(List<CashInOutViewModel> data)
        {
            double totalAmount = 0;
            foreach (CashInOutViewModel item in data)
            {
                totalAmount += Helpers.DataReaderHelper.GetFormDouble(item.Amount.ToString().Replace(",", ""));
            }

            return String.Format("{0:n0}", totalAmount);
        }

        public static void AddRecord(string MerchantID, TransactionType TrxType)
        {
            CashInOutModel m = new CashInOutModel();
            var rng = new Random();
            string transType = (TrxType == TransactionType.CashIn ? "CIN" : "COUT");
            using (Repository<CashInOutModel> db = new Repository<CashInOutModel>())
            {
                m.ID = transType + String.Format("{0:0000000}", GetAutoNumber(MerchantID, TrxType));
                m.MerchantID = MerchantID;
                m.TransactionType = TrxType;
                m.TrxID = "TRX0000" + rng.Next(1, 100);
                m.ShopID = "8fa77e9fe3a248378a8c7561f662384d";
                m.WalletType = WalletType.BKash;
                m.WalletNumber = "W0009" + rng.Next(1, 100);
                m.WalletAccountClass = rng.Next(1, 10).ToString();
                m.MemberID = "TestUser001";
                m.MemberPhoneNo = "025748569" + rng.Next(1, 10);
                m.Amount = 500;
                m.Operator = "Admin";
                m.Status = CashInOutAction.Approved;
                m.Remark = "OK";
                m.CreatedTime = DateTime.Now;
                m.ApprovedTime = DateTime.Now;
                m.Action = CashInOutAction.Approved;
                db.Insert(m);
            }

        }


        public static void ApprovalAction(string UserName, string MerchantID,string ID,string TrxID, CashInOutAction Status)
        {
            CashInOutModel m = new CashInOutModel();
            // var rng = new Random();
            // string transType = (TrxType == TransactionType.CashIn ? "CIN" : "COUT");
            using (Repository<CashInOutModel> db = new Repository<CashInOutModel>())
            {
                m = db.Find(ID, MerchantID);
                if (m != null)
                {
                    m.TrxID = (TrxID != null && TrxID != "" ? TrxID : m.TrxID);
                    m.Status = Status;
                    m.Operator = UserName;
                    m.Remark = (Status == CashInOutAction.Approved ? "Manual Approved" : "Rejected");
                    m.ApprovedTime = DateTime.Now;
                    db.Update(m);

                }

            }

        }

        public static PaymentApiStatus updateCashInOut(string MerchantID, string ID, string TrxID,int FileNo, CashInOutAction Status)
        {
            CashInOutModel m = new CashInOutModel();
            PaymentApiStatus status = PaymentApiStatus.Waiting;
            double AccBalanceHist = 0;
            // var rng = new Random();
            // string transType = (TrxType == TransactionType.CashIn ? "CIN" : "COUT");
            using (Repository<CashInOutModel> db = new Repository<CashInOutModel>())
            {
                m = db.Find(ID, MerchantID);
                if (m != null)
                {
                    if(m.Status == CashInOutAction.New)
                    {
                        m.TrxID = (TrxID != null && TrxID != "" ? TrxID : m.TrxID);
                        m.Status = Status;
                        // m.Operator = UserName;
                        m.Remark = "Player Submitted";
                        m.ApprovedTime = DateTime.Now;
                        m.FileNo = FileNo;
                        db.Update(m);
                        string resultmsg = AutoMatching(MerchantID, m, ref AccBalanceHist);
                        if (resultmsg == AutoMachedMsg)
                        {
                            status = PaymentApiStatus.AutoMatched;
                        }
                    }
                    else
                    {
                        status = PaymentApiStatus.Invalid;
                    }
                 
                }

            }
            return status;

        }



        public static CashInOutApiModel GetCashInOutTransaction(string data)
        {
            CashInOutApiModel m = new CashInOutApiModel();
            // var rng = new Random();
            // string transType = (TrxType == TransactionType.CashIn ? "CIN" : "COUT");
            using (MGatewayDB db = new MGatewayDB())
            {
                string SQL = "Select * from CashInOutApiMapping t1 inner join CashInOut t2 on t1.MerchantID = t2.MerchantID and t1.TrxID = t2.ID where UniqueKey = @p0";
                m = db.Database.SqlQuery<CashInOutApiModel>(SQL, data).FirstOrDefault();
                return (m == null? new CashInOutApiModel() : m) ;

            }

           
        }






        public static void DeleteRecord(CashInOutViewModel vm, string UserName, string MerchantID)
        {
            CashInOutModel m = new CashInOutModel();

            using (Repository<CashInOutModel> db = new Repository<CashInOutModel>())
            {
                m = db.Find(vm.ID, MerchantID);
                if (m != null)
                {
                    InsertIntoCashInOutLog(vm.ID, UserName);
                    db.Delete(m);
                }

            }

        }

        private static void InsertIntoCashInOutLog(string ID, string UserName)
        {
            using (MGatewayDB db = new MGatewayDB())
            {
                string SQL = "Insert Into CashInOutLog Select *,'" + UserName + "' as LastMofified, @p1 as LastModifiedDateTime  from CashInOut where ID = @p0";

                db.Database.ExecuteSqlCommand(SQL, ID, DateTime.Now);
            }
        }

        public static AddManualRecordContainer AddRecordManual(string MerchantID, CashInOutSQLModel vm, bool isAPI)
        {
            AddManualRecordContainer result = new AddManualRecordContainer();
            string resultmsg = string.Empty;
            CashInOutModel m = new CashInOutModel();
            var rng = new Random();
            string transType = (vm.TransactionType == TransactionType.CashIn ? "CIN" : "COUT");
            using (Repository<CashInOutModel> db = new Repository<CashInOutModel>())
            {

                m.ID = transType + String.Format("{0:0000000}", GetAutoNumber(MerchantID, vm.TransactionType));
                m.MerchantID = MerchantID;
                m.TransactionType = vm.TransactionType;
                m.TrxID = (vm.TrxID == null ? string.Empty : vm.TrxID);
                m.TrxID = Helpers.DataReaderHelper.GetFormString(m.TrxID);
                ShopWalletsModel shopwallet = Shop.GetShopbyWalletNumber(vm.WalletNumber, vm.WalletType);

                m.WalletType = vm.WalletType;
                m.WalletNumber = (vm.TransactionType == TransactionType.CashOut ? vm.WalletNumber : string.Empty);
                m.WalletNumber = Helpers.DataReaderHelper.GetFormString(m.WalletNumber);

                m.ShopID = (shopwallet != null ? shopwallet.ShopID : string.Empty); //Key in shop ID?
                m.WalletAccountClass = (vm.WalletAccountClass == null ? string.Empty : vm.WalletAccountClass);
                m.MemberID = string.Empty; //Key in Member ID?
                m.MemberPhoneNo = (vm.TransactionType == TransactionType.CashIn ? vm.WalletNumber : vm.MemberPhoneNo);
                m.MemberPhoneNo = Helpers.DataReaderHelper.GetFormString(m.MemberPhoneNo);

                m.Amount = vm.Amount;

                m.Operator = MerchantID;
                if (isAPI)
                {
                    m.Status = CashInOutAction.New;
                }
                else
                {
                    m.Status = CashInOutAction.Pending;
                }
                
                m.Remark = string.Empty;
                m.CreatedTime = DateTime.Now;
                m.ApprovedTime = DateTime.Now;
                m.Action = CashInOutAction.Pending;

                double AccBalanceHist = 0;
                if (vm.TransactionType == TransactionType.CashOut)
                {
                    CashInOutModel checkTrxID = db.Where(x => x.TrxID == m.TrxID & x.MerchantID == MerchantID & x.Status == CashInOutAction.Approved).FirstOrDefault();
                    if (checkTrxID != null)
                    {
                        resultmsg = "Transaction fail. Transaction ID previously claimed.";
                        //db.Insert(m);
                    }
                    else
                    {
                        resultmsg = AutoMatching(MerchantID, m, ref AccBalanceHist);
                        db.Insert(m);

                        if (m.Status == CashInOutAction.Approved)
                        {
                           
                            RematchSMS(m, false);
                        }
                    }

                }
                else
                {
                    resultmsg = AutoMatching(MerchantID, m, ref AccBalanceHist);
                    db.Insert(m);
                }


            }

            if (vm.TransactionType == TransactionType.CashIn)
            {
                List<string> tokenList = Shop.GetShopToken().Where(x => x.MerchantID == vm.MerchantID).Select(x => x.Token).ToList();

                string Title = "New CashIn";
                string content = "New Transaction";
                _ = PushNotification.pushMessageAsync(Title, content, tokenList);
            }

            result.resultmsg = resultmsg;
            result.transactionID = m.ID;
            return result;

        }

        public static void CallNotifyApi(string ID, string MerchantID)
        {
            string Output = "";
            using (MGatewayDB db = new MGatewayDB())
            {
                string SQL = "Select * from CashInOutApiMapping where TrxID = @p0 and MerchantID = @p1";
                APIIntegration.Model.CashInOutApiMappingModel m = db.Database.SqlQuery<APIIntegration.Model.CashInOutApiMappingModel>(SQL, ID, MerchantID).FirstOrDefault();

                if(m != null)
                {
                    using (System.Net.WebClient client = new System.Net.WebClient())
                    {

                        client.Headers.Add("content-type", "application/json");
                        //client.Headers["Content-Type"] = "application/x-www-form-urlencoded";
                        //client.Headers["X-API-KEY"] = "ceMqIRYCpCjw1DpLdvBj3bEOw3Nkq6g8NjH4XWnGVjA";
                     //   client.Headers["X-API-KEY"] = URLModel.apiKey;

                       // client.Headers["X-API-CLIENT-SIGN"] = GETSingatureKey(GlobalProperties.SignatureKeyURL, Data, URLModel.RSAPrivateKey);

                        client.Encoding = Encoding.UTF8;
                        byte[] data = Encoding.Default.GetBytes("");

                        var response = client.UploadData(m.NotifyURL, "POST", data);

                        UTF8Encoding utf8 = new UTF8Encoding(true, true);
                        Output = utf8.GetString(response);

                     

                    }
                }
               
            }
        }

        public static AddManualRecordContainer AddRecordManual(string MerchantID, CashInOutSQLModel vm)
        {

            return AddRecordManual(MerchantID, vm, false);
        }

        private static string AutoMatching(string MerchantID, CashInOutModel m, ref double AccBalanceHist)
        {
            SMSRecordModel sms = new SMSRecordModel();
            GlobalSetingsViewModel gbl = GlobalSettings.GetSettings();

            if (m.TransactionType == TransactionType.CashIn)
            {
                // sms = db.Where(x => x.MerchantID == MerchantID && x.AccountNumber == m.MemberPhoneNo && x.Amount == m.Amount).FirstOrDefault();
                m.Status = CashInOutAction.New;
                return "Transaction successfully added.";
            }

            if (m.TransactionType == TransactionType.CashOut)
            {
                string SQL = "Select * from SMSRecord t1 left join ShopWallets t2 on t1.ShopID = t2.ShopID and t2.WalletType = @p0" + Environment.NewLine +
                    " where t1.TrxID = @p1 and t1.MerchantID = @p2 and t1.Amount = @p3 and t1.WalletType = @p0 and t1.AccountNumber = @p4 and t2.WalletNumber = @p5 and t1.SMSType = @p6";
                using (MGatewayDB db = new MGatewayDB())
                {
                    if (gbl.DepositAutoApprove == false)
                    {
                        return "Transaction successfully added. Pending for manual verification.";
                    }

                    sms = db.Database.SqlQuery<SMSRecordModel>(SQL, m.WalletType, m.TrxID, MerchantID, m.Amount, m.MemberPhoneNo, m.WalletNumber, m.TransactionType).FirstOrDefault();

                   
                    if (sms != null && CheckBalance(sms, ref AccBalanceHist))
                    {
                        m.ShopID = sms.ShopID;
                        m.Status = CashInOutAction.Approved;
                        m.Action = CashInOutAction.Approved;
                        m.ApprovedTime = DateTime.Now;
                        m.Remark = "Auto Matched";
                        //m.MemberPhoneNo = sms.AccountNumber;
                        ShopWalletsModel shopwallet = Shop.GetShopWallet(sms.ShopID, sms.WalletType);
                        if (shopwallet != null)
                        {
                            // m.WalletNumber = shopwallet.WalletNumber; //Get wallet Number from BO based on wallet type;
                            m.WalletAccountClass = shopwallet.AccountClass;
                        }
                        AccBalanceHist = m.Amount;
                        CallNotifyApi(m.ID, MerchantID);
                        return AutoMachedMsg;
                    }
                    else
                    {
                        return "Transaction successfully added. Invalid transaction.";
                    }

                }


                //using (Repository<SMSRecordModel> db = new Repository<SMSRecordModel>())
                //{
                //    //Receiver - agent
                //    //sender - player
                //    sms = db.Where(x => x.TrxID == m.TrxID && x.MerchantID == MerchantID && x.Amount == m.Amount && x.WalletType == m.WalletType && x.AccountNumber == m.MemberPhoneNo ).FirstOrDefault();




                //}
            }
            return "";







        }


        private static bool CheckBalance(SMSRecordModel vm, ref double AccBalanceHist)
        {
            SMSRecordModel smsHist = new SMSRecordModel();

            using (MGatewayDB db = new MGatewayDB())
            {
                //Get Last approved sms balance (t1.SMSDateTime <= @p3)
                string SQL = "Select * from SMSRecord t1 left join CashInOut t2 on t1.TrxID = t2.TrxID and t1.MerchantID=t2.MerchantID and t1.ShopID=t2.ShopID and t1.WalletType=t2.WalletType where t1.MerchantID = @p0 and t1.ShopID = @p1 and t2.Status=2 and t1.Sender=@p2 and t1.SMSDateTime <= @p3 order by t1.SmsDateTime desc ,t2.ApprovedTime desc";
                smsHist = db.Database.SqlQuery<SMSRecordModel>(SQL, vm.MerchantID, vm.ShopID, vm.Sender, vm.SmsDateTime).FirstOrDefault();

            }

            if (smsHist == null || smsHist.TrxID == null)
            {
                AccBalanceHist = 0;
                return false;
            }


            double checkBalance = GetBalance(vm);

            checkBalance = checkBalance - smsHist.Balance;
            if (checkBalance < 0)
            {
                checkBalance = checkBalance * -1;
            }

            if (checkBalance < 50)
            {
                AccBalanceHist = smsHist.Balance;
                return true;
            }

            AccBalanceHist = 0;
            return false;
        }

        private static double GetBalance(SMSRecordModel vm)
        {
            double resultBalance = 0;
            double commRate = GetCommRate(vm);
            if (vm.SMSType == TransactionType.CashIn)
            {
                resultBalance = vm.Balance + vm.Amount - (vm.Amount * commRate);

            }
            else if (vm.SMSType == TransactionType.CashOut)
            {
                resultBalance = vm.Balance - vm.Amount - (vm.Amount * commRate);

            }

            return resultBalance;
        }

        private static double GetCommRate(SMSRecordModel vm)
        {

            double commRate = 0;
            if (vm.WalletType == WalletType.BKash)
            {
                commRate = (vm.SMSType == TransactionType.CashIn) ? 0.0039 : 0.0041;

            }
            else if (vm.WalletType == WalletType.Nagad)
            {
                commRate = (vm.SMSType == TransactionType.CashIn) ? 0.0041 : 0.0041;
            }

            return commRate;
        }

        public static void RematchSMS(CashInOutModel m, bool checkPrevOnly)
        {
            using (MGatewayDB db = new MGatewayDB())
            {
                //string SQL = "Select t1.* from SMSRecord t1 left join CashInOut t2 on t1.TrxID = t2.TrxID and t1.MerchantID=t2.MerchantID and t1.ShopID=t2.ShopID and t1.WalletType=t2.WalletType where t2.ID=@p0 and t2.Status=2 ";
                //SMSRecordModel smsHist = db.Database.SqlQuery<SMSRecordModel>(SQL,m.ID).FirstOrDefault();

                //1. GET last matched sms
                string SQL = "Select t1.*,t3.ShopName from SMSRecord t1 inner join Shop t3 on t3.ShopID = t1.ShopID left join CashInOut t2 on t1.TrxID = t2.TrxID and t1.MerchantID=t2.MerchantID and t1.ShopID=t2.ShopID and t1.WalletType=t2.WalletType where t2.ID=@p0 and t2.Status=2 and t2.MerchantID = @p1 ";
                SMSRecordRematchVM smsHist = db.Database.SqlQuery<SMSRecordRematchVM>(SQL, m.ID, m.MerchantID).FirstOrDefault();
                if (smsHist != null)
                {
                    CheckPendingSMS(smsHist, checkPrevOnly);
                }


            }
        }
        private static void CheckPendingSMS(SMSRecordRematchVM smsHist, bool checkPrevOnly)
        {
            List<string> trxList = new List<string>();
            List<SMSMatchResult> smsNotMatch = new List<SMSMatchResult>();
            List<SMSRecordModel> smsList = new List<SMSRecordModel>();
            List<CashInOutModel> cashInOutList = new List<CashInOutModel>();
            //2. Get all not matched record where smsdatetime larger than last matched sms
        
            using (MGatewayDB db = new MGatewayDB())
            {
                //string SQL = "Select t1.* from SMSRecord t1 inner join CashInOut t2 on t1.TrxID = t2.TrxID and t1.MerchantID=t2.MerchantID and t1.ShopID=t2.ShopID and t1.WalletType=t2.WalletType where t2.Status=1 and t1.SMSDateTime>=@p0 and t1.WalletType=@p1 order by t1.TrxID desc";
                string SQL = "Select t1.* from SMSRecord t1 inner join CashInOut t2 on t1.TrxID = t2.TrxID and t1.MerchantID=t2.MerchantID and t1.ShopID=t2.ShopID and t1.WalletType=t2.WalletType where t2.Status=1 and t1.SMSDateTime>=@p0 and t1.WalletType=@p1 and t2.MerchantID = @p2";
                smsList = db.Database.SqlQuery<SMSRecordModel>(SQL, smsHist.SmsDateTime, smsHist.WalletType, smsHist.MerchantID).ToList();
             
                
                List<string> smsTrxID = smsList.Select(x => x.TrxID).ToList();
                SQL = "Select * from CashInOut where TrxID in ('" + string.Join("','", smsTrxID) + "') and Status in (1) order by CreatedTime";
                cashInOutList = db.Database.SqlQuery<CashInOutModel>(SQL).ToList();


                SQL = "Select * from CashInOut where TrxID in ('" + string.Join("','", smsTrxID) + "') and Status=2";
                List<CashInOutModel> transList = db.Database.SqlQuery<CashInOutModel>(SQL).ToList();
                trxList = transList.Select(x => x.TrxID).ToList();
            }


            foreach (SMSRecordModel sms in smsList)
            {
                if (trxList.Contains(sms.TrxID))
                {
                    continue;
                }
                else if (checkPrevOnly == true && sms.SMSDeliveryTime > smsHist.SMSDeliveryTime)
                {
                    continue;
                }

                double AccountBalanceHist = 0;
                CashInOutModel m = cashInOutList.Where(x => x.TrxID == sms.TrxID).FirstOrDefault();
                if (m == null || m.ID == null)
                {
                    continue;
                }
                AutoMatching(sms.MerchantID, m, ref AccountBalanceHist);//if matched will return last approved balance
                if (m.Status == CashInOutAction.Approved)
                {
                    UpdateCashInOut(m);
                }
                smsNotMatch.Add(new SMSMatchResult { ID = sms.ID, SmsDateTime = sms.SmsDateTime, isMatch = (AccountBalanceHist > 0), TrxID = sms.TrxID });

                if (AccountBalanceHist > 0)
                {
                    trxList.Add(sms.TrxID);
                    RematchRecursive(smsList, smsNotMatch, sms.SmsDateTime, smsHist.ShopName, sms.Balance, trxList, cashInOutList); //rematch previous not matched record
                }
            }
        }
        public static void RematchRecursive(List<SMSRecordModel> SMSList, List<SMSMatchResult> smsNotMatch, DateTime? smsDateTime, string ShopName, double AccountBalanceHist, List<string> trxList, List<CashInOutModel> cashInOutList)
        {
            List<SMSMatchResult> smsNotMatchTemp = smsNotMatch.Where(x => x.isMatch != true && x.SmsDateTime >= smsDateTime).ToList();

            foreach (SMSMatchResult s in smsNotMatchTemp)
            {
                if (trxList.Contains(s.TrxID))
                {
                    continue;
                }

                SMSRecordModel sms = SMSList.Where(x => x.ID == s.ID).FirstOrDefault();
                double checkBalance = GetBalance(sms) - AccountBalanceHist;
                if (checkBalance < 0)
                {
                    checkBalance = checkBalance * -1;
                }

                if (checkBalance < 50)
                {
                    CashInOutModel m = cashInOutList.Where(x => x.TrxID == sms.TrxID).FirstOrDefault();
                    if (m != null || m.ID != null)
                    {
                        AutoMatching(sms.MerchantID, m, ref AccountBalanceHist);
                        if (m.Status == CashInOutAction.Approved)
                        {
                            UpdateCashInOut(m);
                        }
                        if (AccountBalanceHist > 0)
                        {
                            smsNotMatch.Remove(s); //remove matched record and then check again.
                            trxList.Add(sms.TrxID);
                            RematchRecursive(SMSList, smsNotMatch, sms.SmsDateTime, ShopName, AccountBalanceHist, trxList, cashInOutList);
                        }
                    }
                  
                }
            }

        }

        private static void UpdateCashInOut(CashInOutModel m)
        {
            using (Repository<CashInOutModel> db = new Repository<CashInOutModel>())
            {
                db.Update(m);
            }
        }

        private static int GetAutoNumber(string MerchantID,TransactionType TrxType)
        {
            lock (LockAutoNumber)
            {
                string transType = (TrxType == TransactionType.CashIn ? "CIAutoNumber" : "COAutoNumber");
                int AutoNumber = 0;
                using (Repository<GlobalSetingsModel> db = new Repository<GlobalSetingsModel>())
                {
                    GlobalSetingsModel m = db.Find("SYS", MerchantID + transType);
                    if (m != null)
                    {
                        AutoNumber = Helpers.DataReaderHelper.GetFormInt(m.SysValue);
                        m.SysValue = (AutoNumber + 1).ToString();
                        db.Update(m);
                        return AutoNumber;
                    }

                }
                return -1;
            }


        }

        public static Dictionary<string, MerchantBalanceModel> GetMerchantBalance(string MerchantID, string _StartDate, string _EndDate)
        {
            Dictionary<string, MerchantBalanceModel> m = GetMerchantBalance(MerchantID, _StartDate, _EndDate, null);


            return m;
        }

        public static Dictionary<string, MerchantBalanceModel> GetMerchantBalance(string MerchantID, string _StartDate, string _EndDate, List<string> ShopList)
        {
            Dictionary<string, MerchantBalanceModel> m = new Dictionary<string, MerchantBalanceModel>();
            Dictionary<string, MerchantBalanceComputeModel> WalletComputeDict = new Dictionary<string, MerchantBalanceComputeModel>();
            DateTime StartDate = Helpers.DataReaderHelper.GetFormDate(_StartDate);
            DateTime EndDate = Helpers.DataReaderHelper.GetFormDate(_EndDate).AddHours(23).AddMinutes(59).AddSeconds(59);
            DateTime TodayDate = DateTime.Now.Date;
            DateTime YesterDay = TodayDate.AddDays(-1);
            TodayDate = TodayDate.AddHours(23).AddMinutes(59).AddSeconds(59);
            using (MGatewayDB db = new MGatewayDB())
            {
                string SQL = "Select t1.*,IFNULL(t2.ShopName,'') ShopName from CashInOut t1 " + Environment.NewLine +
                    "left join Shop t2 on t1.ShopID = t2.ShopID" + Environment.NewLine +
                    "where (ApprovedTime between @p0 and @p1 or ApprovedTime between @p2 and @p3 ) and  t1.MerchantID = @p4  and t1.Status = 2";

                List<CashInOutSQLModel> TransactionList = db.Database.SqlQuery<CashInOutSQLModel>(SQL, StartDate, EndDate, YesterDay, TodayDate, MerchantID).ToList();
               
                //double TotalCashIn = 0;
                //double TotalCashOut = 0;
                //double YesterdayCashOut = 0;
                //double YesterdayCashIn = 0;
                //double TodayCashOut = 0;
                //double TodayCashIn = 0;
                double AvailableBalance = 0;
                foreach (CashInOutSQLModel item in TransactionList)
                {
                    if (ShopList == null || ShopList.Contains(item.ShopID))
                    {
                        MerchantBalanceComputeModel Balance = new MerchantBalanceComputeModel();
                        if (WalletComputeDict.ContainsKey(item.WalletType.ToString()))
                        {
                            Balance = WalletComputeDict[item.WalletType.ToString()];
                        }
                        else
                        {
                            WalletComputeDict[item.WalletType.ToString()] = Balance;
                        }

                        if (item.ApprovedTime >= StartDate && item.ApprovedTime <= EndDate)
                        {
                            if (item.TransactionType == TransactionType.CashIn)
                            {
                                Balance.TotalCashIn += item.Amount;
                            }
                            if (item.TransactionType == TransactionType.CashOut)
                            {
                                Balance.TotalCashOut += item.Amount;
                            }
                        }

                        if (item.ApprovedTime >= YesterDay && item.ApprovedTime <= YesterDay.AddHours(23).AddMinutes(59).AddSeconds(59))
                        {
                            if (item.TransactionType == TransactionType.CashIn)
                            {
                                Balance.YesterdayCashIn += item.Amount;
                            }
                            if (item.TransactionType == TransactionType.CashOut)
                            {
                                Balance.YesterdayCashOut += item.Amount;
                            }
                        }

                        if (item.ApprovedTime >= TodayDate.Date && item.ApprovedTime <= TodayDate)
                        {
                            if (item.TransactionType == TransactionType.CashIn)
                            {
                                Balance.TodayCashIn += item.Amount;
                            }
                            if (item.TransactionType == TransactionType.CashOut)
                            {
                                Balance.TodayCashOut += item.Amount;
                            }
                        }
                    }
                    else
                    {
                        string test = "aa";
                    }
                   

                }
                foreach(KeyValuePair<string, MerchantBalanceComputeModel> item in WalletComputeDict)
                {
                    AvailableBalance = (item.Value.YesterdayCashOut - item.Value.YesterdayCashIn) + item.Value.TodayCashOut - item.Value.TodayCashIn;
                    MerchantBalanceModel walletbalance = new MerchantBalanceModel();
                    walletbalance.TotalCashIn = String.Format("{0:n0}", item.Value.TotalCashIn);
                    walletbalance.TotalCashOut = String.Format("{0:n0}", item.Value.TotalCashOut);
                    walletbalance.AvailableBalance = String.Format("{0:n0}", AvailableBalance);
                    m[item.Key] = walletbalance;
                }

                List<WalletType> walletList = new List<WalletType>() { WalletType.BKash, WalletType.Rocket, WalletType.Nagad };
                foreach(WalletType wallet in walletList)
                {
                    if (!m.ContainsKey(wallet.ToString()))
                    {
                        m[wallet.ToString()] = new MerchantBalanceModel() { AvailableBalance = "0", TotalCashIn = "0", TotalCashOut = "0" };
                    }
                }
               

             


                return m;
            }

        }
    }
}


