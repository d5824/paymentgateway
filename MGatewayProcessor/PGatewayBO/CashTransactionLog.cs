﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MGatewayBO.Model;
using Global.Model;
using MGatewayRepository;

using System.Collections.Concurrent;

namespace MGatewayProcessor
{
    public class CashTransactionLog
    {
        private static string LockAutoNumber = "LockAutoNumber";
        private static ConcurrentDictionary<string, List<CashInOutLogViewModel>> _CacheTransInfo = new ConcurrentDictionary<string, List<CashInOutLogViewModel>>();
        public static JsGridModel GetCashTransactionGrid(string UserRole, string MerchantID,  string UserID, string _StartDate, string _EndDate)
        {
            DateTime StartDate = Helpers.DataReaderHelper.GetFormDate(_StartDate);
            DateTime EndDate = Helpers.DataReaderHelper.GetFormDate(_EndDate);
            EndDate = EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);
            JsGridModel m = new JsGridModel();
            using (MGatewayDB db = new MGatewayDB())
            {
                string SQL = "Select t1.*,IFNULL(t2.ShopName,'') ShopName from CashInOutLog t1 " + Environment.NewLine +
                    "left join Shop t2 on t1.ShopID = t2.ShopID" + Environment.NewLine +
                    "where ApprovedTime between @p0 and @p1 and  t1.MerchantID = @p2 order by t1.ID desc";
                //if(UserID == "7c06b2db2f9e4dccad6d165444d555f0")
                //{
                //    SQL = "Select * from TrxInfo where CreatedTime >=  @p1 and CreatedTime <= @p2";
                //}
                List<CashInOutLogViewModel> TransactionList = ConvertToVm(db.Database.SqlQuery<CashInOutLogSQLModel>(SQL, StartDate, EndDate,  MerchantID).ToList());

                _CacheTransInfo[UserID] = TransactionList;
                m.data = TransactionList;
                m.AllowAdd = false;
                m.AllowDelete = false;
                m.AllowEdit = false;
                m.HasDetails = false;
                m.ServerSideLoading = true;
                m.KeyFieldName = "ID";
                HashSet<string> hideColumns = null;
             
                    hideColumns = new HashSet<string>() { "Action" };
             
                GridHelper.AddFields("CashInOutLog", m, hideColumns);


                m.DeleteURL.urlAction = new ControllerURL { action = "DeleteRecord", area = "", controller = "CashTransactionLog" };
                m.ViewURL.urlAction = new ControllerURL { action = "", area = "", controller = "CashTransactionLog" };
                m.LoadDataURL.urlAction = new ControllerURL { action = "LoadCashTransaction", area = "", controller = "CashTransactionLog" };
                m.ViewCallBack.Parentelem = "parentElement";
                m.ViewCallBack.childelem = "chilElement";
                m.ViewCallBack.childGridID = "TransactionDetails";


                //m.ViewCallBack.Parentelem = "parentElement";
                //m.ViewCallBack.childelem = "chilElement";
                //m.ViewCallBack.childGridID = "UserDetails";

                return m;
            }



        }

        public static List<CashInOutLogViewModel> ConvertToVm(List<CashInOutLogSQLModel> m)
        {

            List<CashInOutLogViewModel> vm = new List<CashInOutLogViewModel>();
            foreach (CashInOutLogSQLModel item in m)
            {
                CashInOutLogViewModel vmitem = new CashInOutLogViewModel();
                vmitem.ID = item.ID;
                vmitem.TrxID = Helpers.DataReaderHelper.GetFormString(item.TrxID).Replace("\t", "").Replace("\r", "");
                vmitem.TransactionType = item.TransactionType.ToString();
                vmitem.ShopID = item.ShopID;
                vmitem.MerchantID = item.MerchantID;
                vmitem.ShopName = item.ShopName;
                vmitem.WalletType = item.WalletType.ToString();
                vmitem.WalletNumber = item.WalletNumber.Replace("\t","").Replace("\r","");
                vmitem.WalletAccountClass = item.WalletAccountClass;
                vmitem.MemberID = item.MemberID;
                vmitem.MemberPhoneNo = Helpers.DataReaderHelper.GetFormString(item.MemberPhoneNo).Replace("\t", "").Replace("\r", "");
                vmitem.Amount = String.Format("{0:n0}", item.Amount);
                vmitem.Operator = item.Operator;
                vmitem.Status = item.Status.ToString();
                vmitem.Remark = item.Remark;               
                vmitem.CreatedTime = item.CreatedTime.ToString(Common.Common.gDisplayDateTimeFormat) + " (" + Helpers.DataReaderHelper.GetCompareCurrentDate(item.CreatedTime) + " ago" + ")";
                vmitem.ApprovedTime = item.ApprovedTime.ToString(Common.Common.gDisplayDateTimeFormat) + " (" + Helpers.DataReaderHelper.GetCompareCurrentDate(item.ApprovedTime) + " ago" + ")";
                vmitem.LastModified = item.LastModified;
                vmitem.LastModifiedDateTime = item.LastModifiedDateTime.ToString(Common.Common.gDisplayDateTimeFormat) ;
                vmitem.Action = item.Action.ToString();
                vm.Add(vmitem);


            }
            return vm;
        }


        public static List<CashInOutLogViewModel> GetTransactionInfoCache(string UserID)
        {
            if (_CacheTransInfo.ContainsKey(UserID))
            {
                return _CacheTransInfo[UserID];
            }
            else
            {
                return new List<CashInOutLogViewModel>();
            }
        }

        public static void UpdateTransactionInfoCache(string UserID, List<CashInOutLogViewModel> vm)
        {
            if (_CacheTransInfo.ContainsKey(UserID))
            {
                _CacheTransInfo[UserID] = vm;
            }

        }


        public static void RemoveTransactionFromCache(string UserID, CashInOutLogViewModel vm)
        {
            if (_CacheTransInfo.ContainsKey(UserID))
            {
                CashInOutLogViewModel removeItem = new CashInOutLogViewModel();
                foreach (CashInOutLogViewModel item in _CacheTransInfo[UserID])
                {
                    if (item.ID == vm.ID)
                    {
                        removeItem = item;
                    }

                }

                _CacheTransInfo[UserID].Remove(removeItem);


            }

        }



        public static CashTransactionGridLogFilterResult PerformFilter(string UserID, CashInOutLogViewModel vm, int pageIndex, int pageSize, string sortField, string sortOrder)
        {
            CashTransactionGridLogFilterResult result = new CashTransactionGridLogFilterResult();
            List<CashInOutLogViewModel> data = GetTransactionInfoCache(UserID);
            int FilterpageIndex = pageIndex - 1;
            if (pageIndex > 1)
            {
                FilterpageIndex = FilterpageIndex * pageSize;
            }
            data = FilterColumns(vm, data);
            data = PerformSorting(vm, data, sortField, sortOrder);
            result.FilterAmount = GetTransactionTotalAmount(data);
            List<CashInOutLogViewModel> resultData = data.Skip(FilterpageIndex).Take(pageSize).ToList();
            result.data = resultData;
            result.itemsCount = data.Count;

            return result;


        }

        private static List<CashInOutLogViewModel> FilterColumns(CashInOutLogViewModel vm, List<CashInOutLogViewModel> data)
        {
            List<CashInOutLogViewModel> resultdata = data;

            if (vm.ID != null)
            {
                data = data.Where(x => x.ID.ToString().ToLower().Contains(vm.ID.ToString().ToLower())).ToList();
            }


            if (vm.LastModified != null)
            {
                data = data.Where(x => x.LastModified.ToString().ToLower().Contains(vm.LastModified.ToString().ToLower())).ToList();
            }



            if (vm.TrxID != null)
            {
                data = data.Where(x => x.TrxID.ToString().ToLower().Contains(vm.TrxID.ToString().ToLower())).ToList();
            }


            if (vm.ShopID != null)
            {
                data = data.Where(x => x.ShopID.ToString().ToLower().Contains(vm.ShopID.ToString().ToLower())).ToList();
            }

            if (vm.WalletType != null)
            {
                data = data.Where(x => x.WalletType.ToString().ToLower().Contains(vm.WalletType.ToString().ToLower())).ToList();
            }


            if (vm.WalletNumber != null)
            {
                data = data.Where(x => x.WalletNumber.ToString().ToLower().Contains(vm.WalletNumber.ToString().ToLower())).ToList();
            }

            if (vm.WalletAccountClass != null)
            {
                data = data.Where(x => x.WalletAccountClass.ToString().ToLower().Contains(vm.WalletAccountClass.ToString().ToLower())).ToList();
            }

            if (vm.MemberID != null)
            {
                data = data.Where(x => x.MemberID.ToString().ToLower().Contains(vm.MemberID.ToString().ToLower())).ToList();
            }

            if (vm.MemberPhoneNo != null)
            {
                data = data.Where(x => x.MemberPhoneNo.ToString().ToLower().Contains(vm.MemberPhoneNo.ToString().ToLower())).ToList();
            }

            if (vm.Amount != null)
            {
                data = data.Where(x => x.Amount.ToString().ToLower().Contains(vm.Amount.ToString().ToLower())).ToList();
            }

            if (vm.Operator != null)
            {
                data = data.Where(x => x.Operator.ToString().ToLower().Contains(vm.Operator.ToString().ToLower())).ToList();
            }


            if (vm.Status != null)
            {
                data = data.Where(x => x.Status.ToString().ToLower().Contains(vm.Status.ToString().ToLower())).ToList();
            }

            if (vm.Remark != null)
            {
                data = data.Where(x => x.Remark.ToString().ToLower().Contains(vm.Remark.ToString().ToLower())).ToList();
            }


            if (vm.CreatedTime != null)
            {
                data = data.Where(x => x.CreatedTime.ToString().ToLower().Contains(vm.CreatedTime.ToString().ToLower())).ToList();
            }






            return data;

        }

        private static List<CashInOutLogViewModel> PerformSorting(CashInOutLogViewModel vm, List<CashInOutLogViewModel> data, string sortField, string sortOrder)
        {
            List<CashInOutLogViewModel> resultdata = data;
            if (sortField == null)
            {
                return data;
            }



            if (sortField == "ID")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.ID).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.ID).ToList();
                }

            }


            if (sortField == "LastModified")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.LastModified).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.LastModified).ToList();
                }

            }




            if (sortField == "TrxID")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.TrxID).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.TrxID).ToList();
                }

            }

            if (sortField == "ShopName")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.ShopID).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.ShopID).ToList();
                }

            }


            if (sortField == "WalletType")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.WalletType).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.WalletType).ToList();
                }

            }

            if (sortField == "WalletNumber")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.WalletNumber).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.WalletNumber).ToList();
                }

            }

            if (sortField == "WalletAccountClass")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.WalletAccountClass).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.WalletAccountClass).ToList();
                }

            }

            if (sortField == "MemberID")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.MemberID).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.MemberID).ToList();
                }

            }

            if (sortField == "MemberPhoneNo")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.MemberPhoneNo).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.MemberPhoneNo).ToList();
                }

            }


            if (sortField == "Amount")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.Amount).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.Amount).ToList();
                }

            }


            if (sortField == "Operator")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.Operator).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.Operator).ToList();
                }

            }



            if (sortField == "Status")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.Status).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.Status).ToList();
                }

            }


            if (sortField == "Remark")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.Remark).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.Remark).ToList();
                }

            }

            if (sortField == "CreatedTime")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.CreatedTime).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.CreatedTime).ToList();
                }
            }



            return data;
        }

        public static string GetTransactionTotalAmount(List<CashInOutLogViewModel> data)
        {
            double totalAmount = 0;
            foreach (CashInOutLogViewModel item in data)
            {
                totalAmount += Helpers.DataReaderHelper.GetFormDouble(item.Amount.ToString().Replace(",", ""));
            }

            return String.Format("{0:n0}", totalAmount);
        }

     
    }
}


