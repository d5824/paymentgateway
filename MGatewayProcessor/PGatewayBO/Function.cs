﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;
using System.ComponentModel;
using System.Reflection;
using System.Web;
namespace MGatewayProcessor
{


        public class Function
        {
           
            public static byte[] ExportToExcel(System.Data.DataTable dt)
            {
                ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.Commercial;
                if (dt != null)
                {
                    ExcelPackage ep = new ExcelPackage();
                    ep.Workbook.Worksheets.Add(dt.TableName);
                    ExcelWorksheet es = ep.Workbook.Worksheets[0];
                    es.Name = dt.TableName;
                    es.DefaultColWidth = 21.71;
                    WriteToCell(es, 1, 1, dt, true);
                    return ep.GetAsByteArray();
                }
                else
                {
                    return null;
                }
            }

            public static byte[] ExportToExcel(List<System.Data.DataTable> dt)
            {
                ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.Commercial;
                if (dt != null)
                {
                    int worksheetsCount = 0;
                    ExcelPackage ep = new ExcelPackage();
                    foreach (System.Data.DataTable item in dt)
                    {

                        ep.Workbook.Worksheets.Add(item.TableName);
                        ExcelWorksheet es = ep.Workbook.Worksheets[worksheetsCount];
                        es.Name = item.TableName;
                        es.DefaultColWidth = 21.71;
                        WriteToCell(es, 1, 1, item, true);
                        worksheetsCount++;
                    }

                    return ep.GetAsByteArray();
                }
                else
                {
                    return null;
                }
            }


            private static void WriteToCell(ExcelWorksheet ws, int RefRowIndex, int RefColindex, System.Data.DataTable dt, bool WriteHeader)
            {
                ws.Cells[RefRowIndex, RefColindex].LoadFromDataTable(dt, WriteHeader);
            }

            //    Private Shared Sub WriteToCell(ByVal ws As ExcelWorksheet, ByVal RefRowIndex As Integer, ByVal RefColIndex As Integer, ByVal Tbl As DataTable, ByVal WriteHeader As Boolean, Optional ByVal dictColumnFormat As Dictionary(Of String, String) = Nothing)
            //    ws.Cells(RefRowIndex, RefColIndex).LoadFromDataTable(Tbl, WriteHeader)

            //    If dictColumnFormat IsNot Nothing AndAlso dictColumnFormat.Count > 0 Then
            //        Dim colNum As Integer = 1
            //        For Each Column As DataColumn In Tbl.Columns
            //            If dictColumnFormat.ContainsKey(Column.ColumnName) Then
            //                ws.Column(colNum).Style.Numberformat.Format = dictColumnFormat(Column.ColumnName)
            //            End If

            //            colNum += 1
            //        Next
            //        'Auto fit column performance is bad with lot of data
            //        'ws.Cells(ws.Dimension.Address).AutoFitColumns()
            //    End If
            //End Sub


            //    Public Shared Function Export(ByVal Dt As DataTable, ByVal HasHeader As Boolean) As Byte()
            //    If Dt IsNot Nothing Then
            //        Dim ep As New ExcelPackage
            //        ep.Workbook.Worksheets.Add(Dt.TableName)
            //        Dim es As ExcelWorksheet = ep.Workbook.Worksheets(1)
            //        es.Name = Dt.TableName
            //        es.DefaultColWidth = 21.71

            //        WriteToCell(es, 1, 1, Dt, HasHeader)

            //        Return ep.GetAsByteArray
            //    Else
            //        Return Nothing
            //    End If
            //End Function

            public static byte[] ExportToExcelCollection<T>(IEnumerable<T> Collection)
            {
                byte[] response;
                ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.Commercial;
                using (var excelFile = new OfficeOpenXml.ExcelPackage())
                {
                    var worksheet = excelFile.Workbook.Worksheets.Add("Sheet1");
                    worksheet.Cells["A1"].LoadFromCollection(Collection: Collection, PrintHeaders: true);
                    response = excelFile.GetAsByteArray();
                }

                return response;
            }



        }
    }

