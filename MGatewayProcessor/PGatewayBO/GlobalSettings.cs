﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MGatewayBO.Model;
using Global.Model;
using MGatewayRepository;
namespace MGatewayProcessor
{
    public class GlobalSettings
    {
        public static GlobalSetingsViewModel GetSettings()
        {
            GlobalSetingsViewModel globalsetting = new GlobalSetingsViewModel();
            using (Repository<GlobalSetingsModel> db = new Repository<GlobalSetingsModel>())
            {
                List<GlobalSetingsModel> results = db.GetAll().ToList();

                foreach (GlobalSetingsModel item in results)
                {
                    switch (item.Syskey)
                    {
                        case "DepositAutoApprove":
                            globalsetting.DepositAutoApprove = Helpers.DataReaderHelper.GetFormBoolean(item.SysValue);
                            break;
                        case "DepositMatchFreq":
                            globalsetting.DepositMatchFreq = Helpers.DataReaderHelper.GetFormInt(item.SysValue);
                            break;
                        case "WithdrawalAutoApprove":
                            globalsetting.WithdrawalAutoApprove = Helpers.DataReaderHelper.GetFormBoolean(item.SysValue);
                            break;
                        case "WithdrawalMatchFreq":
                            globalsetting.WithdrawalMatchFreq = Helpers.DataReaderHelper.GetFormInt(item.SysValue);
                            break;
                        case "WalletMinList":
                            globalsetting.WalletMinList = Helpers.DataReaderHelper.GetFormInt(item.SysValue);
                            break;
                        case "WalletMinListApproved":
                            globalsetting.WalletMinListApproved = Helpers.DataReaderHelper.GetFormInt(item.SysValue);
                            break;
                        case "AppVersion":
                            globalsetting.AppVersion = Helpers.DataReaderHelper.GetFormString(item.SysValue);
                            break;
                        case "PingInterval":
                            globalsetting.PingInterval = Helpers.DataReaderHelper.GetFormInt(item.SysValue);
                            break;
                        case "ValidateSMSSender":
                            globalsetting.ValidateSMSSender = Helpers.DataReaderHelper.GetFormBoolean(item.SysValue);
                            break;
                        case "Recipient":
                            globalsetting.Recipient = Helpers.DataReaderHelper.GetFormString(item.SysValue);
                            break;
                        case "IdleInterval":
                            globalsetting.IdleInterval = Helpers.DataReaderHelper.GetFormInt(item.SysValue);
                            break;
                        case "SendNotification":
                            globalsetting.SendNotification = Helpers.DataReaderHelper.GetFormBoolean(item.SysValue);
                            break;
                    }
                }
            }
            return globalsetting;

        }

        public static void UpdateSettings(GlobalSetingsViewModel vm)
        {
            using (Repository<GlobalSetingsModel> db = new Repository<GlobalSetingsModel>())
            {
                List<GlobalSetingsModel> results = db.GetAll().ToList();

                foreach (GlobalSetingsModel item in results)
                {
                    switch (item.Syskey)
                    {
                        case "DepositAutoApprove":
                            item.SysValue = vm.DepositAutoApprove.ToString();

                            break;
                        case "DepositMatchFreq":
                            item.SysValue = vm.DepositMatchFreq.ToString();

                            break;
                        case "WithdrawalAutoApprove":
                            item.SysValue = vm.WithdrawalAutoApprove.ToString();

                            break;
                        case "WithdrawalMatchFreq":
                            item.SysValue = vm.WithdrawalMatchFreq.ToString();

                            break;
                        case "WalletMinList":
                            item.SysValue = vm.WalletMinList.ToString();

                            break;
                        case "WalletMinListApproved":
                            item.SysValue = vm.WalletMinListApproved.ToString();
                            break;
                        case "AppVersion":
                            item.SysValue = vm.AppVersion.ToString();
                            break;
                        case "PingInterval":
                            item.SysValue = vm.PingInterval.ToString();
                            break;
                        case "ValidateSMSSender":
                            item.SysValue = vm.ValidateSMSSender.ToString();
                            break;
                        case "Recipient":
                            item.SysValue = vm.Recipient.ToString();
                            break;
                        case "IdleInterval":
                            item.SysValue = vm.IdleInterval.ToString();
                            break;
                        case "SendNotification":
                            item.SysValue = vm.SendNotification.ToString();
                            break;
                    }
                    db.Update(item);

                }

            }
        }

        public static Dictionary<string, RejectRemarksModel> GetRejectRemarksDict()
        {
         
                Dictionary<string, RejectRemarksModel> RejectRemarkDict = new Dictionary<string, RejectRemarksModel>();

            List<RejectRemarksModel> RejectRemarkList = GetRejectRemarks();

                foreach (RejectRemarksModel item in RejectRemarkList)
                {

                    RejectRemarkDict[item.ID.ToString()] = item;

                }

                return RejectRemarkDict;


        }

        public static List<RejectRemarksModel> GetRejectRemarks()
        {
            using (MGatewayDB db = new MGatewayDB())
            {
                string SQL = " Select * from RejectRemarks ";
                List<RejectRemarksModel> m = db.Database.SqlQuery<RejectRemarksModel>(SQL).ToList();

                return m;


            }
        }


        public static void UpdateRejectRemark(List<RejectRemarksModel> RejectRemarksList)
        {
            if (RejectRemarksList != null)
            {
                using (Repository<RejectRemarksModel> db = new Repository<RejectRemarksModel>())
                {

                    foreach (RejectRemarksModel item in RejectRemarksList)
                    {
                        RejectRemarksModel m = db.Find(item.ID);
                        if (m != null)
                        {
                            m.Remark = item.Remark;
                            db.Update(m);
                        }
                        else
                        {
                            m = new RejectRemarksModel();
                            m.ID = 0;
                            m.Remark = item.Remark;
                            db.Insert(item);
                        }
                    }
                }
            }
        }

        public static void DeleteRejectRemark(string ID)
        {
            int RejectRemarkID = Helpers.DataReaderHelper.GetFormInt(ID);

            using (Repository<RejectRemarksModel> db = new Repository<RejectRemarksModel>())
            {
                RejectRemarksModel m = db.Find(RejectRemarkID);
                if (m != null)
                {
                    db.Delete(m);
                }

            }
        }

    }

}


