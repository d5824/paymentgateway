﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MGatewayBO.Model;
using Global.Model;
using MGatewayRepository;
using System.Security.Cryptography;
namespace MGatewayProcessor
{
    public class MerchantSetup
    {
        public static JsGridModel GetMerchantGrid()
        {
            JsGridModel m = new JsGridModel();
            using (Repository<MerchantDataModel> db = new Repository<MerchantDataModel>())
            {
                List<MerchantDataViewModel> GatewayList = ConvertToVm(db.GetAll().ToList());

                m.data = GatewayList;
                m.AllowAdd = false;
                m.AllowDelete = true;
                m.AllowEdit = false;
                m.HasDetails = true;
                m.KeyFieldName = "MerchantID";
                GridHelper.AddFields("MerchantData", m);

                m.DeleteURL.urlAction = new ControllerURL { action = "DeleteMerchant", area = "", controller = "MerchantSetup" };
                m.ViewURL.urlAction = new ControllerURL { action = "ViewMerchantDetails", area = "", controller = "MerchantSetup" };
                m.ViewCallBack.Parentelem = "parentElement";
                m.ViewCallBack.childelem = "chilElement";
                m.ViewCallBack.childGridID = "MerchantDetails";
                return m;
            }



        }

        private static List<MerchantDataViewModel> ConvertToVm(List<MerchantDataModel> m)
        {
            List<MerchantDataViewModel> vm = new List<MerchantDataViewModel>();
            foreach(MerchantDataModel item in m)
            {
                MerchantDataViewModel vmitem = new MerchantDataViewModel();
                vmitem.merchantID = item.merchantID;
                vmitem.merchantName = item.merchantName;
                vmitem.merchantStatus = item.merchantStatus.ToString();
                vmitem.APIKEY = item.APIKEY;
                vm.Add(vmitem);
              
            }
            return vm;
        }
      
        public static MerchantDataModel UpdateMerchantDetails(MerchantDataModel vm)
        {
            using (Repository<MerchantDataModel> db = new Repository<MerchantDataModel>())
            {


                MerchantDataModel m = db.Find(vm.merchantID);
                if (m != null)
                {

                    CopyVmToM(m, vm);
                    db.Update(m);
                }
                else
                {
                    m = new MerchantDataModel();
                    m.merchantID = vm.merchantID;
                    m.APIKEY = GetAPIKEY();

                    CopyVmToM(m, vm);
                    InsertAutoNumber(vm.merchantID);
                    db.Insert(m);
                }

                return m;
            }
          
        }

        private static void InsertAutoNumber(string MerchantID)
        {
            string CashInName = "CIAutoNumber";
            string CashOutName = "COAutoNumber";
            using (Repository<GlobalSetingsModel> db = new Repository<GlobalSetingsModel>())
            {
                GlobalSetingsModel m = db.Find("SYS", MerchantID + CashInName);
                if (m == null)
                {
                    m = new GlobalSetingsModel();
                    m.SysValue = "0";
                    m.SysType = "SYS";
                    m.Syskey = MerchantID + CashInName;

                    db.Insert(m);
                    
                }

                m = db.Find("SYS", MerchantID + CashOutName);

                if (m == null)
                {
                    m = new GlobalSetingsModel();
                    m.SysValue = "0";
                    m.SysType = "SYS";
                    m.Syskey = MerchantID + CashOutName;

                    db.Insert(m);

                }


            }
        }


        private static string GetAPIKEY()
        {

             var provider = new RNGCryptoServiceProvider();
            var bytes = new byte[32];
            provider.GetBytes(bytes);

            string ApiKey= Convert.ToBase64String(bytes)
                .Replace("/", "")
                .Replace("+", "")
                .Replace("=", "");

            return ApiKey;


            //var bytes = RandomNumberGenerator.GetBytes(64);

            //string ApiKey = "CT-" + Convert.ToBase64String(bytes)
            //    .Replace("/", "")
            //    .Replace("+", "")
            //    .Replace("=", "")
            //    .Substring(0, 33);

            //return ApiKey;
        }

        private static void CopyVmToM(MerchantDataModel m, MerchantDataModel vm)
        {
            m.merchantName = vm.merchantName;
            m.merchantStatus = vm.merchantStatus;
            m.APIKEY = (string.IsNullOrEmpty(vm.APIKEY) ? GetAPIKEY() : vm.APIKEY) ;
    

            //m.success_url = vm.success_url;
            //m.cancel_url = vm.cancel_url;
            //m.fail_url = vm.fail_url;

        }

        public static void DeleteMerchant(MerchantDataModel vm)
        {
            using (Repository<MerchantDataModel> db = new Repository<MerchantDataModel>())
            {
                MerchantDataModel m = db.Find(vm.merchantID);
                db.Delete(m);
            }

        }

        public static string AddGatewayMerchant(string MerchantID, string GatewayID)
        {
            using (Repository<GatewayMerchantModel> db = new Repository<GatewayMerchantModel>())
            {


                GatewayMerchantModel m = db.Where(x => x.GatewayID == GatewayID && x.merchantID == MerchantID).FirstOrDefault();
                if (m == null)
                {
                    m = new GatewayMerchantModel();
                    m.ServiceID = Guid.NewGuid().ToString("N");
                    m.merchantID = MerchantID;
                    m.GatewayID = GatewayID;
                    db.Insert(m);
                    return m.ServiceID;
                }
                else
                {
                    return string.Empty;

                   
                }


            }
           

            //;
            
            
        }

        public static bool DeleteGatewayMerchant(string ServiceID)
        {
            using (Repository<GatewayMerchantModel> db = new Repository<GatewayMerchantModel>())
            {


                GatewayMerchantModel m = db.Find(ServiceID);
                db.Delete(m);


            }
            return true;
        }

        public static List<MerchantDataModel> GetAllMerchantByUserID(string UserID)
        {
            string SQL = "Select t1.* from MerchantData t1" + Environment.NewLine;
            if (UserID != "7c06b2db2f9e4dccad6d165444d555f0") //Admin UserID
            {
                SQL += " inner join UserLogin t2 on t1.MerchantID = t2.MerchantID and t2.UserID = @p0";
            }
            List<MerchantDataModel> MerchantList = MySQLDB.GetValueModel<MerchantDataModel>(SQL, UserID).ToList();

            return MerchantList;

        }
    }

}
