﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MGatewayBO.Model;
using Global.Model;
using MGatewayRepository;
namespace MGatewayProcessor
{
    public class MobileApi
    {
        public static ApiResultModel Login(LoginAPIModel m)
        {

            //update last login time
            ApiResultModel result = new ApiResultModel() { status = "200" };
            string EncryptedPassword = Encryptor.Encryptor.GetHash(m.ShopName.ToLower() + "LDKEY" + m.Password + "ldkey" + m.ShopName.ToUpper());

            using (Repository<ShopModel> db = new Repository<ShopModel>())
            {

                ShopModel Shop = db.Where(x => x.ShopName == m.ShopName && x.MerchantID == m.MerchantID).FirstOrDefault();
                GetGeneralApiResult(m, result);
                if (result.status == "500")
                {
                    return result;
                }
                if (Shop != null)
                {

                    if (Shop.Password == EncryptedPassword)
                    {
                        UpdateShopLogin(Shop, m.Token);
                        SetApiResult(result, "200", "Ok");

                        //GetGeneralApiResult(m, result);
                        if (result.status == "200")
                        {
                            List<ShopWalletsViewModel> shopwallets = MGatewayProcessor.Shop.GetShopWallets(Shop.ShopID, Shop.MerchantID);
                            result.data.ShopWallets = shopwallets;
                        }

                    }
                    else
                    {
                        SetApiResult(result, "500", "Incorrect Username/Password");

                    }
                    return result;
                }
                else
                {
                    SetApiResult(result, "500", "Shop not found");
                }
                return result;

            }

            //ShopModel Shop = MySQLDB.GetValueModel<ShopModel>("Select * from Shop where ShopName = @p0", m.ShopName).FirstOrDefault();

        }

        private static void UpdateShopLogin(ShopModel m, string Token)
        {
            using (Repository<ShopModel> db = new Repository<ShopModel>())
            {

                ShopModel Login = db.Find(m.ShopID);
                if (Login != null)
                {
                    Login.LastLoginDate = DateTime.Now;
                    Login.LastActiveTime = DateTime.Now;
                    if (!string.IsNullOrEmpty(Token))
                    {
                        Login.Token = Token;
                    }
                   
                    db.Update(Login);
                }

            }
        }



        public static ApiResultModel WithdrawalRequest(WithdrawalRequestAPIModel vm)
        {
            ApiResultModel result = new ApiResultModel();
            using (Repository<CashInOutModel> db = new Repository<CashInOutModel>())
            {
                using (MGatewayDB db2 = new MGatewayDB())
                {
                    string SQL = "Select * from Shop where ShopName = @p0 and MerchantID = @p1";
                    ShopModel Shopm = db2.Database.SqlQuery<ShopModel>(SQL, vm.ShopName, vm.MerchantID).FirstOrDefault();


                    CashInOutModel m = db.Find(vm.WithdrawalID, vm.MerchantID);
                    if (m != null && Shopm != null)
                    {
                        if (m.Status != CashInOutAction.New)
                        {
                            SetApiResult(result, "200", "The transaction is not available.");
                        }
                        else
                        {
                            GetGeneralApiResult(vm, result);
                            if (result.status == "200")
                            {
                                m.Status = CashInOutAction.Pending;
                                m.ShopID = Shopm.ShopID;
                                ShopWalletsModel shopwallets = Shop.GetShopWallet(Shopm.ShopID, m.WalletType);
                                if (shopwallets != null)
                                {
                                    m.WalletNumber = shopwallets.WalletNumber;
                                    m.WalletAccountClass = shopwallets.AccountClass;
                                }

                                db.Update(m);
                                result.data.TransactionList = GetTransaction(vm, TransactionType.CashIn);
                            }

                            //result = GetWithdrawalTransaction(vm);
                            //if (result.status == "200")
                            //{

                            //}
                        }



                    }
                    else
                    {
                        SetApiResult(result, "200", "Invalid Withdrawal ID");
                    }

                    if (Shopm == null)
                    {
                        SetApiResult(result, "500", "Shop not found");

                    }

                }
            }
            return result;
        }

        public static ApiResultModel ManualDeposit(ManualDepositAPIModel vm)
        {
            ApiResultModel result = new ApiResultModel();
            using (Repository<CashInOutModel> db = new Repository<CashInOutModel>())
            {
                using (MGatewayDB db2 = new MGatewayDB())
                {
                    string SQL = "Select ShopID from Shop where ShopName = @p0 and MerchantID = @p1";
                    string ShopID = db2.Database.SqlQuery<string>(SQL, vm.ShopName, vm.MerchantID).FirstOrDefault();


                    CashInOutModel m = db.Find(vm.DepositID, vm.MerchantID);
                    if (m != null && ShopID != null)
                    {
                        if (m.Status != CashInOutAction.Pending)
                        {
                            SetApiResult(result, "200", "The transaction is not available.");
                        }
                        else
                        {
                            GetGeneralApiResult(vm, result);
                            if (result.status == "200")
                            {
                                m.Status = (vm.Status == "1" ? CashInOutAction.Approved : CashInOutAction.Rejected);
                                m.Operator = vm.ShopName;
                                m.Remark = (vm.Status == "2" ? vm.RejectRemark : ""); 
                                m.ShopID = ShopID;
                                m.ApprovedTime = DateTime.Now;
                                db.Update(m);
                                result.data.TransactionList = GetTransaction(vm, TransactionType.CashOut);

                                if (m.Status == CashInOutAction.Approved)
                                {
                                    RematchSMS(m, false); // Manual Approval, required to rematch other pending transaction.
                                }
                              
                            }

                            //result = GetWithdrawalTransaction(vm);
                            //if (result.status == "200")
                            //{

                            //}
                        }



                    }
                    else
                    {
                        SetApiResult(result, "200", "Invalid Deposit ID");
                    }

                    if (ShopID == null)
                    {
                        SetApiResult(result, "500", "Shop not found");

                    }

                }
            }
            return result;
        }

        public static List<CashInOutViewModel> GetTransaction(GeneralAPIModel m, TransactionType trxType)
        {
            // List<CashInOutViewModel> withdrawlList = new List<CashInOutViewModel>();
            //ApiResultModel result = new ApiResultModel();
            ShopModel shop = Shop.GetShopByShopName(m.ShopName);
            List<CashInOutViewModel> TransactionList = new List<CashInOutViewModel>();
            if (shop != null)
            {

                using (MGatewayDB db = new MGatewayDB())
                {
                    string SQL = "Select t1.*,IFNULL(t2.ShopName,'') ShopName from CashInOut t1  " + Environment.NewLine +
                        "left join Shop t2 on t1.ShopID = t2.ShopID" + Environment.NewLine +
                        "where t1.TransactionType = @p0  and t1.MerchantID = @p1 and Status in (1,0) ";

                    if (trxType == TransactionType.CashOut)
                    {
                        SQL += " and t1.ShopID = @p2";
                    }
                    //if(UserID == "7c06b2db2f9e4dccad6d165444d555f0")
                    //{
                    //    SQL = "Select * from TrxInfo where CreatedTime >=  @p1 and CreatedTime <= @p2";
                    //}
                    TransactionList = FilterRecordWithdrawal(db.Database.SqlQuery<CashInOutSQLModel>(SQL, trxType, m.MerchantID, shop.ShopID).ToList(), m.ShopName, false);

                    // GetGeneralApiResult(m, result);
                    // if (result.status == "200")
                    //{
                    // withdrawlList = TransactionList;
                    // }



                }
            }
            return TransactionList;


        }

        public static List<CashInOutViewModel> GetHistoryTransaction(HistoryAPIModel m)
        {
            ShopModel shop = Shop.GetShopByShopName(m.ShopName);
            List<CashInOutViewModel> TransactionList = new List<CashInOutViewModel>();
            if (shop != null)
            {
                Helpers.DateRange DateRange = Helpers.DataReaderHelper.GetFormDateRange(m.DateRange);
                using (MGatewayDB db = new MGatewayDB())
                {

                    TransactionType transtype = (m.TransactionType == "CashIn" ? TransactionType.CashIn : TransactionType.CashOut);
                    string transtypeSQL = (m.TransactionType == "All" ? "t1.TransactionType in (1,2)" : "t1.TransactionType = @p0 ");
                    string SQL = "Select t1.*,IFNULL(t2.ShopName,'') ShopName from CashInOut t1  " + Environment.NewLine +
                     "left join Shop t2 on t1.ShopID = t2.ShopID" + Environment.NewLine +
                     "where  " + transtypeSQL + " and t1.MerchantID = @p1 and Status not in (1,0) and t1.ApprovedTime between @p2 and @p3 and t1.ShopID = @p4";

                    TransactionList = FilterRecordWithdrawal(db.Database.SqlQuery<CashInOutSQLModel>(SQL, transtype, m.MerchantID, DateRange.StartDate, DateRange.EndDate, shop.ShopID).ToList(), m.ShopName, true);


                }
            }
            return TransactionList;


        }

        public static List<RejectRemarksModel> GetRejectRemarks()
        {
           
            return GlobalSettings.GetRejectRemarks();


        }



        private static List<CashInOutViewModel> FilterRecordWithdrawal(List<CashInOutSQLModel> m, string ShopName, bool isHistory)
        {
            bool proceed = false;
            List<CashInOutViewModel> vm = new List<CashInOutViewModel>();
            foreach (CashInOutSQLModel item in m)
            {
                proceed = false;
                if (isHistory)
                {
                    if (item.ShopName.ToLower() == ShopName.ToLower())
                    {
                        proceed = true;
                    }
                }
                else
                {
                    if (item.ShopName == "" || item.ShopName.ToLower() == ShopName.ToLower())
                    {
                        proceed = true;
                    }
                }

                if (proceed)
                {
                    CashInOutViewModel vmitem = new CashInOutViewModel();
                    vmitem.ID = item.ID;
                    vmitem.TrxID = item.TrxID;
                    vmitem.TransactionType = item.TransactionType.ToString();
                    vmitem.ShopID = item.ShopID;
                    vmitem.MerchantID = item.MerchantID;
                    vmitem.ShopName = item.ShopName;
                    vmitem.WalletType = item.WalletType.ToString();
                    vmitem.WalletNumber = item.WalletNumber;
                    vmitem.WalletAccountClass = item.WalletAccountClass;
                    vmitem.MemberID = item.MemberID;
                    vmitem.MemberPhoneNo = item.MemberPhoneNo;
                    vmitem.Amount = String.Format("{0:n0}", item.Amount);
                    vmitem.Operator = item.Operator;
                    vmitem.Status = item.Status.ToString();
                    vmitem.Remark = item.Remark;
                    vmitem.CreatedTime = item.CreatedTime.ToString(Common.Common.gDisplayDateTimeFormat);
                    vmitem.ApprovedTime = item.ApprovedTime.ToString(Common.Common.gDisplayDateTimeFormat);
                    vmitem.Action = item.Action.ToString();
                    vm.Add(vmitem);
                }



            }
            return vm;
        }


        public static string PingServer(GeneralAPIModel m)
        {
            return "";
        }

        public static void GetGeneralApiResult(GeneralAPIModel m, ApiResultModel result)
        {

            SetApiResult(result, "200", "OK");
            GlobalSetingsViewModel gbl = GlobalSettings.GetSettings();
            result.data.AppVersion = gbl.AppVersion;
            result.data.WalletMinList = gbl.WalletMinList;
            result.data.PingInterval = gbl.PingInterval;
            result.data.ValidateSMSSender = gbl.ValidateSMSSender;
            CheckShop(m, result);
            if (gbl.AppVersion != m.Version)
            {
                SetApiResult(result, "500", "outdated app version.");
            }

        }

        private static void CheckShop(GeneralAPIModel m, ApiResultModel result)
        {
            using (MGatewayDB db2 = new MGatewayDB())
            {
                DateTime currentDate = DateTime.Now;
                string SQL = "Select * from Shop where ShopName = @p0 and MerchantID = @p1";
                ShopModel Shop = db2.Database.SqlQuery<ShopModel>(SQL, m.ShopName, m.MerchantID).FirstOrDefault();

                if (Shop != null)
                {
                    if (Shop.ShopStatus != ShopStatus.Active)
                    {
                        SetApiResult(result, "500", "Access Denied");
                    }
                    Shop.Contact = (Shop.Contact == null ? "0" : Shop.Contact);
                    m.Telno = (m.Telno == null ? "0" : m.Telno);
                    //int DbPhoneNo = Helpers.DataReaderHelper.GetFormInt(System.Text.RegularExpressions.Regex.Match(Shop.Contact, @"\d+").Value);
                    // int PhoneNo = Helpers.DataReaderHelper.GetFormInt(System.Text.RegularExpressions.Regex.Match(m.Telno, @"\d+").Value);
                    if (Shop.Contact != m.Telno)
                    {
                        SetApiResult(result, "500", "Invalid Phone Number.");
                    }
                }
                else
                {
                    SetApiResult(result, "500", "Shop not found.");
                }

                if (result.status == "200")
                {
                    SQL = "Update Shop set LastActiveTime = @p1 where ShopName = @p0 and MerchantID = @p2";
                    db2.Database.ExecuteSqlCommand(SQL, m.ShopName, currentDate, m.MerchantID);
                }


            }
        }



        private static void SetApiResult(ApiResultModel result, string Status, string Message)
        {
            result.status = Status;
            if (Status == "200")
            {
                result.message = Message;
                result.errorMessage = "";
            }
            else
            {
                result.message = "";
                result.errorMessage = Message;
            }


        }

        public static void GenerateSMSDateTime()
        {
          
            using (Repository<SMSRecordModel> db = new Repository<SMSRecordModel>())
            {
                List<SMSRecordModel> mlist = db.Where(x=> x.SmsDateTime == null).ToList();
                foreach (SMSRecordModel m in mlist)
                {
                    DateTime sDate = DateTime.MinValue;
                    if (m.WalletType == WalletType.BKash)
                    {
                        m.SmsDateTime = GetBkashContent(m);
                    }
                    else if (m.WalletType == WalletType.Nagad)
                    {
                        m.SmsDateTime = GetNagadContent(m);
                    }

                    if (m.SmsDateTime != DateTime.MinValue)
                    {
                        db.Update(m);
                    }
           
                }
            }
        }



        private static double GetCommRate(SMSRecordModel vm)
        {

            double commRate = 0;
            if (vm.WalletType == WalletType.BKash)
            {
                commRate = (vm.SMSType == TransactionType.CashIn) ? 0.0039 : 0.0041;

            }
            else if (vm.WalletType == WalletType.Nagad)
            {
                commRate = (vm.SMSType == TransactionType.CashIn) ? 0.0041 : 0.0041;
            }

            return commRate;
        }
        private static double GetBalance(SMSRecordModel vm)
        {
            double resultBalance = 0;
            double commRate = GetCommRate(vm);
            if (vm.SMSType == TransactionType.CashIn)
            {
                resultBalance = vm.Balance + vm.Amount - (vm.Amount * commRate);
             
            }
            else if (vm.SMSType == TransactionType.CashOut)
            {
                resultBalance = vm.Balance - vm.Amount - (vm.Amount * commRate);
     
            }

            return resultBalance;
        }
        private static bool CheckBalance(SMSRecordModel vm, ref double AccBalanceHist)
        {
            SMSRecordModel smsHist = new SMSRecordModel();

            using (MGatewayDB db = new MGatewayDB())
            {
                //Get Last approved sms balance (t1.SMSDateTime <= @p3)
                string SQL = "Select * from SMSRecord t1 left join CashInOut t2 on t1.TrxID = t2.TrxID and t1.MerchantID=t2.MerchantID and t1.ShopID=t2.ShopID and t1.WalletType=t2.WalletType where t1.MerchantID = @p0 and t1.ShopID = @p1 and t2.Status=2 and t1.Sender=@p2 and t1.SMSDateTime <= @p3 order by t1.SmsDateTime desc ,t2.ApprovedTime desc";
                smsHist = db.Database.SqlQuery<SMSRecordModel>(SQL, vm.MerchantID, vm.ShopID, vm.Sender, vm.SmsDateTime).FirstOrDefault();
            }

            if (smsHist == null || smsHist.TrxID == null)
            {
                AccBalanceHist = 0;
                return false;
            }

          
            double checkBalance = GetBalance(vm);
           
            checkBalance = checkBalance - smsHist.Balance;
            if (checkBalance < 0)
            {
                checkBalance = checkBalance * -1;
            }

            if (checkBalance < 50)
            {
                AccBalanceHist = smsHist.Balance;
                return true;
            }

            AccBalanceHist = 0;
            return false;
        }
        private static void CheckPendingSMS(SMSRecordRematchVM smsHist, bool checkPrevOnly)
        {
            List<string> trxList = new List<string>();
            List<SMSMatchResult> smsNotMatch = new List<SMSMatchResult>();
            List<SMSRecordModel> smsList = new List<SMSRecordModel>();
   
            using (MGatewayDB db = new MGatewayDB())
            {
                //2. Get all not matched record where smsdatetime larger than last matched sms
                string SQL = "Select t1.* from SMSRecord t1 inner join CashInOut t2 on t1.TrxID = t2.TrxID and t1.MerchantID=t2.MerchantID and t1.ShopID=t2.ShopID and t1.WalletType=t2.WalletType where t2.Status=1 and t1.SMSDateTime>=@p0 and t1.WalletType=@p1 and t2.MerchantID = @p2";
                smsList = db.Database.SqlQuery<SMSRecordModel>(SQL, smsHist.SmsDateTime, smsHist.WalletType, smsHist.MerchantID).ToList();
                //smsList = db.Database.SqlQuery<SMSRecordModel>(SQL, smsHist.SmsDateTime, smsHist.WalletType).OrderByDescending(x => x.TrxID).ToList(); //testing code

                List<string> smsTrxID = smsList.Select(x => x.TrxID).ToList();
                SQL = "Select * from CashInOut where TrxID in ('" + string.Join("','",smsTrxID) + "') and Status=2";
                List<CashInOutModel> transList = db.Database.SqlQuery<CashInOutModel>(SQL).ToList();
                trxList = transList.Select(x => x.TrxID).ToList();
            }


            foreach (SMSRecordModel sms in smsList)
            {
                if (trxList.Contains(sms.TrxID))
                {
                    continue;
                }
                else if (checkPrevOnly == true && sms.SMSDeliveryTime > smsHist.SMSDeliveryTime)
                {
                    continue;
                }
               
                double AccountBalanceHist = AutoMatching(sms, smsHist.ShopName, 0);//if matched will return last approved balance
                smsNotMatch.Add(new SMSMatchResult { ID = sms.ID, SmsDateTime = sms.SmsDateTime, isMatch = (AccountBalanceHist > 0), TrxID = sms.TrxID });

                if (AccountBalanceHist > 0)
                {
                    trxList.Add(sms.TrxID);
                    RematchRecursive(smsList, smsNotMatch, sms.SmsDateTime, smsHist.ShopName, sms.Balance,trxList); //rematch previous not matched record
                }
            }
        }
        public static void RematchSMS(CashInOutModel m, bool checkPrevOnly)
        {
            using (MGatewayDB db = new MGatewayDB())
            {
                //string SQL = "Select t1.* from SMSRecord t1 left join CashInOut t2 on t1.TrxID = t2.TrxID and t1.MerchantID=t2.MerchantID and t1.ShopID=t2.ShopID and t1.WalletType=t2.WalletType where t2.ID=@p0 and t2.Status=2 ";
                //SMSRecordModel smsHist = db.Database.SqlQuery<SMSRecordModel>(SQL,m.ID).FirstOrDefault();

                //1. GET last matched sms
                string SQL = "Select t1.*,t3.ShopName from SMSRecord t1 inner join Shop t3 on t3.ShopID = t1.ShopID left join CashInOut t2 on t1.TrxID = t2.TrxID and t1.MerchantID=t2.MerchantID and t1.ShopID=t2.ShopID and t1.WalletType=t2.WalletType where t2.ID=@p0 and t2.Status=2  and t2.MerchantID = @p1";
                SMSRecordRematchVM smsHist = db.Database.SqlQuery<SMSRecordRematchVM>(SQL, m.ID, m.MerchantID).FirstOrDefault();
                if (smsHist != null)
                {
                    CheckPendingSMS(smsHist, checkPrevOnly);
                }
          

            }
        }
        public static void RematchSMS(List<SMSRecordModel> SMSList, bool checkPrevOnly)
        {
            using (MGatewayDB db = new MGatewayDB())
            {
                foreach (SMSRecordModel s in SMSList)
                {
                    //1. GET last matched sms
                    string SQL = "Select t1.*,t3.ShopName from SMSRecord t1 inner join Shop t3 on t3.ShopID = t1.ShopID left join CashInOut t2 on t1.TrxID = t2.TrxID and t1.MerchantID=t2.MerchantID and t1.ShopID=t2.ShopID and t1.WalletType=t2.WalletType where t1.ID=@p0 and t2.Status=2 ";
                    SMSRecordRematchVM smsHist = db.Database.SqlQuery<SMSRecordRematchVM>(SQL, s.ID).FirstOrDefault();

                    if (smsHist != null)
                    {
                        CheckPendingSMS(smsHist, checkPrevOnly);
                    }
                }
            }
        }
        public static void RematchRecursive(List<SMSRecordModel> SMSList, List<SMSMatchResult> smsNotMatch, DateTime? smsDateTime , string ShopName, double AccountBalanceHist, List<string> trxList)
        {
            List<SMSMatchResult> smsNotMatchTemp = smsNotMatch.Where(x => x.isMatch != true && x.SmsDateTime >= smsDateTime).ToList();

            foreach (SMSMatchResult s in smsNotMatchTemp)
            {
                if (trxList.Contains(s.TrxID))
                {
                    continue;
                }

                SMSRecordModel sms = SMSList.Where(x => x.ID == s.ID).FirstOrDefault();
                double checkBalance = GetBalance(sms) - AccountBalanceHist;
                if (checkBalance < 0)
                {
                    checkBalance = checkBalance * -1;
                }

                if (checkBalance < 50)
                {
                    AccountBalanceHist = AutoMatching(sms, ShopName, 0);
                    if (AccountBalanceHist > 0)
                    {
                        smsNotMatch.Remove(s); //remove matched record and then check again.
                        trxList.Add(sms.TrxID);
                        RematchRecursive(SMSList, smsNotMatch, sms.SmsDateTime, ShopName, AccountBalanceHist, trxList);
                    }
                }
            }
         
        }


        public static void ReceiveMessage(ReceiveMessageAPIModel m, List<MessageContainer> messageContainer)
        {
            //GenerateSMSDateTime();
            List<SMSRecordModel> SMSList = new List<SMSRecordModel>();
            List<SMSRecordDetailsModel> SMSDetailsList = new List<SMSRecordDetailsModel>();
            List<UnKnowSMSModel> unknowSMSList = new List<UnKnowSMSModel>();
            ShopModel shop = Shop.GetShopByShopName(m.ShopName);
            Dictionary<string, WalletType> WalletSenderDict = GetWalletSenderDict();
            if (shop != null)
            {
                Dictionary<string, SMSRecordModel> smsDict = GetAllSMS(m.MerchantID, shop.ShopID);
                foreach (MessageContainer item in messageContainer)
                {
                    //SMSRecordModel testing = new SMSRecordModel();
                    //using (Repository<SMSRecordModel> db = new Repository<SMSRecordModel>())
                    //{
                    //    testing = db.Find("897b37ed729b46f88211a95e83bfb904");
                    //}
                    SMSRecordModel sms = new SMSRecordModel();
                    SMSRecordDetailsModel smsDetails = new SMSRecordDetailsModel();
                    sms.SMSType = (item.content.ToLower().Contains("cash in") ? TransactionType.CashIn : TransactionType.CashOut);
                    sms.ID = Guid.NewGuid().ToString("N");
                    sms.MerchantID = m.MerchantID;
                    sms.ShopID = shop.ShopID;
                    sms.Sender = item.Sender;

                    smsDetails.MessageID = sms.ID;
                    //smsDetails.RawMessage = (m.GetMsg == null ? "" : m.GetMsg);
                    //smsDetails.RawMessage += "service center:" + item.ServiceCenter.ToString();
                    string serviceCenterNo = (item.ServiceCenter == null ? "" : item.ServiceCenter.ToString());
                    smsDetails.RawMessage = "service center:" + serviceCenterNo;
                   
                    SMSDetailsList.Add(smsDetails);
                    //item.content = testing.RawMessage;

                    //sms.Sender = testing.Sender;
                    //item.content = testing.RawMessage;
                    //sms.SMSType = (item.content.ToLower().Contains("cash in") ? TransactionType.CashIn : TransactionType.CashOut);

                    if (WalletSenderDict.ContainsKey(item.Sender) && serviceCenterNo.Contains("+88"))
                    {
                        sms.WalletType = WalletSenderDict[item.Sender];
                        if (GetSmsDetails(sms, item, smsDict))
                        {
                            AutoMatching(sms, m.ShopName,0);
                            //Auto Matching start here
                            SMSList.Add(sms);
                        }
                        else
                        {
                            unknowSMSList.Add(PrepareUnknowSMS(shop.ShopID, m, item, sms.ID));
                        }
                    }
                    else
                    {
                        unknowSMSList.Add(PrepareUnknowSMS(shop.ShopID, m, item, sms.ID));
                    }



                }

                if (SMSList.Count > 0)
                {
                    using (Repository<SMSRecordModel> db = new Repository<SMSRecordModel>())
                    {
                        db.BulkInsert(SMSList);
                        RematchSMS(SMSList, true);
                    }
                }

                if (unknowSMSList.Count > 0)
                {
                    using (Repository<UnKnowSMSModel> db = new Repository<UnKnowSMSModel>())
                    {
                        db.BulkInsert(unknowSMSList);
                    }
                }

                if (SMSDetailsList.Count > 0)
                {
                    using (Repository<SMSRecordDetailsModel> db = new Repository<SMSRecordDetailsModel>())
                    {
                        db.BulkInsert(SMSDetailsList);
                    }
                }

            }
        }

        private static double AutoMatching(SMSRecordModel vm, string ShopName, double AccBalanceHist)
        {
            double GETHistBalance = 0;
            GlobalSetingsViewModel gbl = GlobalSettings.GetSettings();

            using (Repository<CashInOutModel> db = new Repository<CashInOutModel>())
            {

                if (gbl.WithdrawalAutoApprove)
                {
                    if (vm.SMSType == TransactionType.CashIn)
                    {
                        //need to insert transaction ID based on sms record

                        //sender amount
                        //receiver number - player
                        CashInOutModel m = db.Where(x => x.ShopID == vm.ShopID && x.MerchantID.ToString().ToLower() == vm.MerchantID.ToString().ToLower() && x.WalletType == vm.WalletType && x.TransactionType == vm.SMSType && x.Status == CashInOutAction.Pending && x.Amount == vm.Amount && x.MemberPhoneNo == vm.AccountNumber).FirstOrDefault();
                        if (m != null)
                        {

                            m.TrxID = vm.TrxID;
                            m.Status = CashInOutAction.Approved;
                            m.Operator = ShopName;
                            m.Remark = "Auto Matched";
                            m.ApprovedTime = DateTime.Now;
                            db.Update(m);
                            CashTransaction.CallNotifyApi(vm.TrxID, vm.MerchantID);
                            GETHistBalance = AccBalanceHist;

                        }
                        else
                        {
                            string SQL = "Insert into UnknowSMSRecord (MerchantID,ShopID,SMSDeliveryTime,CreatedTime,RawMessage,Sender) values (@p0, @p1, @p2,@p3,@p4,@p5)";

                            using (MGatewayDB db2 = new MGatewayDB())
                            {
                                try
                                {

                                    db2.Database.ExecuteSqlCommand(SQL, vm.MerchantID, vm.ShopID, DateTime.Now, DateTime.Now, vm.WalletType + "|" + vm.SMSType + "|" + vm.Amount + "|" + vm.AccountNumber, vm.Sender);
                                    // return true;

                                }
                                catch (Exception ex)
                                {
                                    // db2.Database.ExecuteSqlCommand(SQL, Action, ex.Message + ex.StackTrace, DateTime.Now);
                                    throw;
                                }
                            }
                        }

                    }
                }
               

                if (gbl.DepositAutoApprove)
                {
                    if (vm.SMSType == TransactionType.CashOut)
                    {
                        CashInOutModel m = db.Where(x => x.MerchantID == vm.MerchantID && x.TransactionType == vm.SMSType && x.Status == CashInOutAction.Pending && x.TrxID == vm.TrxID && x.Amount == vm.Amount && x.MemberPhoneNo == vm.AccountNumber).FirstOrDefault();
                        if (m != null)
                        {
                            
                            if (CheckBalance(vm, ref AccBalanceHist))
                            {
                                m.ShopID = vm.ShopID;
                                m.Status = CashInOutAction.Approved;
                                m.Action = CashInOutAction.Approved;
                                m.Operator = ShopName;
                                m.Remark = "Auto Matched";
                                m.ApprovedTime = DateTime.Now;
                                ShopWalletsModel shopwallet = Shop.GetShopWallet(vm.ShopID, vm.WalletType);
                                if (shopwallet != null)
                                {
                                    //m.WalletNumber = shopwallet.WalletNumber; //Get wallet Number from BO based on wallet type;
                                    m.WalletAccountClass = shopwallet.AccountClass;
                                }
                                db.Update(m);
                                CashTransaction.CallNotifyApi(vm.TrxID, vm.MerchantID);
                                GETHistBalance = AccBalanceHist;
                                // RematchSMS(m, ShopName, true); //to handle sms not in order.
                            }
                                
                        }
                    }
                }

                return GETHistBalance;


            }
        }
        public static UnKnowSMSModel PrepareUnknowSMS(string ShopID, ReceiveMessageAPIModel m, MessageContainer item, string MessageID)
        {
            UnKnowSMSModel unknowm = new UnKnowSMSModel();

            unknowm.MerchantID = m.MerchantID;
            unknowm.ShopID = ShopID;
            unknowm.SMSDeliveryTime = Helpers.DataReaderHelper.GetFormDateTime(item.MsgDateTime);
            unknowm.CreatedTime = DateTime.Now;
            unknowm.RawMessage = item.content + "SC:" + item.ServiceCenter;
            unknowm.Sender = item.Sender;
            unknowm.MessageID = MessageID;
            return unknowm;
        }
        private static Dictionary<string, SMSRecordModel> GetAllSMS(string MerchantID, string ShopID)
        {

            using (MGatewayDB db = new MGatewayDB())
            {

                Dictionary<string, SMSRecordModel> smsDict = new Dictionary<string, SMSRecordModel>();
                DateTime CheckDateTime = DateTime.Now.AddDays(-14);
                // string SQL = "Select * from SMSRecord t1 left join CashInOut t2 on t1.TrxID = t2.TrxID where t1.MerchantID = @p0 and t1.ShopID = @p1 and t2.Status = 2";
                string SQL = "Select * from SMSRecord where MerchantID = @p0 and ShopID = @p1 and SMSDeliveryTime >= @p2";

                List<SMSRecordModel> smsList = db.Database.SqlQuery<SMSRecordModel>(SQL, MerchantID, ShopID, CheckDateTime).ToList();

                foreach (SMSRecordModel item in smsList)
                {
                    smsDict[item.TrxID] = item;
                }
                return smsDict;
            }
        }
        private static bool GetSmsDetails(SMSRecordModel sms, MessageContainer item, Dictionary<string, SMSRecordModel> smsDict)
        {
            string TrxID = string.Empty;
            GetDefaultContent(sms, item);
            if (sms.WalletType == WalletType.BKash)
            {
                TrxID = GetBkashContent(sms, item);
            }

            if (sms.WalletType == WalletType.Nagad)
            {
                TrxID = GetNagadContent(sms, item);
                if(TrxID == "")
                {
                    return false;
                }
            }

            if (sms.WalletType == WalletType.Rocket)
            {
                TrxID = GetRocketContent(sms, item);
            }

            if (TrxID == string.Empty || !smsDict.ContainsKey(TrxID))
            {
                return true;
            }
            return false;

        }

        public static DateTime GetBkashContent(SMSRecordModel sms)
        {
            string[] smsArray = sms.RawMessage.Split(' ');
            DateTime setDateTime = DateTime.MinValue;
            if (smsArray.Length > 17)
            {
                    string getDate = smsArray[16] + " " + smsArray[17] + ":00";
                    setDateTime = Helpers.DataReaderHelper.GetFormDateTime(getDate);
               
            }

            return setDateTime;
        }

        public static DateTime GetNagadContent(SMSRecordModel sms)
        {
            DateTime setDateTime = DateTime.MinValue;
            string[] smsArray = sms.RawMessage.Replace("\n", " ").Split(' ');
            if (sms.RawMessage.Contains("\r") == false)
            {
                if (smsArray.Length > 17)
                {
                    string getDate = smsArray[16] + " " + smsArray[17] + ":00";
                    setDateTime = Helpers.DataReaderHelper.GetFormDateTime(getDate);

                }
              
            }
            return setDateTime;
        }


        private static string GetBkashContent(SMSRecordModel sms, MessageContainer item)
        {
            string trxID = string.Empty;
            string[] smsArray = item.content.Split(' ');
            if (smsArray.Length > 15)
            {
                sms.Amount = Helpers.DataReaderHelper.GetFormDouble(smsArray[3]);
                sms.AccountNumber = Helpers.DataReaderHelper.GetFormString(smsArray[5]);
                sms.TrxID = Helpers.DataReaderHelper.GetFormString(smsArray[14]);
                sms.SMSDeliveryTime = Helpers.DataReaderHelper.GetFormDateTime(item.MsgDateTime);
                sms.CreatedTime = DateTime.Now;
                sms.RawMessage = item.content;
                sms.Balance = Helpers.DataReaderHelper.GetFormDouble(smsArray[12].TrimEnd('.'));
                string getDate = smsArray[16] + " " + smsArray[17] + ":00";
                sms.SmsDateTime = Helpers.DataReaderHelper.GetFormDateTime(getDate);

                return sms.TrxID;
            }
            return trxID;

        }

        private static string GetNagadContent(SMSRecordModel sms, MessageContainer item)
        {
            string trxID = string.Empty;
            string[] smsArray = item.content.Replace("\n", " ").Split(' ');
            if(item.content.Contains("\r") == false)
            {
                if (smsArray.Length > 10)
                {
                    sms.Amount = Helpers.DataReaderHelper.GetFormDouble(smsArray[5]);
                    sms.AccountNumber = Helpers.DataReaderHelper.GetFormString(smsArray[7]);
                    sms.TrxID = Helpers.DataReaderHelper.GetFormString(smsArray[9]);
                    sms.SMSDeliveryTime = Helpers.DataReaderHelper.GetFormDateTime(item.MsgDateTime);
                    sms.CreatedTime = DateTime.Now;
                    sms.RawMessage = item.content;
                    sms.Balance = Helpers.DataReaderHelper.GetFormDouble(smsArray[15]);
                    double Comm = Helpers.DataReaderHelper.GetFormDouble(smsArray[12]);
                    string getDate = smsArray[16] + " " + smsArray[17] + ":00";
                    sms.SmsDateTime = Helpers.DataReaderHelper.GetFormDateTime(getDate);
                   
                    sms.SmsDateTime = Helpers.DataReaderHelper.GetFormDateTime(getDate);
                    if (Comm != Math.Round(sms.Amount * 0.0041, 2))
                    {
                        return "";
                    }

                    if(sms.Balance == 0)
                    {
                        return "";
                    }
                 
                    return sms.TrxID;
                }
                return trxID;
            }
            return "";
          

        }

        private static string GetRocketContent(SMSRecordModel sms, MessageContainer item)
        {
            string trxID = string.Empty;
            string[] smsArray = item.content.Split(' ');
            if (smsArray.Length > 10)
            {
                sms.Amount = Helpers.DataReaderHelper.GetFormDouble(smsArray[5].ToString().Replace("Tk", ""));
                sms.AccountNumber = Helpers.DataReaderHelper.GetFormString(smsArray[4]);
                sms.TrxID = Helpers.DataReaderHelper.GetFormString(smsArray[10]);
                sms.SMSDeliveryTime = Helpers.DataReaderHelper.GetFormDateTime(item.MsgDateTime);
                sms.CreatedTime = DateTime.Now;
                sms.RawMessage = item.content;
                sms.Balance = Helpers.DataReaderHelper.GetFormDouble(smsArray[9].Replace("Tk", "").Replace(".TxnId:", ""));
                return sms.TrxID;
            }
            return trxID;

        }


        private static void GetDefaultContent(SMSRecordModel sms, MessageContainer item)
        {
            sms.Amount = 0;
            sms.AccountNumber = "A12346";
            sms.TrxID = "B123456";
            sms.SMSDeliveryTime = DateTime.Now;
            sms.CreatedTime = DateTime.Now;
            sms.RawMessage = item.content;
        }


        private static Dictionary<string, WalletType> GetWalletSenderDict()
        {

            using (MGatewayDB db = new MGatewayDB())
            {
                Dictionary<string, WalletType> SenderWalletDict = new Dictionary<string, WalletType>();
                string SQL = " Select * from ShopWalletsSender ";
                List<ShopWalletsSenderModel> ShopWalletSenderList = db.Database.SqlQuery<ShopWalletsSenderModel>(SQL).ToList();

                foreach (ShopWalletsSenderModel item in ShopWalletSenderList)
                {
                    foreach (string sender in item.Sender.Split(';'))
                    {
                        SenderWalletDict[sender] = item.WalletType;
                    }
                }

                return SenderWalletDict;


            }

        }

        public static bool SaveTriggerAPILog(string Action, string JsonString)
        {

            string SQL = "Insert into API_TriggerLog (Action,jsonString,CreatedDateTime) values (@p0, @p1, @p2)";

            using (MGatewayDB db = new MGatewayDB())
            {
                try
                {

                    db.Database.ExecuteSqlCommand(SQL, Action, JsonString, DateTime.Now);
                    return true;

                }
                catch (Exception ex)
                {
                    db.Database.ExecuteSqlCommand(SQL, Action, ex.Message + ex.StackTrace, DateTime.Now);
                    throw;
                }
            }

        }

        public static void UpdateActiveShopStatus(PingAPIModel m)
        {
            using (MGatewayDB db2 = new MGatewayDB())
            {
                ActiveStatus shopactivestatus;
                string SQL = "";
                if (m.appStatus.ToString().ToLower() == "pause")
                {
                    shopactivestatus = ActiveStatus.LastSeen;
                }
                else
                {
                    shopactivestatus = ActiveStatus.Online;
                }
                SQL = "Update Shop set ActiveStatus = @p1 where ShopName = @p0 and MerchantID = @p2";
                db2.Database.ExecuteSqlCommand(SQL, m.ShopName, shopactivestatus, m.MerchantID);

            }
        }



    }

}
