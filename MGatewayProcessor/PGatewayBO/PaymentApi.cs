﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MGatewayBO.Model;
using Global.Model;
using MGatewayRepository;
using APIIntegration.Model;
namespace MGatewayProcessor
{
    public class PaymentApi
    {
        public static DepositAPIModel ConvertToDepositApi(DepositSandBoxModel vm)
        {
            DepositAPIModel m = new DepositAPIModel();
            m.merchantDepositId = vm.MerchantTransactionID;
            m.AccountNumber = vm.SenderNumber;
            m.Amount = vm.Amount;
            m.PaymentGateway = vm.PaymentMethod;

            return m;
        }

        public static WithdrawalAPIModel ConvertToWithdrawalApi(DepositSandBoxModel vm)
        {
            WithdrawalAPIModel m = new WithdrawalAPIModel();
            m.merchantWithdrawalId = vm.MerchantTransactionID;
            m.AccountNumber = vm.ReceiverNumber;
            m.Amount = vm.Amount;
            m.PaymentGateway = vm.PaymentMethod;

            return m;
        }

        public static string InsertToApiMapping(string MerchantID, string ID, string MerchantTransactionID, string NotifyURL, string RedirectURL)
        {
            using (Repository<CashInOutApiMappingModel> db = new Repository<CashInOutApiMappingModel>())
            {


                CashInOutApiMappingModel m = new CashInOutApiMappingModel();
                m.UniqueKey = Guid.NewGuid().ToString("N");
                m.MerchantID = MerchantID;
                m.MerchantTrxID = MerchantTransactionID;
                m.TrxID = ID;
                m.CreatedDateTime = DateTime.Now;
                m.NotifyURL = NotifyURL;
                m.RedirectURL = NotifyURL;
                db.Insert(m);

                return m.UniqueKey;
            }



        }





    }

}


