﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MGatewayBO.Model;
using Global.Model;
using MGatewayRepository;
namespace MGatewayProcessor
{
    public class PaymentGatewaySetup
    {
        public static JsGridModel GetPaymentGatewayGrid()
        {
            JsGridModel m = new JsGridModel();
            using (Repository<GatewayDataModel> db = new Repository<GatewayDataModel>())
            {
                List<GatewayDataModel> GatewayList = db.GetAll().ToList();

                m.data = GatewayList;
                m.AllowAdd = false;
                m.AllowDelete = true;
                m.AllowEdit = false;
                m.HasDetails = true;
                m.KeyFieldName = "GatewayID";
                GridHelper.AddFields("GatewayData", m);

                m.DeleteURL.urlAction = new ControllerURL { action = "DeleteGateway", area = "", controller = "PaymentGatewaySetup" };
                m.ViewURL.urlAction = new ControllerURL { action = "ViewGatewayDetails", area = "", controller = "PaymentGatewaySetup" };
                m.ViewCallBack.Parentelem = "parentElement";
                m.ViewCallBack.childelem = "chilElement";
                m.ViewCallBack.childGridID = "PaymentGatewayDetails";
                return m;
            }



        }


        public static bool UpdateGatewayDetails(GatewayDataModel vm)
        {
            using (Repository<GatewayDataModel> db = new Repository<GatewayDataModel>())
            {


                GatewayDataModel m = db.Find(vm.GatewayID);
                if (m != null)
                {

                    CopyVmToM(m, vm);
                    db.Update(m);
                }
                else
                {
                    m = new GatewayDataModel();
                    m.GatewayID = vm.GatewayID;
                    CopyVmToM(m, vm);

                    db.Insert(m);
                }


            }
            return true;
        }


        private static void CopyVmToM(GatewayDataModel m, GatewayDataModel vm)
        {
            m.GatewayKey = vm.GatewayKey;
            m.GatewayName = vm.GatewayName;
            m.GatewayEmail = vm.GatewayEmail;

        }

        public static void DeleteGateway(GatewayDataModel vm)
        {
            using (Repository<GatewayDataModel> db = new Repository<GatewayDataModel>())
            {
                GatewayDataModel m = db.Find(vm.GatewayID);
                db.Delete(m);
            }

        }


        public static List<GatewayDataModel> GetAllPaymentGateway()
        {
            using (Repository<GatewayDataModel> db = new Repository<GatewayDataModel>())
            {
                List<GatewayDataModel> GatewayList = db.GetAll().ToList();

                return GatewayList;
            }
        }

   

    }

}
