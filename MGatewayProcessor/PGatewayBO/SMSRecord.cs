﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MGatewayBO.Model;
using Global.Model;
using MGatewayRepository;

using System.Collections.Concurrent;

namespace MGatewayProcessor
{
    public class SMSRecord
    {
        private static ConcurrentDictionary<string, List<SMSRecordViewModel>> _CacheTransInfo = new ConcurrentDictionary<string, List<SMSRecordViewModel>>();
        public static JsGridModel GetSMSRecordGrid(string UserID, string MerchantID, string _StartDate, string _EndDate)
        {
            DateTime StartDate = Helpers.DataReaderHelper.GetFormDate(_StartDate);
            DateTime EndDate = Helpers.DataReaderHelper.GetFormDate(_EndDate);
            EndDate = EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);
            JsGridModel m = new JsGridModel();
            using (MGatewayDB db = new MGatewayDB())
            {
                string SQL = "Select IF(t3.ID is null,'Not Match','Matched') Matched,t1.*,t2.ShopName from SMSRecord t1 inner join Shop t2 on t1.ShopID = t2.ShopID " + Environment.NewLine +
                    "left join CashInOut t3 on t3.TrxID = t1.TrxID and t3.Status = 2" + Environment.NewLine + 
                    "where t1.CreatedTime between @p0 and @p1 and t1.MerchantID = @p2 order by t1.CreatedTime desc";
                //if(UserID == "7c06b2db2f9e4dccad6d165444d555f0")
                //{
                //    SQL = "Select * from TrxInfo where CreatedTime >=  @p1 and CreatedTime <= @p2";
                //}
                List<SMSRecordViewModel> TransactionList = ConvertToVm(db.Database.SqlQuery<SMSRecordSQLModel>(SQL, StartDate, EndDate,MerchantID ).ToList());

                _CacheTransInfo[UserID] = TransactionList;
                m.data = TransactionList;
                m.AllowAdd = false;
                m.AllowDelete = false;
                m.AllowEdit = false;
                m.HasDetails = false;
                m.ServerSideLoading = true;
                m.KeyFieldName = "ID";
                GridHelper.AddFields("SMSRecord", m, new HashSet<string> { "ID", "MerchantID","ShopID" });


                m.DeleteURL.urlAction = new ControllerURL { action = "", area = "", controller = "SMSRecord" };
                m.ViewURL.urlAction = new ControllerURL { action = "", area = "", controller = "SMSRecord" };
                m.LoadDataURL.urlAction = new ControllerURL { action = "LoadSMSRecord", area = "", controller = "SMSRecord" };
                m.ViewCallBack.Parentelem = "parentElement";
                m.ViewCallBack.childelem = "chilElement";
                m.ViewCallBack.childGridID = "TransactionDetails";
                return m;
            }



        }

        private static List<SMSRecordViewModel> ConvertToVm(List<SMSRecordSQLModel> m)
        {
            //Dictionary<string, MerchantDataModel> MerchantDataDict = new Dictionary<string, MerchantDataModel>();
            //Dictionary<string, GatewayDataModel> GatewayDataDict = new Dictionary<string, GatewayDataModel>();
            //using (MGatewayDB db = new MGatewayDB())
            //{
            //    MerchantDataDict = db.Database.SqlQuery<MerchantDataModel>("Select * from MerchantData").ToDictionary(x => x.merchantID, x => x);
            //    GatewayDataDict = db.Database.SqlQuery<GatewayDataModel>("Select * from GatewayData ").ToDictionary(x => x.GatewayID, x => x);
            //}
            List<SMSRecordViewModel> vm = new List<SMSRecordViewModel>();
            foreach (SMSRecordSQLModel item in m)
            {
                SMSRecordViewModel vmitem = new SMSRecordViewModel();
                vmitem.ID = item.ID;
                vmitem.Matched = item.Matched;
                vmitem.TrxID = item.TrxID;
                vmitem.ShopID = item.ShopID;
                vmitem.ShopName = item.ShopName;
                vmitem.WalletType = item.WalletType.ToString();
                vmitem.SMSType = item.SMSType.ToString();
                vmitem.Amount = String.Format("{0:n}", item.Amount);
                vmitem.AccountNumber = item.AccountNumber;
                vmitem.SMSDeliveryTime = item.SMSDeliveryTime.ToString(Common.Common.gDisplayDateTimeFormat) + " (" + Helpers.DataReaderHelper.GetCompareCurrentDate(item.SMSDeliveryTime) + " ago)"; ;
                vmitem.CreatedTime = item.CreatedTime.ToString(Common.Common.gDisplayDateTimeFormat) + " (" + Helpers.DataReaderHelper.GetCompareCurrentDate(item.CreatedTime) + " ago)";
                vmitem.RawMessage = Helpers.DataReaderHelper.GetFormString(item.RawMessage).Replace("\n", " ").Replace("\r", " ");
                vmitem.Sender = item.Sender;
                vmitem.Balance = String.Format("{0:n}", item.Balance);
                vm.Add(vmitem);


            }
            return vm;
        }

        public static List<SMSRecordViewModel> GetTransactionInfoCache(string UserID)
        {
            if (_CacheTransInfo.ContainsKey(UserID))
            {
                return _CacheTransInfo[UserID];
            }
            else
            {
                return new List<SMSRecordViewModel>();
            }
        }

        public static void UpdateTransactionInfoCache(string UserID, List<SMSRecordViewModel> vm)
        {
            if (_CacheTransInfo.ContainsKey(UserID))
            {
                _CacheTransInfo[UserID] = vm;
            }

        }

        public static SMSRecordGridFilterResult PerformFilter(string UserID, SMSRecordViewModel vm, int pageIndex, int pageSize, string sortField, string sortOrder)
        {
            SMSRecordGridFilterResult result = new SMSRecordGridFilterResult();
            List<SMSRecordViewModel> data = GetTransactionInfoCache(UserID);
            int FilterpageIndex = pageIndex - 1;
            if (pageIndex > 1)
            {
                FilterpageIndex = FilterpageIndex * pageSize;
            }
            data = FilterColumns(vm, data);
            data = PerformSorting(vm, data, sortField, sortOrder);
            result.FilterAmount = GetTransactionTotalAmount(data);
            List<SMSRecordViewModel> resultData = data.Skip(FilterpageIndex).Take(pageSize).ToList();
            result.data = resultData;
            result.itemsCount = data.Count;

            return result;


        }

        private static List<SMSRecordViewModel> FilterColumns(SMSRecordViewModel vm, List<SMSRecordViewModel> data)
        {
            List<SMSRecordViewModel> resultdata = data;
            if (vm.Matched != null)
            {
                data = data.Where(x => x.Matched.ToString().ToLower().Contains(vm.Matched.ToString().ToLower())).ToList();
            }

            if (vm.TrxID != null)
            {
                data = data.Where(x => x.TrxID.ToString().ToLower().Contains(vm.TrxID.ToString().ToLower())).ToList();
            }

            if (vm.ShopName != null)
            {
                data = data.Where(x => x.ShopName.ToString().ToLower().Contains(vm.ShopName.ToString().ToLower())).ToList();
            }


            if (vm.WalletType != null)
            {
                data = data.Where(x => x.WalletType.ToString().ToLower().Contains(vm.WalletType.ToString().ToLower())).ToList();
            }

            if (vm.SMSType != null)
            {
                data = data.Where(x => x.SMSType.ToString().ToLower().Contains(vm.SMSType.ToString().ToLower())).ToList();
            }

            if (vm.Amount != null)
            {
                data = data.Where(x => x.Amount.ToString().ToLower().Contains(vm.Amount.ToString().ToLower())).ToList();
            }

            if (vm.AccountNumber != null)
            {
                data = data.Where(x => x.AccountNumber.ToString().ToLower().Contains(vm.AccountNumber.ToString().ToLower())).ToList();
            }


            if (vm.SMSDeliveryTime != null)
            {
                data = data.Where(x => x.SMSDeliveryTime.ToString().ToLower().Contains(vm.SMSDeliveryTime.ToString().ToLower())).ToList();
            }

            if (vm.CreatedTime != null)
            {
                data = data.Where(x => x.CreatedTime.ToString().ToLower().Contains(vm.CreatedTime.ToString().ToLower())).ToList();
            }

            if (vm.RawMessage != null)
            {
                data = data.Where(x => x.RawMessage.ToString().ToLower().Contains(vm.RawMessage.ToString().ToLower())).ToList();
            }

            if (vm.Sender != null)
            {
                data = data.Where(x => x.Sender.ToString().ToLower().Contains(vm.Sender.ToString().ToLower())).ToList();
            }

            return data;

        }

        private static List<SMSRecordViewModel> PerformSorting(SMSRecordViewModel vm, List<SMSRecordViewModel> data, string sortField, string sortOrder)
        {
            List<SMSRecordViewModel> resultdata = data;
            if (sortField == null)
            {
                return data;
            }
            if (sortField == "TrxID")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.TrxID).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.TrxID).ToList();
                }

            }

            if (sortField == "ShopName")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.ShopName).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.ShopName).ToList();
                }

            }

            if (sortField == "WalletType")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.WalletType).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.WalletType).ToList();
                }

            }

            if (sortField == "SMSType")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.SMSType).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.SMSType).ToList();
                }

            }

            if (sortField == "Amount")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.Amount).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.Amount).ToList();
                }

            }

            if (sortField == "AccountNumber")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.AccountNumber).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.AccountNumber).ToList();
                }

            }

            if (sortField == "SMSDeliveryTime")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.SMSDeliveryTime).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.SMSDeliveryTime).ToList();
                }
            }

            if (sortField == "CreatedTime")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.CreatedTime).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.CreatedTime).ToList();
                }
            }

            if (sortField == "RawMessage")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.RawMessage).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.RawMessage).ToList();
                }
            }


            return data;
        }

        public static string GetTransactionTotalAmount(List<SMSRecordViewModel> data)
        {
            double totalAmount = 0;
            foreach (SMSRecordViewModel item in data)
            {
                totalAmount += Helpers.DataReaderHelper.GetFormDouble(item.Amount.ToString().Replace(",", ""));
            }

            return String.Format("{0:n0}", totalAmount);
        }


        public static void AddRecord(string MerchantID)
        {
            SMSRecordModel m = new SMSRecordModel();
            var rng = new Random();
            using (Repository<SMSRecordModel> db = new Repository<SMSRecordModel>())
            {
                m.ID = Guid.NewGuid().ToString("N");
                m.MerchantID = MerchantID;
                m.TrxID = "TRX0000" + rng.Next(1, 100);
                m.ShopID = "79819843c5b14bc099c77610d7254261";
                m.WalletType = WalletType.BKash;
                m.SMSType = TransactionType.CashIn;
                m.Amount = 500;
                m.AccountNumber = "ABC000" + rng.Next(1, 100);
                m.SMSDeliveryTime = DateTime.Now;
                m.CreatedTime = DateTime.Now;
                m.RawMessage = "Thank you. Welcome. BonJour.";
                db.Insert(m);
            }

        }


    }

}
