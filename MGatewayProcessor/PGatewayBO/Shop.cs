﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MGatewayBO.Model;
using Global.Model;
using MGatewayRepository;
namespace MGatewayProcessor
{
    public class Shop
    {
        public static JsGridModel GetShopGrid( string MerchantID)
        {
            JsGridModel m = new JsGridModel();
            using (Repository<ShopModel> db = new Repository<ShopModel>())
            {
                HashSet<string> HideColumns = new HashSet<string> { "ShopID",  "Password", "LastActiveTime","ActiveStatus" };
                List<ShopViewModel> ShopList = new List<ShopViewModel>();
                //if (UserID == "7c06b2db2f9e4dccad6d165444d555f0")
                //{
                //    ShopList = ConvertToVm(db.Include(x => x.wallets).ToList());
                //}
                //else
                //{
                    HideColumns.Add("MerchantID");
                     ShopList = ConvertToVm(db.Include(x => x.wallets).Where(x => x.MerchantID == MerchantID).ToList());
                //}
               

                m.data = ShopList;
                m.AllowAdd = false;
                m.AllowDelete = true;
                m.AllowEdit = false;
                m.HasDetails = true;
                m.KeyFieldName = "ShopID";
                GridHelper.AddFields("Shop", m, HideColumns);

                m.DeleteURL.urlAction = new ControllerURL { action = "DeleteShop", area = "", controller = "Shop" };
                m.ViewURL.urlAction = new ControllerURL { action = "ViewShopDetails", area = "", controller = "Shop" };
                m.ViewCallBack.Parentelem = "parentElement";
                m.ViewCallBack.childelem = "chilElement";
                m.ViewCallBack.childGridID = "ShopDetails";
                return m;
            }



        }

        private static List<ShopViewModel> ConvertToVm(List<ShopModel> m)
        {
            List<ShopViewModel> vm = new List<ShopViewModel>();
            foreach (ShopModel item in m)
            {
                ShopViewModel vmitem = new ShopViewModel();
                vmitem.ShopID = item.ShopID;
                vmitem.MerchantID = item.MerchantID;
                vmitem.ShopName = item.ShopName;
                vmitem.Contact = item.Contact;
                vmitem.WhatappsLink = item.WhatappsLink;
                vmitem.ShopStatus = item.ShopStatus.GetHashCode();
                vmitem.Password = "";
                vmitem.LastLoginDate = item.LastLoginDate.ToString(Common.Common.gDisplayDateTimeFormat);
                vmitem.CreatedDate = item.CreatedDate.ToString(Common.Common.gDisplayDateTimeFormat);
                vmitem.LastActiveTime = item.LastActiveTime.ToString(Common.Common.gDisplayDateTimeFormat);
                vmitem.ActiveStatus = item.ActiveStatus;
                double totalSeconds = DateTime.Now.Subtract(item.LastActiveTime).TotalSeconds;
                if(item.LastActiveTime == DateTime.MinValue)
                {
                    totalSeconds = -1;
                }
                if(totalSeconds < 0)
                {
                    vmitem.ActiveStatus = ActiveStatus.NeverActive;
                    vmitem.LastActiveTime = "Never Active";
                }else if(totalSeconds > 0 && totalSeconds <= 60)
                {
                    vmitem.ActiveStatus = ActiveStatus.Online;
                    vmitem.LastActiveTime = "Online";
                }
                else
                {
                    vmitem.ActiveStatus = ActiveStatus.LastSeen;
                    vmitem.LastActiveTime = "Last Seen " + Helpers.DataReaderHelper.GetCompareCurrentDate(item.LastActiveTime) + " ago";
                }
                //if (vmitem.ActiveStatus ==  ActiveStatus.NeverActive)
                //{
                //    vmitem.ActiveStatus = ActiveStatus.NeverActive;
                //    vmitem.LastActiveTime = "Never Active";
                //}
                //else if(vmitem.ActiveStatus == ActiveStatus.LastSeen )
                //{

                //    vmitem.ActiveStatus = ActiveStatus.LastSeen;
                //    vmitem.LastActiveTime = "Last Seen " + Helpers.DataReaderHelper.GetCompareCurrentDate(item.LastActiveTime) + " ago" ;
                //}
                //else
                //{
                //    vmitem.ActiveStatus = ActiveStatus.Online;
                //    vmitem.LastActiveTime = "Online";
                //}

                //if( totalSeconds <= 60)
                //{
                //    vmitem.ActiveStatus = ActiveStatus.Online;
                //    vmitem.LastActiveTime = "Online";
                //}
                vmitem.WalletsCount = item.wallets.Count;
                vm.Add(vmitem);
             
            }
            return vm;
        }

        private static List<ShopWalletsViewModel> ConvertToVmWallets(List<ShopWalletsSQLModel> m, Dictionary<string, ShopWalletTransactionModel> transm)
        {
            List<ShopWalletsViewModel> vm = new List<ShopWalletsViewModel>();
            foreach (ShopWalletsSQLModel item in m)
            {
                ShopWalletsViewModel vmitem = new ShopWalletsViewModel();
                vmitem.ShopID = item.ShopID;
                vmitem.WalletType = item.WalletType.GetHashCode().ToString();
                vmitem.WalletTypeDesc = item.WalletType.ToString();
                vmitem.WalletTypeOri = item.WalletType;
                vmitem.WalletNumber = item.WalletNumber;
                vmitem.CashInLimit = String.Format("{0:n0}", item.CashInLimit);
                vmitem.CashInTransaction = string.Format("{0:n0}", 0);
                if (transm.ContainsKey(item.ShopID + "|" + "1" + "|" + vmitem.WalletType))
                {
                    vmitem.CashInTransaction = string.Format("{0:n0}", transm[item.ShopID + "|" + "1" + "|" + vmitem.WalletType].Amount);
                }
                vmitem.CashOutLimit = String.Format("{0:n0}", item.CashOutLimit);
                vmitem.CashOutTransaction = string.Format("{0:n0}", 0);
                if (transm.ContainsKey(item.ShopID + "|" + "2" + "|" + vmitem.WalletType))
                {
                    vmitem.CashOutTransaction = string.Format("{0:n0}", transm[item.ShopID + "|" + "2" + "|" + vmitem.WalletType].Amount);
                }
                vmitem.AccountClass = item.AccountClass;
                vmitem.Priority = item.Priority.GetHashCode().ToString();
                vmitem.Enabled = item.Enabled;
                vmitem.ShopName = item.ShopName;
                vmitem.Sender = item.Sender;
                vm.Add(vmitem);
                //string SQL = "Select * from MerchantData t1 inner join GatewayMerchant t2 on t1.MerchantID = t2.MerchantID inner join GatewayData t3 on t3.GatewayID = t2.GatewayID where t1.MerchantID = @p0";
                //using (MGatewayDB db = new MGatewayDB())
                //{
                //    List<GatewayMerchantViewModel> GatewayList = db.Database.SqlQuery<GatewayMerchantViewModel>(SQL,vmitem.merchantID).ToList();
                //    vmitem.GatewayMerchants = GatewayList;
                //    vm.Add(vmitem);
                //}
                // List<GatewayMerchantViewModel> GatewayList = MySQLDB.GetValueModel<GatewayMerchantViewModel>(SQL, vmitem.merchantID).ToList();

            }
            return vm;
        }

        public static string UpdateShopDetails(string MerchantID, ShopModel vm)
        {
            using (Repository<ShopModel> db = new Repository<ShopModel>())
            {


                ShopModel m = db.Where(x => x.ShopName == vm.ShopName && x.MerchantID == MerchantID).FirstOrDefault();
                if (m != null)
                {

                    CopyVmToM(m, vm);
                    db.Update(m);
                }
                else
                {
                    return "Shop name not exists";
                }


            }
            return "";
        }

        public static string AddShopDetails(string MerchantID, ShopModel vm)
        {
            using (Repository<ShopModel> db = new Repository<ShopModel>())
            {


                ShopModel m = db.Where(x => x.ShopName.ToLower() == vm.ShopName.ToLower() && x.MerchantID == MerchantID).FirstOrDefault();
                if (m == null){
                    m = new ShopModel();
                    m.ShopID = Guid.NewGuid().ToString("N");
                    m.MerchantID = MerchantID;
                    m.CreatedDate = DateTime.Now;
                    m.LastLoginDate = DateTime.MinValue;
                    m.LastActiveTime = DateTime.MinValue;
                    CopyVmToM(m, vm);

                    db.Insert(m);
                }
                else
                {
                    return "This shop name has been taken.";
                }


            }
            return "";
        }

        private static void CopyVmToM(ShopModel m, ShopModel vm)
        {

            m.ShopName = vm.ShopName;
            m.Contact = vm.Contact;
            m.WhatappsLink = vm.WhatappsLink;
            m.ShopStatus = vm.ShopStatus;
            m.Password = (vm.Password != null ? Encryptor.Encryptor.GetHash(vm.ShopName.ToLower() + "LDKEY" + vm.Password + "ldkey" + vm.ShopName.ToUpper()) : m.Password);

        }

        public static void DeleteShop(ShopModel vm)
        {
            using (Repository<ShopModel> db = new Repository<ShopModel>())
            {
                ShopModel m = db.Find(vm.ShopID);
                db.Delete(m);
            }

        }


        public static JsGridModel GetWalletsGrid(string ShopID, string MerchantID)
        {
            JsGridModel m = new JsGridModel();
            using (Repository<ShopWalletsModel> db = new Repository<ShopWalletsModel>())
            {

                List<ShopWalletsViewModel> WalletList = GetShopWallets(ShopID, MerchantID);

                m.data = WalletList;
                m.AllowAdd = true;
                m.AllowDelete = true;
                m.AllowEdit = true;
                m.HasDetails = false;
                m.KeyFieldName = "ShopID,WalletType";
                GridHelper.AddFields("ShopWallets", m, (ShopID == string.Empty ? new HashSet<string> { "ShopID", "MerchantID" } : new HashSet<string> { "ShopID", "ShopName", "MerchantID" }));

                m.DeleteURL.urlAction = new ControllerURL { action = "DeleteWallet", area = "", controller = "Shop" };
                m.InsertURL.urlAction = new ControllerURL { action = "AddWallet", area = "", controller = "Shop" };
                m.UpdateURL.urlAction = new ControllerURL { action = "UpdateWallet", area = "", controller = "Shop" };
                //m.ViewURL.urlAction = new ControllerURL { action = "ViewShopDetails", area = "", controller = "Shop" };
                m.ViewCallBack.Parentelem = "parentElement";
                m.ViewCallBack.childelem = "chilElement";
                m.ViewCallBack.childGridID = "ShopDetails";
                return m;
            }
        }

        public static List<ShopWalletsViewModel> GetShopWallets(string ShopID, string MerchantID)
        {
            List<ShopWalletsSQLModel> m = new List<ShopWalletsSQLModel>();
            Dictionary<string, ShopWalletTransactionModel> Transm = new Dictionary<string, ShopWalletTransactionModel>();
            DateTime TodayDate = DateTime.Today;
            using (MGatewayDB db = new MGatewayDB())
            {
                string SQL = "Select t2.ShopID,t2.ShopName,t1.WalletType,t1.WalletNumber,t1.CashOutLimit,t1.CashInLimit,t1.AccountClass,t1.Priority,t1.Enabled,IFNULL(t3.Sender,'') Sender from ShopWallets " + Environment.NewLine +
                            " t1 inner join Shop t2 on t1.ShopID = t2.ShopID" + Environment.NewLine +
                            " left join ShopWalletsSender t3 on t3.WalletType = t1.WalletType";
                SQL += " where t2.MerchantID = @p0 ";

                
                string conditionSQL = "";
                if (ShopID != string.Empty)
                {
                    SQL += " and t2.ShopID = @p1";
                    conditionSQL += " and ShopID = @p3";

                }

                string TransactionSQL = "Select ShopID,MerchantID,TransactionType,WalletType,CAST(ApprovedTime AS DATE) AS ApprovedTime,Sum(Amount) Amount from CashInOut  where Status = 2 and " + Environment.NewLine +
                    " ApprovedTime between @p0 and @p1 and MerchantID = @p2 " + conditionSQL + " Group by ShopID,MerchantID,TransactionType,WalletType,CAST(ApprovedTime AS DATE)";

                Transm = db.Database.SqlQuery<ShopWalletTransactionModel>(TransactionSQL, TodayDate, TodayDate.AddHours(23).AddMinutes(59).AddSeconds(59), MerchantID, ShopID).ToDictionary(x => x.ShopID + "|" + x.TransactionType.GetHashCode() + "|" + x.WalletType.GetHashCode(), x=> x);
                m = db.Database.SqlQuery<ShopWalletsSQLModel>(SQL, MerchantID, ShopID).ToList();
                List<ShopWalletsViewModel> vm = ConvertToVmWallets(m, Transm);
                return vm;
            }


        }



        public static List<ShopModel> GetShop(string MerchantID)
        {
            using (Repository<ShopModel> db = new Repository<ShopModel>())
            {
                List<ShopModel> ShopList = db.Where(x => x.MerchantID == MerchantID).ToList();

                return ShopList;
            }
        }

        public static ShopModel GetShopByShopName(string ShopName)
        {
            using (Repository<ShopModel> db = new Repository<ShopModel>())
            {
                ShopModel Shop = db.Where(x => x.ShopName == ShopName).FirstOrDefault();

                return Shop;
            }
        }

        public static ShopWalletsModel GetShopWallet(string ShopID, WalletType walletype)
        {
            ShopWalletsModel m = new ShopWalletsModel();
          
            using (MGatewayDB db = new MGatewayDB())
            {
                string SQL = "Select * from ShopWallets where ShopID = @p0 and WalletType = @p1";
                m = db.Database.SqlQuery<ShopWalletsModel>(SQL,  ShopID, walletype).FirstOrDefault();
                return m;
            }

        }

        public static ShopWalletsModel GetShopbyWalletNumber(string WalletNumber, WalletType walletype)
        {
            ShopWalletsModel m = new ShopWalletsModel();

            using (MGatewayDB db = new MGatewayDB())
            {
                string SQL = "Select * from ShopWallets where WalletNumber = @p0 and WalletType = @p1";
                m = db.Database.SqlQuery<ShopWalletsModel>(SQL, WalletNumber, walletype).FirstOrDefault();
                return m;
            }

        }

        public static JsGridModel GetWalletsGrid(string MerchantID)
        {
            return GetWalletsGrid("", MerchantID);

        }

        public static List<ShopWalletsViewModel> GetAllShopWallets(string MerchantID)
        {
            return GetShopWallets("", MerchantID); 

        }




        public static void DeleteWallet(ShopWalletsModel vm)
        {
            using (Repository<ShopWalletsModel> db = new Repository<ShopWalletsModel>())
            {
                ShopWalletsModel m = db.Find(vm.ShopID, vm.WalletType);
                db.Delete(m);
            }
        }


        public static bool AddWallet(ShopWalletsModel vm)
        {
            using (Repository<ShopWalletsModel> db = new Repository<ShopWalletsModel>())
            {
                ShopWalletsModel m = db.Find(vm.ShopID, vm.WalletType);
                if (m == null)
                {
                    vm.CreatedTime = DateTime.Now;
                    db.Insert(vm);
                    return true;
                }
                return false;
            }
        }

        public static bool UpdateWallet(ShopWalletsViewModel vm)
        {
            using (Repository<ShopWalletsModel> db = new Repository<ShopWalletsModel>())
            {
                ShopWalletsModel m = db.Find(vm.ShopID, vm.WalletTypeOri);
                if (m != null)
                {
                    ConvertWalletToM(m, vm);
                    db.Update(m);
                    return true;
                }
                return false;

            }
        }


        private static void ConvertWalletToM(ShopWalletsModel m, ShopWalletsViewModel vm)
        {
            m.WalletType = vm.WalletTypeOri;
            m.WalletNumber = vm.WalletNumber;
            m.CashInLimit = Helpers.DataReaderHelper.GetFormDouble(vm.CashInLimit);
            m.CashOutLimit = Helpers.DataReaderHelper.GetFormDouble(vm.CashOutLimit);
            m.AccountClass = vm.AccountClass;
            m.Priority = (ShopPriority)Helpers.DataReaderHelper.GetFormInt(vm.Priority);
            m.Enabled = vm.Enabled;
        }

        public static List<ShopModel> GetShopToken()
        {
            using (Repository<ShopModel> db = new Repository<ShopModel>())
            {
                return db.Where(x=> x.Token != "" && x.ShopStatus == ShopStatus.Active).ToList();
            }
        }


        public static Dictionary<string, ShopWalletsSenderModel> GetShopWalletSender()
        {
            using (MGatewayDB db = new MGatewayDB())
            {
                Dictionary<string, ShopWalletsSenderModel> SenderWalletDict = new Dictionary<string, ShopWalletsSenderModel>();
                string SQL = " Select * from ShopWalletsSender ";
                List<ShopWalletsSenderModel> ShopWalletSenderList = db.Database.SqlQuery<ShopWalletsSenderModel>(SQL).ToList();

                foreach (ShopWalletsSenderModel item in ShopWalletSenderList)
                {
                  
                        SenderWalletDict[item.WalletType.GetHashCode().ToString()] = item;
                    
                }

                return SenderWalletDict;


            }
        }

        public static void UpdateShopWalletSender(List<ShopWalletsSenderModel> WalletSenderList)
        {
            if(WalletSenderList != null)
            {
                using (Repository<ShopWalletsSenderModel> db = new Repository<ShopWalletsSenderModel>())
                {

                    foreach (ShopWalletsSenderModel item in WalletSenderList)
                    {
                        ShopWalletsSenderModel m = db.Find(item.WalletType);
                        if (m != null)
                        {
                            m.Sender = item.Sender;
                            db.Update(m);
                        }
                    }
                }
            }
           

        }
    }

}
