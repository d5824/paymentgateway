﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MGatewayBO.Model;
using Global.Model;
using MGatewayRepository;

using System.Collections.Concurrent;

namespace MGatewayProcessor
{
    public class TransactionInfo
    {
        private static ConcurrentDictionary<string, List<TrxInfoViewModel>> _CacheTransInfo = new ConcurrentDictionary<string, List<TrxInfoViewModel>>();
        public static JsGridModel GetTransactionInfoGrid(string UserID, string _StartDate, string _EndDate)
        {
            DateTime StartDate = Helpers.DataReaderHelper.GetFormDate(_StartDate);
            DateTime EndDate = Helpers.DataReaderHelper.GetFormDate(_EndDate);
            EndDate = EndDate.AddHours(23).AddMinutes(59).AddSeconds(59);
            JsGridModel m = new JsGridModel();
            using (MGatewayDB db= new MGatewayDB())
            {
                string SQL = "Select t3.* from UserAccount t1 " + Environment.NewLine +
                    "inner join GatewayMerchant t2 on t1.MerchantID = t2.MerchantID" + Environment.NewLine +
                    "inner join TrxInfo t3 on t3.GatewayID = t2.GatewayID and t3.MerchantID = t2.merchantID" + Environment.NewLine +
                    "where UserID = @p0 and t3.CreatedTime >=  @p1 and t3.CreatedTime <= @p2";
                if(UserID == "7c06b2db2f9e4dccad6d165444d555f0")
                {
                    SQL = "Select * from TrxInfo where CreatedTime >=  @p1 and CreatedTime <= @p2";
                }
                List<TrxInfoViewModel> TransactionList = ConvertToVm(db.Database.SqlQuery<TrxInfoModel>(SQL,UserID,StartDate,EndDate).ToList());

                _CacheTransInfo[UserID] = TransactionList;
                m.data = TransactionList;
                m.AllowAdd = false;
                m.AllowDelete = false;
                m.AllowEdit = false;
                m.HasDetails = false;
                m.ServerSideLoading = true;
                m.KeyFieldName = "TrxID";
                GridHelper.AddFields("TrxInfo", m);

                
                m.DeleteURL.urlAction = new ControllerURL { action = "DeleteGateway", area = "", controller = "MerchantSetup" };
                m.ViewURL.urlAction = new ControllerURL { action = "ViewMerchantDetails", area = "", controller = "MerchantSetup" };
                m.LoadDataURL.urlAction = new ControllerURL { action = "LoadTransactionInfo", area = "", controller = "TransactionInfo" };
                m.ViewCallBack.Parentelem = "parentElement";
                m.ViewCallBack.childelem = "chilElement";
                m.ViewCallBack.childGridID = "TransactionDetails";
                return m;
            }



        }

        private static List<TrxInfoViewModel> ConvertToVm(List<TrxInfoModel> m)
        {
            Dictionary<string, MerchantDataModel> MerchantDataDict = new Dictionary<string, MerchantDataModel>();
            Dictionary<string, GatewayDataModel> GatewayDataDict = new Dictionary<string, GatewayDataModel>();
            using (MGatewayDB db = new MGatewayDB())
            {
                MerchantDataDict = db.Database.SqlQuery<MerchantDataModel>("Select * from MerchantData").ToDictionary(x => x.merchantID, x => x);
                GatewayDataDict = db.Database.SqlQuery<GatewayDataModel>("Select * from GatewayData ").ToDictionary(x => x.GatewayID, x => x);
            }
            List<TrxInfoViewModel> vm = new List<TrxInfoViewModel>();
            foreach(TrxInfoModel item in m)
            {
                TrxInfoViewModel vmitem = new TrxInfoViewModel();
                vmitem.TrxID  = item.TrxID;
                vmitem.TrxID_G = item.TrxID_G;
                vmitem.TrxID_M = item.TrxID_M;
                vmitem.TrxStatus = item.TrxStatus.GetEnumDescription();
                vmitem.MerchantID = (MerchantDataDict.ContainsKey(item.MerchantID) ? MerchantDataDict[item.MerchantID].merchantName : string.Empty) ;
                vmitem.GatewayID = (GatewayDataDict.ContainsKey(item.GatewayID) ? GatewayDataDict[item.GatewayID].GatewayName : string.Empty);
                vmitem.CreatedTime = item.CreatedTime.ToString(Common.Common.gDisplayDateTimeFormat );
                vmitem.GatewayGeneratedTime = (item.GatewayGeneratedTime == DateTime.MinValue ? "" : item.GatewayGeneratedTime.ToString(Common.Common.gDisplayDateTimeFormat));
                vmitem.GatewayRespondTime = (item.GatewayRespondTime == DateTime.MinValue ? "" : item.GatewayRespondTime.ToString(Common.Common.gDisplayDateTimeFormat));
                vmitem.ManualUpdateTime = (item.ManualUpdateTime == DateTime.MinValue ? "" : item.ManualUpdateTime.ToString(Common.Common.gDisplayDateTimeFormat));
                vmitem.UpdateBy = item.UpdateBy;
                vmitem.Amount = String.Format("{0:n0}",item.Amount);
                vmitem.Currency = item.Currency;
                vmitem.ServiceID = item.ServiceID;
                vm.Add(vmitem);


                // List<GatewayMerchantViewModel> GatewayList = MySQLDB.GetValueModel<GatewayMerchantViewModel>(SQL, vmitem.merchantID).ToList();

            }
            return vm;
        }
      
     


      
        public static void DeleteMerchant(MerchantDataModel vm)
        {
            using (Repository<MerchantDataModel> db = new Repository<MerchantDataModel>())
            {
                MerchantDataModel m = db.Find(vm.merchantID);
                db.Delete(m);
            }

        }

        public static string AddGatewayMerchant(string MerchantID, string GatewayID)
        {
            using (Repository<GatewayMerchantModel> db = new Repository<GatewayMerchantModel>())
            {


                GatewayMerchantModel m = db.Where(x => x.GatewayID == GatewayID && x.merchantID == MerchantID).FirstOrDefault();
                if (m == null)
                {
                    m = new GatewayMerchantModel();
                    m.ServiceID = Guid.NewGuid().ToString("N");
                    m.merchantID = MerchantID;
                    m.GatewayID = GatewayID;
                    db.Insert(m);
                    return m.ServiceID;
                }
                else
                {
                    return string.Empty;

                   
                }


            }
           

            //;
            
            
        }

        public static bool DeleteGatewayMerchant(string ServiceID)
        {
            using (Repository<GatewayMerchantModel> db = new Repository<GatewayMerchantModel>())
            {


                GatewayMerchantModel m = db.Find(ServiceID);
                db.Delete(m);


            }
            return true;
        }

        public static List<TrxInfoViewModel> GetTransactionInfoCache(string UserID)
        {
            if (_CacheTransInfo.ContainsKey(UserID))
            {
                return _CacheTransInfo[UserID];
            }
            else
            {
                return new List<TrxInfoViewModel>();
            }
        }

        public static TrxInfoGridFilterResult PerformFilter(string UserID, TrxInfoViewModel vm, int pageIndex, int pageSize, string sortField, string sortOrder)
        {
            TrxInfoGridFilterResult result = new TrxInfoGridFilterResult();
            List<TrxInfoViewModel> data = GetTransactionInfoCache(UserID);
            int FilterpageIndex = pageIndex - 1;
            if (pageIndex > 1)
            {
                FilterpageIndex = FilterpageIndex * pageSize;
            }
            data = FilterColumns(vm,  data);
            data = PerformSorting(vm, data, sortField, sortOrder);
            result.FilterAmount = GetTransactionTotalAmount(data);
            List<TrxInfoViewModel> resultData = data.Skip(FilterpageIndex).Take(pageSize).ToList();
            result.data = resultData;
            result.itemsCount = data.Count;

            return result;


        }

        private static List<TrxInfoViewModel> FilterColumns(TrxInfoViewModel vm,List<TrxInfoViewModel> data)
        {
            List<TrxInfoViewModel> resultdata = data;
            if (vm.TrxID != null)
            {
                data = data.Where(x => x.TrxID.ToString().ToLower().Contains(vm.TrxID.ToString().ToLower())).ToList();
            }

            if(vm.TrxID_G != null)
            {
                data = data.Where(x => x.TrxID_G.ToString().ToLower().Contains(vm.TrxID_G.ToString().ToLower())).ToList();
            }

            if(vm.TrxID_M != null)
            {
                data = data.Where(x => x.TrxID_M.ToString().ToLower().Contains(vm.TrxID_M.ToString().ToLower())).ToList();
            }

            if (vm.Amount != null)
            {
                data = data.Where(x => x.Amount.ToString().ToLower().Contains(vm.Amount.ToString().ToLower())).ToList();
            }

            if (vm.Currency != null)
            {
                data = data.Where(x => x.Currency.ToString().ToLower().Contains(vm.Currency.ToString().ToLower())).ToList();
            }

            if (vm.MerchantID != null)
            {
                data = data.Where(x => x.MerchantID.ToString().ToLower().Contains(vm.MerchantID.ToString().ToLower())).ToList();
            }

            if(vm.GatewayID != null)
            {
                data = data.Where(x => x.GatewayID.ToString().ToLower().Contains(vm.GatewayID.ToString().ToLower())).ToList();
            }

            if(vm.TrxStatus != null)
            {
                data = data.Where(x => x.TrxStatus.ToString().ToLower().Contains(vm.TrxStatus.ToString().ToLower())).ToList();
            }

            if(vm.CreatedTime != null)
            {
                data = data.Where(x => x.CreatedTime.ToString().ToLower().Contains(vm.CreatedTime.ToString().ToLower())).ToList();
            }

            if(vm.GatewayGeneratedTime != null)
            {
                data = data.Where(x => x.GatewayGeneratedTime.ToString().ToLower().Contains(vm.GatewayGeneratedTime.ToString().ToLower())).ToList();
            }

            if(vm.GatewayRespondTime != null)
            {
                data = data.Where(x => x.GatewayRespondTime.ToString().ToLower().Contains(vm.GatewayRespondTime.ToString().ToLower())).ToList();
            }

            if(vm.ManualUpdateTime != null)
            {
                data = data.Where(x => x.ManualUpdateTime.ToString().ToLower().Contains(vm.ManualUpdateTime.ToString().ToLower())).ToList();
            }

            if(vm.UpdateBy != null)
            {
                data = data.Where(x => x.UpdateBy.ToString().ToLower().Contains(vm.UpdateBy.ToString().ToLower())).ToList();
            }


            return data;
      
    }

        private static List<TrxInfoViewModel> PerformSorting(TrxInfoViewModel vm, List<TrxInfoViewModel> data,string sortField, string sortOrder)
        {
            List<TrxInfoViewModel> resultdata = data;
            if(sortField == null)
            {
                return data;
            }
            if (sortField == "TrxID")
            {
                if(sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.TrxID).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.TrxID).ToList();
                }
                
            }

            if (sortField == "Currency")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.Currency).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.Currency).ToList();
                }

            }


            if (sortField == "Amount")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.Amount).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.Amount).ToList();
                }

            }

            if (sortField == "TrxID_G")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.TrxID_G).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.TrxID_G).ToList();
                }
            }

            if (sortField == "TrxID_M")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.TrxID_M).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.TrxID_M).ToList();
                }
            }

            if (sortField == "MerchantID")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.MerchantID).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.MerchantID).ToList();
                }
            }

            if (sortField == "GatewayID")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.GatewayID).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.GatewayID).ToList();
                }
            }

            if (sortField == "TrxStatus")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.TrxStatus).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.TrxStatus).ToList();
                }
            }

            if (sortField == "CreatedTime")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.CreatedTime).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.CreatedTime).ToList();
                }
            }

            if (sortField == "GatewayGeneratedTime")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.GatewayGeneratedTime).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.GatewayGeneratedTime).ToList();
                }
            }

            if (sortField == "GatewayRespondTime")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.GatewayRespondTime).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.GatewayRespondTime).ToList();
                }
            }

            if (sortField == "ManualUpdateTime")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.ManualUpdateTime).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.ManualUpdateTime).ToList();
                }
            }

            if (sortField == "UpdateBy")
            {
                if (sortOrder == "asc")
                {
                    data = data.OrderBy(x => x.UpdateBy).ToList();
                }
                else
                {
                    data = data.OrderByDescending(x => x.UpdateBy).ToList();
                }
            }

            return data;
        }

        public static  string GetTransactionTotalAmount( List<TrxInfoViewModel> data)
        {
            double totalAmount = 0;
            foreach (TrxInfoViewModel item in data)
            {
                totalAmount += Helpers.DataReaderHelper.GetFormDouble(item.Amount.ToString().Replace(",", ""));
            }

            return String.Format("{0:n0}", totalAmount);
        }


    }

}
