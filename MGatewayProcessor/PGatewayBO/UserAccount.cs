﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MGatewayBO.Model;
using Global.Model;
using MGatewayRepository;
namespace MGatewayProcessor
{
    public class UserAccount
    {

        public static JsGridModel GetUserGrid(string UserID)
        {
            JsGridModel m = new JsGridModel();
            //using (Repository<UserAccountModel> db = new Repository<UserAccountModel>())
            //{
            using (MGatewayDB db = new MGatewayDB())
            {

                string SQL = "Select * from UserLogin t1 " + Environment.NewLine;
                if(UserID != "7c06b2db2f9e4dccad6d165444d555f0")
                {
                    SQL += "inner join UserLogin t2 on t1.MerchantID = t2.MerchantID and t2.UserID = @p0";
                }
                List<UserLoginModel> UserList = db.Database.SqlQuery<UserLoginModel>(SQL, UserID).ToList();

                m.data = ConvertToVm(UserList);
                m.AllowAdd = false;
                m.AllowDelete = true;
                m.AllowEdit = false;
                m.HasDetails = true;
                m.KeyFieldName = "UserID";
                GridHelper.AddFields("UserLogin", m, new HashSet<string> { "UserID", "UserPassword", "CreatedDate", "FailedLoginAttempt" });

                m.DeleteURL.urlAction = new ControllerURL { action = "DeleteUser", area = "", controller = "UserAccount" };
                m.ViewURL.urlAction = new ControllerURL { action = "ViewUserDetails", area = "", controller = "UserAccount" };
                m.ViewCallBack.Parentelem = "parentElement";
                m.ViewCallBack.childelem = "chilElement";
                m.ViewCallBack.childGridID = "UserDetails";
                return m;
                //}
            }



        }


        public static bool UpdateUsersDetails(UserLoginViewModel vm)
        {
            using (Repository<UserLoginModel> db = new Repository<UserLoginModel>())
            {


                UserLoginModel m = db.Find(vm.UserID);
                if (m != null)
                {

                    CopyVmToM(m, vm);
                    db.Update(m);
                }
                else
                {
                    if(db.Where(x=> x.UserName == vm.UserName).FirstOrDefault() == null)
                    {
                        m = new UserLoginModel();
                        m.UserID = Guid.NewGuid().ToString("N");
                        m.CreatedDate = DateTime.Now;
                        m.UserRole = "";
                        CopyVmToM(m, vm);

                        db.Insert(m);
                        return true;
                    }
                    return false;

                }


            }
            return true;
        }

        private static UserLoginViewModel ConvertToVm(UserLoginModel m)
        {
            UserLoginViewModel vm = new UserLoginViewModel();
            vm.UserID = m.UserID;
            vm.MerchantID = m.MerchantID;
            vm.UserName = m.UserName;
            vm.UserPassword = "";
            vm.Email = m.Email;
            vm.LastLoginDate = m.LastLoginDate.ToString(Common.Common.gDisplayDateTimeFormat);
            vm.FailedLoginAttempt = m.FailedLoginAttempt;
            vm.Status = (int)m.Status;
            vm.strStatus = m.Status.GetEnumDescription();
            vm.CreatedDate = m.CreatedDate.ToString(Common.Common.gDisplayDateTimeFormat);
            vm.UserRole = m.UserRole;
            return vm;


        }

        private static List<UserLoginViewModel> ConvertToVm(List<UserLoginModel> m)
        {
            List<UserLoginViewModel> vm = new List<UserLoginViewModel>();
            foreach (UserLoginModel item in m.OrderByDescending(x => x.CreatedDate))
            {
                vm.Add(ConvertToVm(item));
            }

            return vm;
        }

        private static void CopyVmToM(UserLoginModel m, UserLoginViewModel vm)
        {
            m.UserName = vm.UserName;
            m.UserPassword = (vm.UserPassword != null ? Encryptor.Encryptor.GetHash(vm.UserName.ToLower() + "LDKEY" + vm.UserPassword + "ldkey" + vm.UserName.ToUpper()) : m.UserPassword);
            m.Email = vm.Email;
            m.MerchantID = vm.MerchantID;
            m.UserRole = vm.UserRole;
            m.Status = (AccStatus)vm.Status;
        }

        public static void DeleteUser(UserLoginViewModel vm)
        {
            using (Repository<UserLoginModel> db = new Repository<UserLoginModel>())
            {
                UserLoginModel m = db.Find(vm.UserID);
                db.Delete(m);
            }

        }
    }

}
