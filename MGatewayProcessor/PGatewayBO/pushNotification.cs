﻿using FirebaseAdmin;
using FirebaseAdmin.Messaging;
using Google.Apis.Auth.OAuth2;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;


namespace MGatewayProcessor
{
    public class PushNotification
    {
        public static async Task pushMessageAsync(string Title, string Content, List<string> Receiver)
        {
            
            // See documentation on defining a message payload.
            var message = new MulticastMessage()
            {
                Tokens = Receiver,
                Data = new Dictionary<string, string>()
                        {
                            { "score", "850" },
                            { "time", "2:45" },
                            { "body", Content },
                            { "title", Title },
                            { "click_action", ".NavActivity" },

                        },
                Notification = new Notification()
                {
                    Title = Title,
                    Body = Content,
                },
                Android = new AndroidConfig()
                { 
                    Data = new Dictionary<string, string>()
                            {
                                { "score", "850" },
                                { "time", "2:45" },
                                { "body", Content },
                                { "title", Title },
                                { "click_action", ".NavActivity" },

                            },
                    Notification = new AndroidNotification()
                    { 
                        ClickAction = ".NavActivity",
                        Body = Content,
                        Title = Title
                        
                    }


                }
               
            };

            // Send a message to the device corresponding to the provided
            // registration token.
            //string response = await FirebaseMessaging.DefaultInstance.SendAsync(message);

            var response = await FirebaseMessaging.DefaultInstance.SendMulticastAsync(message);
        }

        public static string SendNotification(string message)
        {
            string returnMessage = null;
            var jFCMData = new JObject();
            var jData = new JObject();
            jData.Add("message", message);
            jFCMData.Add("to", "/topics/global");
            jFCMData.Add("data", jData);
            //string path = AppDomain.CurrentDomain.BaseDirectory + @"Content\pushNotificationKey.json";
            //string API_KEY = GoogleCredential.FromFile(path);
            string API_KEY = "AAAAtZrkgp0:APA91bF_4wIooUxFd233eUUCwMuIRHCAipUSmpiSHRScY8XE1iBpd2gT3LU4CsRJ3MOa9cAE7Qxale_zYzODr7JB9EBlB7ERMwi2unM6vOAf7jLRvG4u5mYcOKomAjMXyM444hnioTFg";
            var url = new Uri("https://fcm.googleapis.com/fcm/send");
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "key=" + API_KEY);
                    Task.WaitAll(client.PostAsync(url, new StringContent(jFCMData.ToString(), Encoding.Default, "application/json")).ContinueWith(response => {
                        returnMessage = response + "\nMessage sent";
                        Console.WriteLine(jFCMData.ToString());
                    }));
                }
            }
            catch (Exception e)
            {
                throw (new Exception("Unable to send GCM message:\n" + e.StackTrace));
            }
            return returnMessage;
        }


        private static string serverKey = "AAAAtZrkgp0:APA91bF_4wIooUxFd233eUUCwMuIRHCAipUSmpiSHRScY8XE1iBpd2gT3LU4CsRJ3MOa9cAE7Qxale_zYzODr7JB9EBlB7ERMwi2unM6vOAf7jLRvG4u5mYcOKomAjMXyM444hnioTFg";
        private static string senderId = "779987747485";
        private static string webAddr = "https://fcm.googleapis.com/fcm/send";
        public static string SendNotification2(List<string> Receiver, string title, string msg)
        {
            var result = "-1";
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(webAddr);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Headers.Add(string.Format("Authorization: key={0}", serverKey));
            httpWebRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
            httpWebRequest.Method = "POST";

            var payload = new
            {
                to= "/topics/my_topic",
                registration_ids = Receiver,
                priority = "high",
                content_available = true,
                data = new
                {
                    body = msg,
                    title = title,
                    text = msg
                },
                click_action = ".NavActivity"
            };
           // var serializer = new JavaScriptSerializer();
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
               // string json = serializer.Serialize(payload);
                string json = JsonConvert.SerializeObject(payload);
                streamWriter.Write(json);
                streamWriter.Flush();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }
            return result;
        }

    }
}
