﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Encryptor;
using MGatewayRepository;
using MGatewayBO.Model;
using MySql.Data.MySqlClient;
using System.Data;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MGatewayProcessor
{
    public class Generator
    {
        public static void InsertUserData(List<UserDataModel> mList)
        {
            //init db connection for bulk insert
            using (Repository<UserDataModel> db = new Repository<UserDataModel>())
            {
                db.Find("");
            }


            using (Repository<UserDataModel> db = new Repository<UserDataModel>())
            {
                List<UserDataModel> mInsert = new List<UserDataModel>();
                mInsert.AddRange(mList);
                // db.Insert(mList[0]);  
                db.BulkInsert(mInsert);
              
            }


        }

        public static void InsertUserData2(List<UserDataModel> mList)
        {
            StringBuilder json = new StringBuilder();
            // new JavaScriptSerializer().Serialize(mList, json);
            string strJson = Newtonsoft.Json.JsonConvert.SerializeObject(mList);

    
            using (MySqlCommand comm = new MySqlCommand())
            {
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandText = "spUpdateParticipantList";
                comm.Parameters.AddWithValue("@_jsonData", strJson);
               //  MySQLDB.GetValueModel<UserDataModel>(comm).ToList();
                MySQLDB.ExecuteNonQuery(comm);
            }

         

        }

        public static UserDataModel UserDataModelGET(string UID)
        {
            List<UserDataModel> mList;
            using (MySqlCommand comm = new MySqlCommand())
            {
                comm.CommandType = CommandType.StoredProcedure;
                comm.CommandText = "spGetUserData";
                comm.Parameters.AddWithValue("@_UID", UID);
                mList = MySQLDB.GetValueModel<UserDataModel>(comm).ToList();
            }

            return mList.FirstOrDefault();
        }
    }
}