﻿using FirebaseAdmin;
using FirebaseAdmin.Messaging;
using Google.Apis.Auth.OAuth2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MGatewayBO.Model;
using Global.Model;
using MGatewayRepository;
using System.Net.Mail;
using System.Net;

namespace MGatewayProcessor
{
    public class EmailNotification
    {
        public static string SendEmail()
        {
            GlobalSetingsViewModel gbl = GlobalSettings.GetSettings();
            double Idletime = Helpers.DataReaderHelper.GetFormDouble(gbl.IdleInterval.ToString());
            bool SendNotification = Helpers.DataReaderHelper.GetFormBoolean(gbl.SendNotification.ToString());

            List<string> MerchantList = new List<string>();
            //MerchantList = MerchantID.Split(',').ToList();
            if(SendNotification)
            {
                using (Repository<ShopModel> db = new Repository<ShopModel>())
                {
                    List<ShopModel> ShopOffline = new List<ShopModel>();

                    List<ShopModel> ShopList = new List<ShopModel>();
                    ShopList = db.Where(x => x.ShopStatus == ShopStatus.Active).ToList();
                    foreach (ShopModel item in ShopList)
                    {
                        double totalSeconds = DateTime.Now.Subtract(item.LastActiveTime).TotalSeconds;
                        if (totalSeconds > Idletime)
                        {
                            //item.ShopName + " : " + " Last Seen " + Helpers.DataReaderHelper.GetCompareCurrentDate(item.LastActiveTime) + " ago"
                            ShopOffline.Add(item);
                        }
                    }
                    if (ShopOffline.Count > 0)
                    {
                        return Send(gbl.Recipient, ShopOffline);
                    }
                    else
                    {
                        return "No Shop Offline";
                    }



                }
            }

            return "Send Email Off";
           

        }

        public static string Send(string recipientID, List<ShopModel> ShopOffline)
        {


            SmtpClient Smtp = new SmtpClient();

            MailAddress Sender = new MailAddress("genkinpay@gmail.com", "GenKin Pay Admin");
            MailMessage mm = new MailMessage();
            mm.Sender = Sender;
            foreach (string item in recipientID.Split(';'))
            {
                MailAddress Recipient = new MailAddress(item, item);
                mm.To.Add(Recipient);
            }
            // change here for testing
           
      
            string EmailBody = string.Empty;
            EmailBody = "<table style=\"border-collapse: collapse;width: 100%;\">";
            EmailBody += "<thead>";
            EmailBody += "<tr>";
            EmailBody += "<td style=\"border:1px black solid\">Shop Name</td>";
            EmailBody += "<td style=\"border:1px black solid\">Merchant</td>";
            EmailBody += "<td style=\"border:1px black solid\">Contact No</td>";
            EmailBody += "<td style=\"border:1px black solid\">Last Seen</td>";
            EmailBody += "</tr>";
            EmailBody += "</thead>";
            foreach (ShopModel shop in ShopOffline)
            {
                EmailBody += "<tr>";
                EmailBody += "<td style=\"border:1px black solid\">" + shop.ShopName + "</td>";
                EmailBody += "<td style=\"border:1px black solid\">" + shop.MerchantID + "</td>";
                EmailBody += "<td style=\"border:1px black solid\">" + shop.Contact + "</td>";
                EmailBody += "<td style=\"border:1px black solid\">" + Helpers.DataReaderHelper.GetCompareCurrentDate(shop.LastActiveTime) + " ago" + "</td>";
                EmailBody += "</tr>";
            }
            EmailBody += "</table>";

            mm.Body = EmailBody;
            mm.From = Sender;  //new MailAddress(m.UserName); 
            mm.Sender = Sender;   // Sender: Address of the actual sender acting on behalf of the author listed in the From: field (secretary, list manager, etc.).
            mm.Subject = "GenKinPay - Agent Offline";
            mm.IsBodyHtml = true;
            Smtp.Host = "smtp.gmail.com";
            Smtp.Port = 587;//587 for tsl,  465 for ssl
            Smtp.UseDefaultCredentials = true;
            NetworkCredential nc = new NetworkCredential("genkinpay@gmail.com", "Abcd123#");
            Smtp.EnableSsl = true; //is enable = ssl else tls
            Smtp.Credentials = nc;
            //required to open lessscure connection from google settings.
            try
            {
                Smtp.Send(mm);
            }
            catch (Exception ex)
            {

                return ex.Message + Environment.NewLine + ex.StackTrace;
            }

            return "Mail Succesfully Sent";



        }
    }
}

