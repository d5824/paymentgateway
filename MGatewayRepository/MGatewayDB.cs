﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using MGatewayBO.Model;
using MySql.Data.EntityFramework;

namespace MGatewayRepository
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class MGatewayDB : DbContext
    {
        public MGatewayDB() : base(MySQLDB.Instance(true).ConnectionString)
        {
            this.Configuration.ValidateOnSaveEnabled = false;
        }

      public MGatewayDB(string Connectionstring)
        {
            this.Database.Connection.ConnectionString = Connectionstring;
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<UserLoginModel>().ToTable("UserLogin");
            modelBuilder.Entity<ShopModel>().ToTable("Shop");
            modelBuilder.Entity<ShopWalletsModel>().ToTable("ShopWallets").
                HasRequired(x => x.master).WithMany(x => x.wallets).
                HasForeignKey(x => x.ShopID).
                WillCascadeOnDelete(true);
            modelBuilder.Entity<SMSRecordModel>().ToTable("SMSRecord");
            modelBuilder.Entity<CashInOutModel>().ToTable("CashInOut");
            modelBuilder.Entity<MerchantDataModel>().ToTable("MerchantData");
            modelBuilder.Entity<GlobalSetingsModel>().ToTable("GlobalSettings");
            modelBuilder.Entity<UnKnowSMSModel>().ToTable("UnknowSMSRecord");
            modelBuilder.Entity<ShopWalletsSenderModel>().ToTable("ShopWalletsSender");
            modelBuilder.Entity<SMSRecordDetailsModel>().ToTable("SMSRecordDetails");
            modelBuilder.Entity<RejectRemarksModel>().ToTable("RejectRemarks");
            modelBuilder.Entity<APIIntegration.Model.CashInOutApiMappingModel>().ToTable("CashInOutApiMapping");
            modelBuilder.Entity<Global.Model.FileLibraryModel>().ToTable("FileLibrary");
            //modelBuilder.Entity<TrxInfoModel>().ToTable("TrxInfo");
            //modelBuilder.Entity<AamarPayRecordModel>().ToTable("AamarPayRecord");
            //modelBuilder.Entity<AamarPayRespondModel>().ToTable("AamarPayRespond");
            //modelBuilder.Entity<GatewayDataModel>().ToTable("GatewayData");
            //modelBuilder.Entity<MerchantDataModel>().ToTable("MerchantData");
            //modelBuilder.Entity<GatewayMerchantModel>().ToTable("GatewayMerchant");
            //modelBuilder.Entity<LogDataModel>().ToTable("LogData").Property(x => x.LogID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            //modelBuilder.Entity<UserAccountModel>().ToTable("UserAccount");
            //modelBuilder.Entity<RequestPayment>().ToTable("RequestInfo");

        }
    } 


}