use PGatewayDev;
drop table GatewayMerchant;
drop table TrxInfo;
drop table GatewayData;
drop table MerchantData;

Create TABLE IF NOT EXISTS LogData(
LogID INT NOT NULL AUTO_INCREMENT,
LogCategory varchar(50),
LogType varchar(50),
LogContent varchar(5000),
LogBy varchar(50),
LogDateTime datetime,
CONSTRAINT PK_LogData PRIMARY KEY (LogID)
);

Create TABLE IF NOT EXISTS MerchantData(
merchantID varchar(50) not null,
merchantName varchar(50),
merchantStatus int,
success_url varchar(100) not null,
fail_url varchar(100) not null,
cancel_url varchar(100) not null,
RedirectUrl varchar(100) not null,
CONSTRAINT PK_MerchantData PRIMARY KEY (MerchantID)
);

ALTER TABLE `MerchantData` ADD `RedirectUrl` varchar(100) NOT NULL default '';


/*Further break to GatewayChannel table if necessary*/
Create TABLE IF NOT EXISTS GatewayData(
GatewayID varchar(50) not null,
GatewayKey varchar(50),
GatewayName varchar(50),
GatewayEmail varchar(100) DEFAULT NULL,
CONSTRAINT PK_GatewayData PRIMARY KEY (GatewayID)
);

Create TABLE IF NOT EXISTS GatewayMerchant(
GatewayID varchar(50) not null,
merchantID varchar(50) not null,
AssignStatus int,
CONSTRAINT PK_GatewayMerchant PRIMARY KEY (GatewayID,merchantID),
CONSTRAINT FK_GatewayMerchant_Gateway foreign key (GatewayID) references GatewayData(GatewayID),
CONSTRAINT FK_GatewayMerchant_Merchant foreign key (merchantID) references MerchantData(merchantID)
);

Create TABLE IF NOT EXISTS TrxInfo(
TrxID varchar(100) not null,
TrxID_M varchar(100) not null,
TrxID_G varchar(100) not null,
TrxStatus int,
Currency varchar(10) not null,
Amount decimal(20,4) not null,
ServiceID varchar(50) not null,
MerchantID varchar(50) not null,
GatewayID varchar(50) not null,
CreatedTime datetime,
GatewayGeneratedTime datetime,
GatewayRespondTime datetime,
ManualUpdateTime datetime,
UpdateBy varchar(50) not null,
CONSTRAINT PK_TrxInfo PRIMARY KEY (TrxID),
CONSTRAINT FK_TrxInfo_GatewayMerchant foreign key (ServiceID) references GatewayMerchant(ServiceID)
);


Create TABLE IF NOT EXISTS AamarPayRecord(
store_id varchar(50) not null,
signature_key varchar(50) not null,
amount decimal not null,
currency varchar(100) not null,
tran_id varchar(100) not null,
cus_name varchar(100) not null,
cus_email varchar(100) not null,
cus_add1 varchar(100) not null,
cus_add2 varchar(100) not null,
cus_city varchar(100) not null,
cus_state varchar(100) not null,
cus_postcode varchar(100) not null,
cus_country varchar(100) not null,
cus_phone varchar(100) not null,
cus_fax varchar(100) not null,
ship_name varchar(100) not null,
ship_add1 varchar(100) not null,
ship_add2 varchar(100) not null,
ship_city varchar(100) not null,
ship_state varchar(100) not null,
ship_postcode varchar(100) not null,
ship_country varchar(100) not null,
`desc` varchar(100) not null,
amount_processingfee_ratio varchar(100) not null,
amount_processingfee varchar(100) not null,
amount_vatratio varchar(100) not null,
amount_vat varchar(100) not null,
amount_taxratio varchar(100) not null,
amount_tax varchar(100) not null,
success_url varchar(100) not null,
fail_url varchar(100) not null,
cancel_url varchar(100) not null,
opt_a varchar(100) not null,
opt_b varchar(100) not null,
opt_c varchar(100) not null,
opt_d varchar(100) not null,
CONSTRAINT PK_AamarPayRecord PRIMARY KEY (tran_id)
);

Create TABLE IF NOT EXISTS AamarPayRespond(
pg_service_charge_bdt varchar(100) not null,
pg_service_charge_usd varchar(100) not null,
pg_card_bank_name varchar(100) not null,
pg_card_bank_country varchar(100) not null,
card_number varchar(100) not null,
card_holder varchar(100) not null,
cus_phone varchar(100) not null,
`desc` varchar(100) not null,
success_url varchar(100) not null,
fail_url varchar(100) not null,
cus_name varchar(100) not null,
cus_email varchar(100) not null,
currency_merchant varchar(100) not null,
convertion_rate varchar(100) not null,
ip_address varchar(100) not null,
other_currency varchar(100) not null,
pay_status varchar(100) not null,
pg_txnid varchar(100) not null,
epw_txnid varchar(100) not null,
mer_txnid varchar(100) not null,
store_id varchar(100) not null,
merchant_id varchar(100) not null,
currency varchar(100) not null,
store_amount varchar(100) not null,
pay_time varchar(100) not null,
amount varchar(100) not null,
bank_txn varchar(100) not null,
card_type varchar(100) not null,
reason varchar(100) not null,
pg_card_risklevel varchar(100) not null,
pg_error_code_details varchar(100) not null,
opt_a varchar(100) not null,
opt_b varchar(100) not null,
opt_c varchar(100) not null,
opt_d varchar(100) not null,
RecordTime datetime not null,
ResultStatus int not null,
CONSTRAINT PK_AamarPayRespond PRIMARY KEY (mer_txnid)
);


Create TABLE IF NOT EXISTS RequestInfo(
ID INT NOT NULL AUTO_INCREMENT,
serviceID varchar(50) not null,
transactionID varchar(50) not null,
userID varchar(100) not null,
Currency varchar(10) not null,
Amount decimal (20,4) not null,
sign varchar(100) not null,
NotifyUrl varchar(100) not null,
CONSTRAINT PK_RequestInfo PRIMARY KEY (ID)
);

Create TABLE IF NOT EXISTS tblTrxID(
ID INT NOT NULL AUTO_INCREMENT,
GUID varchar(50),
RecordDate datetime,
CONSTRAINT PK_tblTrxID PRIMARY KEY (ID)
);


Insert into MerchantData (merchantID,merchantName,merchantStatus,success_url,fail_url,cancel_url)
value ('jeetbuzz','jeetbuzz',1,'https://test@test.com/success','','');

Insert into GatewayData (GatewayID,GatewayKey,GatewayName,GatewayEmail)
value ('G000001','soho',1,'https://test@test.com/success','','');

ALTER TABLE `TrxInfo` ADD `Currency` varchar(10) NOT NULL default '';
ALTER TABLE `TrxInfo` ADD `Amount` decimal(20,4) NOT NULL default 0;
ALTER TABLE `TrxInfo` ADD `ServiceID` varchar(50) NOT NULL default '';



select * from TrxInfo
update TrxInfo set Currency = 'BDT' where TrxID <> '';

select * from AamarPayRecord t1 left join TrxInfo t2 on t1.Tran_id = t2.TrxID;
update TrxInfo t1
inner join AamarPayRecord t2 on t2.Tran_id = t1.TrxID
set t1.Amount = t2.Amount


update TrxInfo t1
INNER JOIN GatewayMerchant t2 on t1.merchantID = t2.merchantID and t1.GatewayID = t2.GatewayID
set t1.ServiceID = t2.ServiceID;

ALTER TABLE TrxInfo add CONSTRAINT FK_TrxInfo_GatewayMerchant FOREIGN KEY (ServiceID) references GatewayMerchant(ServiceID);

delete t1 from TrxInfo t1
left join GatewayMerchant t2 on t1.ServiceID = t2.ServiceID 
where t2.ServiceID is null and t1.TrxID <> '';

select * from TrxInfo

select * from GatewayMerchant

select * from GatewayData

delete from GatewayData where GatewayID = 'JeetBuzzPay'