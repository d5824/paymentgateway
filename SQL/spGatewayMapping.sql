DELIMITER $$
DROP PROCEDURE IF EXISTS `spGatewayMapping`$$
CREATE PROCEDURE `spGatewayMapping`(
in _ServiceID varchar(50)
)
BEGIN
SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ;
select t1.*,t2.GatewayName,t2.GatewayKey,t2.GatewayEmail,t3.merchantStatus,t3.success_url,t3.fail_url,t3.cancel_url
from GatewayMerchant t1
inner join GatewayData t2 on t2.GatewayID = t1.GatewayID
inner join MerchantData t3 on t3.merchantID = t1.merchantID
where ServiceID =_ServiceID

;
SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;
END$$
DELIMITER ;

select * 
from GatewayMerchant t1
inner join GatewayData t2 on t2.GatewayID = t2.GatewayID
inner join MerchantData t3 on t3.merchantID = t1.merchantID


select * from UserData