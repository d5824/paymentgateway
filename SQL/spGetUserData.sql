DELIMITER $$
DROP PROCEDURE IF EXISTS `spGetUserData`$$
CREATE PROCEDURE `spGetUserData`(
in _UID varchar(100)
)
BEGIN
DECLARE _Phone varchar(100);
SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED ;

SET _Phone = (select Phone from UserData where uid = _UID and uid != '');

if(_Phone is null) then
SET _Phone = (select Phone from UserData where uid = '' limit 1);
update UserData set uid = _UID where UserData.phone = _Phone;
end if;

select * from UserData where uid = _UID

;
SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ ;
END$$
DELIMITER ;



select * from UserData where uid = _UID
call spGetUserData('userid001');

truncate UserData
select * from UserData where City = 'n/a'
