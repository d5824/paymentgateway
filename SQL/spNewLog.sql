DELIMITER $$
DROP PROCEDURE IF EXISTS `spNewLog`$$
CREATE PROCEDURE `spNewLog`(
in _LogCategory varchar(50),
in _LogType varchar(50),
in _LogContent mediumtext,
in _LogBy varchar(50),
in _LogDateTime datetime
)
BEGIN
insert into LogData (LogCategory,LogType,LogContent,LogBy,LogDateTime)
values (_LogCategory,_LogType,_LogContent,_LogBy,_LogDateTime);
END$$
DELIMITER ;

