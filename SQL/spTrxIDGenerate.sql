
DELIMITER $$
DROP PROCEDURE IF EXISTS `spTrxIDGenerate`$$
CREATE PROCEDURE `spTrxIDGenerate`(
in _GUID varchar(50),
in _RecordDate datetime,
in _CompareDate datetime
)
BEGIN
declare lastDate datetime;
SET lastDate = (select RecordDate from tblTrxID order by ID desc limit 1);

if (lastDate is not null) then
	if(_CompareDate > lastDate) then 
		truncate tblTrxID;
	End if;
End if;

Insert into tblTrxID (GUID, RecordDate) values (_GUID,_RecordDate);
Select ID from tblTrxID where GUID = _GUID 
union
Select ID from tblTrxID where GUID = _GUID;
END$$
DELIMITER ;

Select ID from tblTrxID where GUID = '0941ee55284040929ba94b5b36f7bd59' 
union
Select ID from tblTrxID where GUID = '0941ee55284040929ba94b5b36f7bd59';

select * from TrxInfo
select * from GatewayMerchant

f16dd13ab4b549219bf08f6deb81938a
77c3b91a66bc92385a81633f8e1bbc43cd1868c43e827d0719dc389d3dd5ea18

/*sohoz*/
8e754e5fb7a34cdfaac8710aacc20dbe
c2fe28c12f9298a0d8be1cdac92fe762dfe806bd4731c61c788cc8eebf48cffc