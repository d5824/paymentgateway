DELIMITER $$
DROP PROCEDURE IF EXISTS `spUpdateParticipantList`$$
CREATE PROCEDURE `spUpdateParticipantList`(
in _jsonData longtext
)
BEGIN
DROP TEMPORARY TABLE IF EXISTS tempJSONPList;
CREATE TEMPORARY TABLE tempJSONPList select * from JSON_TABLE(
_jsonData,
"$[*]" COLUMNS(
Phone VARCHAR(100) PATH "$.Phone",
`Name` VARCHAR(100) PATH "$.Name",
Add1 VARCHAR(100) PATH "$.Add1",
Add2 VARCHAR(100) PATH "$.Add2",
City VARCHAR(100) PATH "$.City",
Country VARCHAR(100) PATH "$.Country",
Email VARCHAR(100) PATH "$.Email",
Uid VARCHAR(100) PATH "$.Uid")
) AS tmptbl;

insert into UserData
select t1.* from tempJSONPList t1
left join UserData t2 on t1.Phone = t2.Phone
where t2.Phone is null;
END$$
DELIMITER ;
