DELIMITER $$
CREATE DEFINER=`RDAdmin`@`%` PROCEDURE `spWinnerListGET`(
in _CampaignName varchar(200),
in _isDrawed TINYINT(1),
in _UserID varchar(50)
)
BEGIN
select * from tbl_CampaignWinners t1 
inner join tbl_CampaignPrize t2 on t2.CampaignName = t1.CampaignName 
and t2.UserID = t1.UserID
and t2.Prize=t1.Prize 
and t2.isDrawed = _isDrawed
where t1.CampaignName = _CampaignName and t1.UserID=_UserID;
END$$
DELIMITER ;
