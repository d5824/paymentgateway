Create TABLE IF NOT EXISTS `UserData` (
  `Phone` varchar(100) NOT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `Add1` varchar(100) DEFAULT NULL,
  `Add2` varchar(100) DEFAULT NULL,
  `City` varchar(100) DEFAULT NULL,
  `Country` varchar(100) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Uid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Phone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE INDEX index_UserData_Phone
ON UserData (Phone);

CREATE INDEX index_UserData_Uid
ON UserData (Uid);

select * from UserData

