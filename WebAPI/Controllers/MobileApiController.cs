﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.Extensions.Configuration;
using MGatewayBO.Model;
namespace WebAPI.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class MobileApiController : ControllerBase
    {
        private readonly ILogger<MobileApiController> _logger;
        private readonly IConfiguration _configuration;
        public MobileApiController(ILogger<MobileApiController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;

        }
        //[HttpPost("[action]/{setType}")]
        [HttpPost]
        //[HttpPost]
        public string Post(string setType, [FromBody] object model)
        {
            try
            {
                //string setType = string.Empty;
                string FilePath = _configuration.GetSection("FilePath").Value;
                // string json = Newtonsoft.Json.JsonConvert.SerializeObject(model);
                string j2 = Newtonsoft.Json.JsonConvert.DeserializeObject(model.ToString()).ToString();
                //System.IO.File.WriteAllText(@FilePath + "\\" + setType.ToUpper() + "_" + DateTime.Now.ToString("ddMMyy_HHmmss") + ".json", j2);
                //List<Visitor> tt = new List<Visitor>();
                //tt = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Visitor>>(model.ToString()); 
                return "Received, Thank you.";
            }
            catch (Exception ex)
            {
                return "Error , pls let me know. " + ex.Message;
                // return NotFound(ex.Message);
            }
        }

       

        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 104857600)]
        public ActionResult Login([FromHeader] string Head, [FromBody] object Response)
        {
            string LogApi = _configuration.GetValue<string>("LogFile:Login");
            //auto refresh list
            //Player profile, available cash in limit, available cash out limit
            if(LogApi == "Y")
            {
                MGatewayProcessor.MobileApi.SaveTriggerAPILog("Login", Response.ToString());
            }
            
               LoginAPIModel m = JsonConvert.DeserializeObject<LoginAPIModel>(Response.ToString());

            ApiResultModel result = MGatewayProcessor.MobileApi.Login(m); //new ApiResultModel() { status = "200", message = "OK" };
            JsonResult JResult = new JsonResult(result);
            return JResult;
        }


        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 104857600)]
        public ActionResult PingServer([FromBody] object Response)
        {
            string LogApi = _configuration.GetValue<string>("LogFile:Ping");
            //auto refresh list
            //Player profile, available cash in limit, available cash out limit
            if (LogApi == "Y")
            {
                MGatewayProcessor.MobileApi.SaveTriggerAPILog("Ping", Response.ToString());
            }

           
            PingAPIModel m = JsonConvert.DeserializeObject<PingAPIModel>(Response.ToString());
          
            ApiResultModel result = new ApiResultModel() ;
            MGatewayProcessor.MobileApi.GetGeneralApiResult(m, result);
            if (result.status == "200")
            {
                MGatewayProcessor.MobileApi.UpdateActiveShopStatus(m);
            }
            JsonResult JResult = new JsonResult(result);
            return JResult;
        }


        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 104857600)]
        public ActionResult WithdrawalRequest([FromBody] object Response)
        {
            WithdrawalRequestAPIModel m = JsonConvert.DeserializeObject<WithdrawalRequestAPIModel>(Response.ToString());
            ApiResultModel result =  MGatewayProcessor.MobileApi.WithdrawalRequest(m);//new ApiResultModel() { status = "200", message = "OK" };
            JsonResult JResult = new JsonResult(result);
            return JResult;
        }

        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 104857600)]
        public ActionResult ReceiveMessage([FromBody] object Response)
        {
            ReceiveMessageAPIModel m = JsonConvert.DeserializeObject<ReceiveMessageAPIModel>(Response.ToString());
            List<MessageContainer> messageContainer = JsonConvert.DeserializeObject<List<MessageContainer>>(m.RawMessage);
            ApiResultModel result = new ApiResultModel() { status = "200", message = "OK" };
            MGatewayProcessor.MobileApi.GetGeneralApiResult(m, result);
            if(result.status == "200")
            {
                MGatewayProcessor.MobileApi.ReceiveMessage(m, messageContainer);
            }
            JsonResult JResult = new JsonResult(result);
            return JResult;
        }


        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 104857600)]
        public ActionResult getWithdrawalTransaction([FromBody] object Response)
        {
            GeneralAPIModel m = JsonConvert.DeserializeObject<GeneralAPIModel>(Response.ToString());
            ApiResultModel result = new ApiResultModel();

            MGatewayProcessor.MobileApi.GetGeneralApiResult(m, result);
            if (result.status == "200")
            {
                result.data.TransactionList = MGatewayProcessor.MobileApi.GetTransaction(m, TransactionType.CashIn);
            }

            JsonResult JResult = new JsonResult(result);
          
            return JResult;
        }



        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 104857600)]
        public ActionResult getDepositTransaction([FromBody] object Response)
        {
            GeneralAPIModel m = JsonConvert.DeserializeObject<GeneralAPIModel>(Response.ToString());
            ApiResultModel result = new ApiResultModel();

            MGatewayProcessor.MobileApi.GetGeneralApiResult(m, result);
            if (result.status == "200")
            {
                result.data.TransactionList = MGatewayProcessor.MobileApi.GetTransaction(m, TransactionType.CashOut);
            }

            JsonResult JResult = new JsonResult(result);

            return JResult;
        }


        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 104857600)]
        public ActionResult ManualDeposit([FromBody] object Response)
        {
            ManualDepositAPIModel m = JsonConvert.DeserializeObject<ManualDepositAPIModel>(Response.ToString());
            ApiResultModel result = MGatewayProcessor.MobileApi.ManualDeposit(m);//new ApiResultModel() { status = "200", message = "OK" };

            JsonResult JResult = new JsonResult(result);
            return JResult;
        }

        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 104857600)]
        public ActionResult getHistoryTransaction([FromBody] object Response)
        {
            HistoryAPIModel m = JsonConvert.DeserializeObject<HistoryAPIModel>(Response.ToString());
            ApiResultModel result = new ApiResultModel();

            MGatewayProcessor.MobileApi.GetGeneralApiResult(m, result);
            if (result.status == "200")
            {
                result.data.TransactionList = MGatewayProcessor.MobileApi.GetHistoryTransaction(m);
            }

            JsonResult JResult = new JsonResult(result);

            return JResult;
        }



        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 104857600)]
        public ActionResult getRejectRemarks([FromBody] object Response)
        {
            GeneralAPIModel m = JsonConvert.DeserializeObject<GeneralAPIModel>(Response.ToString());
            ApiResultModel result = new ApiResultModel();

            MGatewayProcessor.MobileApi.GetGeneralApiResult(m, result);
            if (result.status == "200")
            {
                result.data.RemarkList = MGatewayProcessor.MobileApi.GetRejectRemarks().Select(x=> x.Remark).ToList();
            }

            JsonResult JResult = new JsonResult(result);

            return JResult;
        }

    }


}
