﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        public class testModel
        {
            public test2Model mWrappedSmsMessage { get; set; }
        }

        public class test2Model
        {
            public int[] mPdu { get; set; }
        }
        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            testModel m = JsonConvert.DeserializeObject<testModel>("{\"mWrappedSmsMessage\":{\"mPdu\":[8,-111,-120,16,8,1,0,38,-14,4,9,-48,-30,101,120,-114,6,0,0,34,80,19,1,115,64,66,121,-61,-16,28,13,122,-42,-23,32,-22,26,36,99,-63,96,48,23,12,6,50,-53,-33,109,16,44,-122,-69,-43,100,52,89,109,102,3,-51,-21,-29,113,121,62,55,-41,-39,46,-112,-79,92,6,81,-41,32,-104,11,6,115,-127,-124,97,118,-40,61,46,-125,-88,107,80,-114,-58,-78,-63,114,-82,25,-50,5,-94,-54,-15,73,34,40,87,-76,-30,-116,-42,106,49,25,4,-123,-23,-96,89,-20,5,-85,-67,100,48,-103,12,20,-125,-23,102,55]}}");
            byte[] byteSign = System.Text.Encoding.ASCII.GetBytes("ABC");

            byte[] result = new byte[m.mWrappedSmsMessage.mPdu.Length * sizeof(int)];
            Buffer.BlockCopy(m.mWrappedSmsMessage.mPdu, 0, result, 0, result.Length);
            string asdasd =  BitConverter.ToString(result).Replace("-", "");
            string utfString = System.Text.Encoding.UTF8.GetString(byteSign, 0, byteSign.Length);
            //string utfString2 = System.Text.Encoding.UTF8.GetString(byteSign, 0, byteSign.Length);
            string base64String = System.Text.Encoding.UTF8.GetString(result, 0, result.Length);
            //foreach(var item in m.mWrappedSmsMessage.mPdu)
            //{

            //}
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}
