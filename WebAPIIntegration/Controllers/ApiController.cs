﻿using Microsoft.AspNetCore.Mvc;
using System.Security.Cryptography;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using MGatewayProcessor;
using Newtonsoft.Json;
using APIIntegration.Model;
using System.Linq;
using System;
namespace WebAPIIntegration.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class ApiController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

        private readonly ILogger<ApiController> _logger;

        public ApiController(ILogger<ApiController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "GetWeatherForecastss")]
        public IEnumerable<WeatherForecast> Get()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),

                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]

            })
            .ToArray();
        }

        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 104857600)]
        public ActionResult GetAPIKEY([FromBody] object Response)
        {

            //var bytes = RandomNumberGenerator.GetBytes(64);

            //string ApiKey = "CT-" + Convert.ToBase64String(bytes)
            //    .Replace("/", "")
            //    .Replace("+", "")
            //    .Replace("=", "")
            //    .Substring(0, 33);

            return new JsonResult("");

        }

        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 104857600)]
        public ActionResult DepositRequest([FromBody] object Response)
        {
            string ApiKey = Request.Headers["API-KEY"].ToString();
            DepositAPIModel res = JsonConvert.DeserializeObject<DepositAPIModel>(Response.ToString());
            ApiResultModel result = new ApiResultModel() { status = "200", message = "OK" };
            string MerchantID = Api.GetGeneralApiResult(ApiKey, result);
        
            if (result.status != "500")
            {
                if (!Api.CheckMD5Encryption("deposit", Response.ToString(), ApiKey))
                {
                    Api.SetApiResult(result, "500", "Invalid MD5 hash.");
                }
                else
                {
                    MGatewayBO.Model.CashInOutSQLModel m = Api.ConvertDeposit(MerchantID, res);
                    MGatewayBO.Model.AddManualRecordContainer AddResult = CashTransaction.AddRecordManual(MerchantID, m, true);
                    string UniqueKey = PaymentApi.InsertToApiMapping(MerchantID, AddResult.transactionID, res.merchantDepositId, res.NotifyURL, res.RedirectURL);
                    result.data.depositId = UniqueKey;
                }
               
            }


            JsonResult JResult = new JsonResult(result);
            return JResult;
        }

        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 104857600)]
        public ActionResult WithdrawalRequest([FromBody] object Response)
        {

            string ApiKey = Request.Headers["API-KEY"].ToString();
            WithdrawalAPIModel res = JsonConvert.DeserializeObject<WithdrawalAPIModel>(Response.ToString());
            ApiResultModel result = new ApiResultModel() { status = "200", message = "OK" };
            string MerchantID = Api.GetGeneralApiResult(ApiKey, result);
         
            if (result.status != "500")
            {
                if (!Api.CheckMD5Encryption("withdrawal", Response.ToString(), ApiKey))
                {
                    Api.SetApiResult(result, "500", "Invalid MD5 hash.");
                }
                else
                {
                    MGatewayBO.Model.CashInOutSQLModel m = Api.ConvertWithdrawal(MerchantID, res);
                    MGatewayBO.Model.AddManualRecordContainer AddResult = CashTransaction.AddRecordManual(MerchantID, m, true);
                    string UniqueKey = PaymentApi.InsertToApiMapping(MerchantID, AddResult.transactionID, res.merchantWithdrawalId, res.NotifyURL, res.RedirectURL);
                    result.data.withdrawId = UniqueKey;
                }
               
            }

            JsonResult JResult = new JsonResult(result);
            return JResult;
        }




    }
}