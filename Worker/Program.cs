﻿using System;
using System.Configuration;
using System.Threading;
namespace Worker
{
    class Program
    {

        static void Main(string[] args)
        {
            string DBName = ConfigurationManager.AppSettings.Get("DBName");
            string ServerName = ConfigurationManager.AppSettings.Get("ServerName");
            MGatewayRepository.MySQLDB._DbName = DBName;
            MGatewayRepository.MySQLDB._Server = ServerName;
            MGatewayBO.Model.GlobalSetingsViewModel gbl = MGatewayProcessor.GlobalSettings.GetSettings();
            string IdleDuration = gbl.IdleInterval.ToString();
            int Interval = MGatewayProcessor.Helpers.DataReaderHelper.GetFormInt(IdleDuration);
           if(Interval == 0)
            {
                Console.WriteLine("Worker not started. Interval not set");
                Console.WriteLine("Press \'q\' to quit the worker.");
                while (Console.Read() != 'q') ;
                return;
            }
            System.Timers.Timer aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new System.Timers.ElapsedEventHandler(TimerCallback);
            aTimer.Interval = Interval * 1000;
            aTimer.Enabled = true;
            Console.WriteLine("Worker started.");
            Console.WriteLine("Press \'q\' to quit the worker.");
            while (Console.Read() != 'q') ;

        }

        private static void TimerCallback(object source, System.Timers.ElapsedEventArgs e)
        {

         

            string message = MGatewayProcessor.EmailNotification.SendEmail();
            Console.WriteLine("[" + DateTime.Now.ToString(Common.Common.gDisplayDateTimeFormat) + "] " + message);

            // Display the date/time when this method got called.
            //Console.WriteLine("In TimerCallback: " + DateTime.Now);
        }
    }
}
